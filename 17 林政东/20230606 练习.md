```mysql
CREATE DATABASE AttDB CHARSET utf8; -- 创建数据库
use AttDB;
CREATE table Student( -- 创建学生表
	sid	int PRIMARY key auto_increment,		-- 主键,自动增长列 学号	
	sname	varchar(20)	not null UNIQUE KEY	-- 唯一,非空 学生姓名	
);
CREATE TABLE Attence( -- 创建考勤表
	aid	int	PRIMARY key auto_increment,-- 主键,自动增长列 考勤编号
	time	varchar(20)	not null,	-- 非空 出勤时间
	type	int	,	-- 1:已到；2:迟到；3旷课 出勤状况
	sid	int	,	-- 外键 学号
	FOREIGN KEY (sid) REFERENCES Student(sid)
);

insert into student VALUES -- 插入学生数据
(1,"张三"),
(2,"李四"),
(3,"万五");
insert into Attence VALUES -- 插入考勤数据
(1,"2022-05-20 08:20:00",1,1),
(2,"2022-05-23 08:20:00",2,1),
(3,"2022-05-23 13:40:00",2,2),
(4,"2022-05-27 08:20:00",3,2),
(5,"2022-05-30 08:20:00",2,3);
```

```java
package bean;

public class Student {
    private int id;
    private String name;

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Student() {
    }

    public Student(int id, String name) {
        this.id = id;
        this.name = name;
    }
}

```

```java
package bean;

import javax.xml.crypto.Data;

public class Attence {
    private int aid;
    private int sid;
    private String name;
    private Data time;
    private int type;

    @Override
    public String toString() {
        return "Attence{" +
                "aid=" + aid +
                ", sid=" + sid +
                ", name='" + name + '\'' +
                ", time=" + time +
                ", type=" + type +
                '}';
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Data getTime() {
        return time;
    }

    public void setTime(Data time) {
        this.time = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public Attence() {
    }

    public Attence(int aid, int sid, String name, Data time, int type) {
        this.aid = aid;
        this.sid = sid;
        this.name = name;
        this.time = time;
        this.type = type;
    }
}

```

```java
package utils;

import java.sql.*;

public class DBUtil {
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }


    }
    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(
                "jdbc:mysql:///?characterEncoding=utf8&useSSL=false&useUnicode=true",
                "root",
                "root"
        );
        return conn;
    }
    public static ResultSet query(String sql,Object... x){
        ResultSet rs = null;
        try {
            Connection conn = getConn();
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < x.length; i++) {
                pst.setObject((i+1),x[i]);
            }
            rs = pst.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;

    }
    public static int update(String sql,Object... x){
        int num=0;
        try {
            Connection conn = getConn();
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < x.length; i++) {
                pst.setObject((i+1),x[i]);
            }
            num = pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return num;
    }

}

```

```java
package server;

import bean.Student;
import utils.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/add")
public class AddServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sql="select * from student";
        ResultSet rs = DBUtil.query(sql);
        ArrayList<bean.Student> list = new ArrayList<>();
        try {
            while (rs.next()) {
                int sid = rs.getInt("sid");
                String sName = rs.getString("sname");
                Student student = new Student(sid, sName);
                list.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("list",list);
        request.getRequestDispatcher("/WEB-INF/add.jsp").forward(request,response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

```

```java
package server;

import bean.Attence;
import utils.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import javax.xml.crypto.Data;
import java.io.IOException;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/list")
public class Servlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sql="select * from student s,Attence a where a.sid=s.sid";
        ResultSet rs = DBUtil.query(sql);
        ArrayList<Attence> list = new ArrayList<>();
        String num=null;
        try {
            while(rs.next()){
                int sid = rs.getInt("sid");
                String sName = rs.getString("sname");
                int aid = rs.getInt("aid");
                Date time = rs.getDate("time");
                int type = rs.getInt("type");
                switch (type){
                    case 1:
                        num="已到";
                        break;
                    case 2:
                        num="迟到";
                        break;
                    case 3:
                        num="旷课";
                        break;
                }
                Attence att = new Attence(aid,sid,sName, time,num);
                list.add(att);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("list",list);
        request.getRequestDispatcher("/WEB-INF/list.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

```

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 18059506693
  Date: 2023/6/7
  Time: 13:09
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
    <h1>学生考勤系统</h1>
    <form action="/se" method="post">
       <table border="1" cellspacing="0">
           <tr>
               <td>学生姓名</td>
               <td>
                   <select name="class" >
                       <c:forEach items="${list}" var="list">
                           <option value="${list.id}">${list.name}</option>
                       </c:forEach>
                   </select>
               </td>
           </tr>
           <tr>
               <td>考勤时间</td>
               <td>  <input type="datetime-local" name="date"></td>
           </tr>
           <tr>
               <td>考勤情况</td>
               <td><input type="radio" name="type" value="1">已到
               <input type="radio" name="type" value="2">迟到
               <input type="radio" name="type" value="3">旷课</td>
           </tr>
           <tr>
               <td colspan="2">
                   <input type="submit" value="添加">
               </td>
           </tr>
       </table>
    </form>
</body>
</html>

```

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 18059506693
  Date: 2023/6/7
  Time: 12:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生考勤</title>
    <style>
        td{
            text-align: center;
        }
    </style>
</head>
<body>
<a href="/add">添加</a>
  <table border="1" cellspacing="0" >
      <tr>
          <th>考勤编号</th>
          <th>学生编号</th>
          <th>学生姓名</th>
          <th>出勤时间</th>
          <th>出勤情况</th>
      </tr>
      <c:forEach items="${list}" var="list">
          <tr>
              <td>${list.aid}</td>
              <td>${list.sid}</td>
              <td>${list.name}</td>
              <td>${list.time}</td>
              <td>${list.type}</td>
          </tr>
      </c:forEach>
  </table>
</body>
</html>

```

```java
package Servlet;

import bean.Attence;
import bean.Student;
import utils.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/add")
public class AddServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sql = "select * from student ";
        ResultSet rs = DBUtil.query(sql);
        ArrayList<Student> list = new ArrayList<>();
        try {
            while (rs.next()){
                int sid = rs.getInt("sid");
                String name = rs.getString("sname");
                Student stu = new Student(sid, name);
                list.add(stu);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("list", list);
        request.getRequestDispatcher("/WEB-INF/add.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String sid = request.getParameter("sid");
        String time = request.getParameter("time");
        String type = request.getParameter("type");
        String sql="insert into attence values (?,?,?,?)";
        int i = DBUtil.update(sql, null, time, type, sid);
        if (i>0){
            response.sendRedirect("/list");
        }else {
            request.setAttribute("msg","添加失败");
            request.getRequestDispatcher("/WEB-INF/msg.jsp").forward(request,response);
        }
    }
}

```

