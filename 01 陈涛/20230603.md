```
package bean;

public class Brand {
    private int BrandID;
    private String BrandName;

    public Brand(int brandID, String brandName) {
        BrandID = brandID;
        BrandName = brandName;
    }

    public Brand() {
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int brandID) {
        BrandID = brandID;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    @Override
    public String toString() {
        return "Brand{" +
                "BrandID=" + BrandID +
                ", BrandName='" + BrandName + '\'' +
                '}';
    }
}
package bean;

import java.util.Date;

public class CarDetail {
    private   int CID;
    private   String Cname;
    private   String Content;
    private Date ltime;
    private   int price;
    private int BrandID;
    private String BrandName;

    public CarDetail(int CID, String cname, String content, Date ltime, int price, int brandID, String brandName) {
        this.CID = CID;
        Cname = cname;
        Content = content;
        this.ltime = ltime;
        this.price = price;
        BrandID = brandID;
        BrandName = brandName;
    }

    public CarDetail() {
    }

    public int getCID() {
        return CID;
    }

    public void setCID(int CID) {
        this.CID = CID;
    }

    public String getCname() {
        return Cname;
    }

    public void setCname(String cname) {
        Cname = cname;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public Date getLtime() {
        return ltime;
    }

    public void setLtime(Date ltime) {
        this.ltime = ltime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int brandID) {
        BrandID = brandID;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    @Override
    public String toString() {
        return "CarDetail{" +
                "CID=" + CID +
                ", Cname='" + Cname + '\'' +
                ", Content='" + Content + '\'' +
                ", ltime=" + ltime +
                ", price=" + price +
                ", BrandID=" + BrandID +
                ", BrandName='" + BrandName + '\'' +
                '}';
    }
}
package dao;

import bean.Brand;
import until.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BrandDao {
    public ArrayList<Brand>getAllBrnd() {
        ArrayList<Brand> list = new ArrayList<>();
        String sql = "select * from Brand";
        ResultSet rs = DBUtil.query(sql);
        try {
            while (rs.next()){
                int brandID = rs.getInt("BrandID");
                String brandName = rs.getString("BrandName");
                Brand brand = new Brand(brandID, brandName);
                list.add(brand);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            DBUtil.close(null,null,rs);
        }
        return list;
    }
}
package dao;

import bean.CarDetail;
import until.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Date;

public class CarDao {
    public ArrayList<CarDetail> getAllCar(){
        ArrayList<CarDetail>list=new ArrayList<>();
        String sql="SELECT * FROM `cardetail` c,brand b where c.BrandID=b.BrandID";
        ResultSet rs= DBUtil.query(sql);
        try {
            while (rs.next()){
                int CID = rs.getInt("CID");
                String Cname=rs.getString("Cname");
                String Content=rs.getString("Content");
                Date ltime=rs.getDate("ltime");
                int price=rs.getInt("price");
                int BrandID=rs.getInt("BrandID");
                String brandName = rs.getString("BrandName");
                CarDetail carDetail = new CarDetail(CID, Cname,Content, ltime, price, BrandID,brandName);
                list.add(carDetail);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            DBUtil.close(null,null,rs);
        }
        return list;
    }

    public int addCar(CarDetail carDetail){
        String sql="insert into CarDetail values(?,?,?,?,?,?)";
        int i = DBUtil.update(sql, carDetail.getCID()
                , carDetail.getCname(), carDetail.getContent(), carDetail.getLtime(),
                carDetail.getPrice(), carDetail.getBrandID());
        return i;
    }


}
package servlet;

import bean.Brand;
import bean.CarDetail;
import dao.BrandDao;
import dao.CarDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@WebServlet("/car/*")

public class CarController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String path = req.getPathInfo();
        if (path==null || path.equals("/")) {
            ArrayList<CarDetail> list = new CarDao().getAllCar();
            req.setAttribute("list", list);
            req.getRequestDispatcher("/WEB-INF/carList.jsp").forward(req, resp);
            //resp.sendRedirect("/car");
        }else if (path.equals("/add"))
        {
            ArrayList<Brand>list=new BrandDao().getAllBrnd();
            req.setAttribute("list",list);
            req.getRequestDispatcher("/WEB-INF/form.jsp").forward(req,resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String path=req.getPathInfo();
        if (path.equals("/save")){
            String Cname=req.getParameter("cname");
            int BrandID= Integer.parseInt(req.getParameter("brandid"));
            String Content=req.getParameter("content");
            int price= Integer.parseInt(req.getParameter("price"));
            String ltime=req.getParameter("ltime");

            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-mm-dd");
            Date date1 = null;
            try {
                date1 = simpleDateFormat.parse(ltime);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            CarDetail car = new CarDetail(0, Cname, Content, date1, price, BrandID, null);
            int i = new CarDao().addCar(car);
            if (i>0){
                req.setAttribute("msg","添加成功");
                req.getRequestDispatcher("/WEB-INF/msg.jsp").forward(req,resp);
                resp.sendRedirect("/car");
            }else {
                req.setAttribute("msg","添加失败");
                req.getRequestDispatcher("/WEB-INF/msg.jsp").forward(req,resp);
                resp.sendRedirect("/car");
            }
        }

    }
}
package until;

import java.sql.*;

public class DBUtil {
    private static final String url="jdbc:mysql:///CarInfo?characterEncoding=utf8";
    private static final String username="root";
    private static final String password="root";
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    public static Connection getConn(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }
    public static ResultSet query(String sql,Object...keys){
        ResultSet rs= null;
        try {
            Connection conn=getConn();
            PreparedStatement pst= conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject(i+1,keys[0]);
            }
            rs = pst.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return rs;
    }

    public static int update(String sql,Object...keys){
        int num=0;
        try {
            Connection conn=getConn();
            PreparedStatement pst= conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject(i+1,keys[0]);
            }
            num= pst.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return num;
    }

    public static void close(Connection conn,PreparedStatement ps,ResultSet rs){
        try {
            if (rs!=null){
                rs.close();
            }
            if (ps!=null){
                ps.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 17543
  Date: 2023/6/5
  Time: 13:36
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table border="1">
    <h1>查询车库车辆</h1>
    <a href="/car/add"><button>添加</button></a>
    <tr>
        <th>序号</th>
        <th>车辆名称</th>
        <th>所属品牌</th>
        <th>车辆简介</th>
        <th>价格</th>
        <th>录入时间</th>
    </tr>
<c:forEach items="${list}" var="car">
    <tr>
        <td>${car.CID}</td>
        <td>${car.cname}</td>
        <td>${car.content}</td>
        <td>${car.ltime}</td>
        <td>${car.price}</td>
        <td>${car.brandName}</td>

    </tr>
</c:forEach>


</table>
</body>
</html>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 17543
  Date: 2023/6/5
  Time: 21:11
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="/car/save" method="post">
  <table>
    <tr>
      <th>车辆名称</th>
      <td><input type="text" name="cname"></td>
    </tr>
    <tr>
      <th>所属品牌</th>
      <td>
        <select name="brandID" id="">
          <option value="">请选择</option>
          <c:forEach items="${list}" var="Brand">
          <option value="${brand.BrandID}">${brand.BrandName}</option>
          </c:forEach>
        </select>
      </td>
    </tr>
    <tr>
      <td>车辆简介</td>
      <td><input type="text" name="Content"></td>
    </tr>
    <tr>
      <th>价格</th>
      <td><input type="text" name="price"></td>
    </tr>
    <tr>
      <th>录入时间</th>
      <td><input type="text" name="ltime"></td>
    </tr>
    <tr>
      <td colspan="2"><input type="submit" value="确定"> </td>
    </tr>
  </table>
  
</form>
</body>
</html>
<%--
  Created by IntelliJ IDEA.
  User: 17543
  Date: 2023/6/5
  Time: 22:43
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>提示信息</title>
</head>
<body>
<h1>${msg}</h1>
<hr>
<a href="/car/">返回主页</a>
</body>
</html>
```