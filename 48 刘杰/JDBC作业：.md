### JDBC作业：

1. MySQL中创建一个数据库student_db

2. 库中创建student表

3. 表中数据如下

4. | 编号 | 姓名 | 性别 |
   | ---- | ---- | ---- |
   | 1    | 张三 | 男   |
   | 2    | 李四 | 女   |
   | 3    | 王五 | 男   |

5. 编写java 4个类，分别实现以下功能

   1. 查询功能，查询student中所有数据
   2. 添加功能
   3. 修改功能
   4. 删除功能

6. 扩展题【预习题】

   1. 能否实现一个类中，用四个方法来实现上面4个类的功能
   2. 能否实现将查询的结果，封装成java对象

```java
import org.w3c.dom.ls.LSOutput;

import java.sql.*;

public class Students {
    public static void main(String[] args) {
        select();
        insert();
        select();
        update();
        select();
        delete();
        select();
    }

    static void delete() {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/qwer?useSSL=false&useUnicode=true&charactorEncoding=utf8",
                    "root", "liujie");
            stmt = conn.createStatement();
            String delete = "DELETE FROM students where id=5;";
            int rs = stmt.executeUpdate(delete);
            if (rs > 0) {
                System.out.println("您成功删除"+ rs + "行!");
            } else {
                System.out.println("您删除了" + rs + "行!");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
            System.out.println("加载驱动类错误");
        } catch (SQLException e) {
            System.out.println("sql执行错误");
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    static void update() {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/qwer?useSSL=false&useUnicode=true&charactorEncoding=utf8",
                    "root", "liujie");
            String update = "UPDATE  students set name ='丘丘'  where name='丘丘' ;";
            stmt = conn.createStatement();
            int rs = stmt.executeUpdate(update);
            if ( rs> 0) {
                System.out.println("您成功修改了" + rs + "行!");
            } else {
                System.out.println("您修改了" + rs + "行!");
            }
        } catch (ClassNotFoundException e) {
            System.out.println("加载驱动类错误");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("mysql执行错误");
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    static void insert() {
        Connection conn = null;
        Statement stmt = null;
        try {
            //连接Mysql
            Class.forName("com.mysql.jdbc.Driver");
            String lianjie = "jdbc:mysql://localhost:3306/qwer?useSSL=false&useUnicode=true&charactorEncoding=utf8";
            String use = "root";
            String password = "liujie";
            conn = DriverManager.getConnection(lianjie, use, password);
            stmt = conn.createStatement();
            String insert = "insert into students values(5,'丘丘','女',87);";
            int rs = stmt.executeUpdate(insert);
            if (rs > 0) {
                System.out.println("您已成功添加" + rs + "行");
            }
        } catch (ClassNotFoundException e) {
            System.out.println("加载驱动类错误");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("mysql执行错误");
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }

    static void select() {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            //连接Mysql
            Class.forName("com.mysql.jdbc.Driver");
            String lianjie = "jdbc:mysql://localhost:3306/qwer?useSSL=false&useUnicode=true&charactorEncoding=utf8";
            String use = "root";
            String password = "liujie";
            conn = DriverManager.getConnection(lianjie, use, password);
            stmt = conn.createStatement();
            String select = "select * from Students;";
            rs = stmt.executeQuery(select);
            while (rs.next()) {
                System.out.println(
                        rs.getInt("id")
                                + "\t" + rs.getString("sex")
                                + "\t" + rs.getString("name")
                                + "\t" + rs.getInt("score")
                );
            }
        } catch (ClassNotFoundException e) {
            System.out.println("加载驱动类错误");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("mysql执行错误");
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
```



# MYSQL

CREATE DATABASE qwer charset utf8;
use qwer;
create table students(
id int,
name varchar(23),
sex varchar(33),
score int
);
insert into students VALUES(1,'甲','男',56);
insert into students VALUES(2,'乙','男',45);
insert into students VALUES(3,'丙','男',34);
insert into students VALUES(4,'丁','男',45);