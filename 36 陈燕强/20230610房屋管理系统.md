# mysql代码

```mysql
# 数据库名称：test
create database if not exists test character set utf8;
use test;
# 表：house_type (房屋类型表)
create table if not exists house_type
(
    id   int primary key auto_increment,# 字段显示  # 编号     主键,自动增长列
    type varchar(50) not null           # 房屋类型      不允许为空
);
# 房屋信息
insert into house_type
values (null, '三室一厅'),
       (null, '二室一厅'),
       (null, '四室一厅');
# 表：house_info (房源信息表)
create table if not exists house_info
(
    id             int primary key auto_increment, # 编号 主键,自动增长列
    lease_mode     varchar(50),                    # 租赁方式   可以为空
    rent           double       not null,          # 租金     不允许为空
    contacts       varchar(20),                    # 联系人    可以为空
    deposit_method varchar(20),                    # 押金方式   可以为空
    house_type_id  int,                            # 房屋类型   外键
    address        varchar(200) not null,          # 详细地址   不允许为空
    foreign key (house_type_id) references house_type (id)
);
# 房源信息
insert into house_info
values (null, '合租', 1244, '张三', '押一付三', 1, '龙岩新罗一号'),
       (null, '合租', 1244, '王五', '押二付三', 2, '龙岩新罗二号'),
       (null, '合租', 1244, '李四', '押一付一', 3, '龙岩新罗三号');
```



# java代码

## 1.工具类

```java
package utils;

import java.sql.*;

public class DBUtil {
//主机 数据库 数据库名 字符集
   private static final String url="jdbc:mysql://localhost:3306/test?useSSL=false&useUnicode=true&characterEncoding=utf8";
//  数据库用户
   private static final String user="root";
//   数据库密码
   private static final String password="root";

//   注册驱动
   static {
      try {
         Class.forName("com.mysql.jdbc.Driver");
      } catch (ClassNotFoundException e) {
         System.out.println("驱动异常！");
         e.printStackTrace();
      }
   }

//   连接数据库

   public static Connection getConn() {
      Connection conn = null;
      try {
         conn = DriverManager.getConnection(url, user, password);
      } catch (SQLException e) {
         System.out.println("连接异常！");
         e.printStackTrace();
      }
      return conn;
   }

   /**
    * 通用查询方法
    *
    * @param sql  mysql通用查询语句
    * @param keys 查询条件
    * @return 结果集
    */
   public static ResultSet getSelect(String sql, Object... keys) {
      Connection conn = getConn();
      ResultSet re = null;
      try {
         PreparedStatement pr = conn.prepareStatement(sql);
         for (int i = 0; i < keys.length; i++) {
            pr.setObject((i + 1), keys[i]);
         }
         re = pr.executeQuery();
      } catch (SQLException e) {
         System.out.println("sql查询语句异常！");
      }
      return re;
   }

   /**
    * 通用增删改方法
    *
    * @param sql  mysql通用增删改语句
    * @param keys 增删改条件
    * @return 影响行数
    */
   public static int getUpdate(String sql, Object... keys) {
      Connection conn = getConn();
      int i1 = 0;
      try {
         PreparedStatement pr = conn.prepareStatement(sql);
         for (int i = 0; i < keys.length; i++) {
            pr.setObject((i + 1), keys[i]);
         }
         i1 = pr.executeUpdate();
      } catch (SQLException e) {
         System.out.println("sql增删改语句异常！");
         e.printStackTrace();
      }
      return i1;
   }

   /**
    * 关闭资源
    * @param conn
    * @param pr
    * @param re
    */
   public static void getClose(Connection conn, PreparedStatement pr, ResultSet re) {
      try {
         if (re != null) {
            re.close();
         }
         if (pr != null) {
            pr.close();
         }
         if (conn != null) {
            conn.close();
         }
      } catch (SQLException e) {
         System.out.println("资源释放异常！");
         e.printStackTrace();
      }

   }


}

```

## 2.封装类

```java
package bean;

public class HouseInfo {
    private int id;           //编号	主键,自动增长列
    private String mode;      //租赁方式	可以为空
    private double rent;      //租金		不允许为空
    private String contacts;  //联系人	可以为空
    private String method;    //押金方式	可以为空
    private int typeId;       //房屋编号
    private String address;   //详细地址
    private String type;      //房屋类型

    public HouseInfo() {
    }

    public HouseInfo(int id, String mode, double rent, String contacts, String method, int typeId, String address, String type) {
        this.id = id;
        this.mode = mode;
        this.rent = rent;
        this.contacts = contacts;
        this.method = method;
        this.typeId = typeId;
        this.address = address;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public double getRent() {
        return rent;
    }

    public void setRent(double rent) {
        this.rent = rent;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "HouseInfo{" +
                "id=" + id +
                ", mode='" + mode + '\'' +
                ", rent=" + rent +
                ", contacts='" + contacts + '\'' +
                ", method='" + method + '\'' +
                ", typeId=" + typeId +
                ", address='" + address + '\'' +
                ", type='" + type + '\'' +
                '}';
    }
}

```

```java
package bean;

public class HouseType {
   private int typeId; //房屋编号
   private String type;//房屋类型

    public HouseType() {
    }

    public HouseType(int typeId, String type) {
        this.typeId = typeId;
        this.type = type;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "HouseType{" +
                "typeId=" + typeId +
                ", type='" + type + '\'' +
                '}';
    }
}

```

## 3.测试类

```java
package servlet;

import bean.HouseInfo;
import bean.HouseType;
import utils.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/test/*")
public class TestServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //        设置字符集，避免乱码
//        浏览器请求的字符集
        req.setCharacterEncoding("utf-8");
//        响应的字符集
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
//        获取工具类对象
        DBUtil ut = new DBUtil();


        String path = req.getPathInfo();
        if (path.matches("/add")) {
//            获取浏览器数据
            int id = Integer.parseInt(req.getParameter("id"));
            String mode = req.getParameter("mode");
            double rent = Double.parseDouble(req.getParameter("rent"));
            String contacts = req.getParameter("contacts");
            String method = req.getParameter("method");
            String typeId = req.getParameter("typeId");
            String address = req.getParameter("address");
//            定义sql语句
            String sql = "insert into house_info values (?,?,?,?,?,?,?)";
            int ins = ut.getUpdate(sql, id, mode, rent, contacts, method, typeId, address);
            if (ins > 0) {
                resp.sendRedirect("/test");
            } else {
                req.setAttribute("mag", "添加失败！");
                req.getRequestDispatcher("").forward(req, resp);
            }
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
//        设置字符集，避免乱码
//        浏览器请求的字符集
        req.setCharacterEncoding("utf-8");
//        响应的字符集
        resp.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
//        获取工具类对象
        DBUtil ut = new DBUtil();
        String path = req.getPathInfo();

        if (path == null || path.equals("/")) {
            //        建立集合，遍历结果集，获取数据 ，封装成一个对象，存入集合
            ArrayList<HouseInfo> list = new ArrayList<>();
            try {
                String sql = "select * from house_info i left join house_type ht on ht.id = i.house_type_id";
                ResultSet se = ut.getSelect(sql);
//            遍历结果集
                while (se.next()) {
                    //                获取数据
                    int id = se.getInt("i.id");
                    String mode = se.getString("lease_mode");
                    double rent = se.getDouble("rent");
                    String contacts = se.getString("contacts");
                    String method = se.getString("deposit_method");
                    int typeId = se.getInt("ht.id");
                    String address = se.getString("address");
                    String type = se.getString("type");
                    //                封装成一个对象，存入集合
                    HouseInfo info = new HouseInfo(id, mode, rent, contacts, method, typeId, address, type);
                    list.add(info);
                }
            } catch (SQLException e) {
                System.out.println("sql语句异常！");
                e.printStackTrace();
            }
            req.setAttribute("list", list);
            req.getRequestDispatcher("/WEB-INF/test.jsp").forward(req, resp);


        } else if (path.matches("/add")) {
            //        建立集合，遍历结果集，获取数据 ，封装成一个对象，存入集合
            ArrayList<HouseType> list = new ArrayList<>();
            try {
                String sql = "select *from house_type";
                ResultSet se = ut.getSelect(sql);
                while (se.next()) {
                    int id = se.getInt("id");
                    String type = se.getString("type");
                    HouseType ty = new HouseType(id, type);
                    list.add(ty);
                }
            } catch (SQLException e) {
                System.out.println("sql语句执行异常！");
                e.printStackTrace();
            }

            req.setAttribute("list", list);
            req.getRequestDispatcher("/WEB-INF/add.jsp").forward(req, resp);
        }
    }

}

```



# jsp 代码

## 1.查看

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<a href="/test/add">
    <button>添加房源</button>
</a>
<head>
    <title>查看所有学生</title>
</head>
<body>
<table border="1">
    <tr>
        <td>编号</td>
        <td>租赁方式</td>
        <td>租金（元）</td>
        <td>联系人</td>
        <td>押金方式</td>
        <td>房屋类型</td>
        <td>详细地址</td>
    </tr>
    <c:forEach items="${list}" var="list">
        <tr>
            <td>${list.id}</td>
            <td>${list.mode}</td>
            <td>${list.rent}</td>
            <td>${list.contacts}</td>
            <td>${list.method}</td>
            <td>${list.type}</td>
            <td>${list.address}</td>
        </tr>

    </c:forEach>
</table>
</body>
</html>

```

## 2.添加

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加学生</title>
</head>
<body>
<form action="/test/add" method="post">
    <table>


        <input type="hidden" name="id" value="0">


        <tr>
            <td>租赁方式</td>
            <td><input type="text" name="mode"></td>
        </tr>


        <tr>
            <td>租金</td>
            <%--  number  数字       required 必填--%>
            <td><input type="number" name="rent" required></td>
        </tr>


        <tr>
            <td>联系人</td>
            <td><input type="text" name="contacts"></td>
        </tr>


        <tr>
            <td>押金方式</td>
            <td><input type="text" name="method"></td>
        </tr>


        <tr>
            <td>房屋类型</td>
            <td>
                <select  name="typeId" required>
                    <c:forEach items="${list}" var="list">
                        <option value="${list.typeId}">${list.type}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>


        <tr>
            <td>详细地址</td>
            <td><input type="text" name="address" required></td>
        </tr>


        <tr>
            <td colspan="2"><input type="submit"></td>
        </tr>
    </table>

</form>
</body>
</html>

```

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
${mag}
</body>
</html>

```

