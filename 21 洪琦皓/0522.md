```java
import java.sql.*;
import java.util.ArrayList;

public class Test{
    public static void main(String[] args) {
        ResultSet re = null;
        Statement st = null;
        Connection conn = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            String url = "jdbc:mysql://localhost:3306/kkk?useSSL=false&useUnicode=true&characterEncoding=utf8";
            String username = "root";
            String password = "root";
            conn = DriverManager.getConnection(url,username,password);
            String sql = "select * from iii";
            st = conn.createStatement();
            re = st.executeQuery(sql);
            ArrayList<Integer> id = new ArrayList<>();
            ArrayList<String> name = new ArrayList<>();
            ArrayList<Integer> age = new ArrayList<>();
            /*for (int i = 0; i < 7; i++) {
                int id1 = re.getInt("id");
                id.add(id1);
                String name1 = re.getString("name");
                name.add(name1);
                int age1 = re.getInt("age");
                age.add(age1);
            }
            for (int k = 0; k < 7; k++) {
                System.out.println(id.get(k)+name.get(k)+age.get(k));
            }*/
            while (re.next()) {
                int id1 = re.getInt("id");
                id.add(id1);
                String name1 = re.getString("name");
                name.add(name1);
                int age1 = re.getInt("age");
                age.add(age1);
            }
            for (int i = 0; i < 6; i++) {
                System.out.println(id.get(i)+"   "+name.get(i)+"   "+age.get(i));
            }

        } catch (ClassNotFoundException e) {
            System.out.println("驱动导入异常");
        } catch (SQLException e) {
            System.out.println("SQL执行异常");
        } finally {
            try {
                re.close();
                st.close();
                conn.close();
            } catch (SQLException e) {
                System.out.println("资源释放失败");
            }
        }
    }
}
```