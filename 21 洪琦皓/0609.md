类

```java
package bean;

public class Dept {
    private int DeptID;
    private String DeptName;

    @Override
    public String toString() {
        return "Dept{" +
                "DeptID=" + DeptID +
                ", DeptName='" + DeptName + '\'' +
                '}';
    }

    public int getDeptID() {
        return DeptID;
    }

    public void setDeptID(int deptID) {
        DeptID = deptID;
    }

    public String getDeptName() {
        return DeptName;
    }

    public void setDeptName(String deptName) {
        DeptName = deptName;
    }

    public Dept() {
    }

    public Dept(int deptID, String deptName) {
        DeptID = deptID;
        DeptName = deptName;
    }
}
```

```java
package bean;

public class Emp {
    int id;
    String name;
    String sex;
    int age;
    String tel;
    String pwd;
    int deptID;
    String deptName;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public int getDeptID() {
        return deptID;
    }

    public void setDeptID(int deptID) {
        this.deptID = deptID;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Emp() {
    }

    public Emp(int id, String name, String sex, int age, String tel, String pwd, int deptID, String deptName) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.tel = tel;
        this.pwd = pwd;
        this.deptID = deptID;
        this.deptName = deptName;
    }
}
```

servlet

```java
package servlet;

import bean.Emp;
import utils.DBUtils;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/All")
public class AllServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sql="select * from emp e,dept d where e.DeptID=d.DeptID";
        ResultSet rs = DBUtils.query(sql);
        ArrayList<Emp> list = new ArrayList<>();
        try {
            while(rs.next()){
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String sex=rs.getString(3);
                int age = rs.getInt(4);
                String tel = rs.getString(5);
                String pwd = rs.getString(6);
                int deptId = rs.getInt(7);
                String DeptName = rs.getString(9);
                Emp emp = new Emp(id, name, sex, age, tel, pwd, deptId,DeptName);
                list.add(emp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("list",list);
        request.getRequestDispatcher("WEB-INF/lib/All.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
```

```java
package servlet;

import bean.Emp;
import utils.DBUtils;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/s")
public class sServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf8");
        String user=request.getParameter("user");
        String sql="select * from emp e,dept d where e.deptid=d.DeptID and EmpName like ?";
        ResultSet rs = DBUtils.query(sql, "%"+user+"%");
        ArrayList<Emp> list = new ArrayList<>();
        try {
            while(rs.next()){
                int id = rs.getInt(1);
                String name = rs.getString( 2);
                String sex=rs.getString(3);
                int age = rs.getInt(4);
                String tel = rs.getString(5);
                String pwd = rs.getString(6);
                int deptId = rs.getInt(7);
                String DeptName = rs.getString(9);
                Emp emp = new Emp(id, name, sex, age, tel, pwd, deptId,DeptName);
                list.add(emp);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("list",list);
        request.getRequestDispatcher("WEB-INF/lib/All.jsp").forward(request,response);
    }
}
```

工具类

```java
package utils;

import java.sql.*;
import java.util.Enumeration;

public class DBUtils {
    static String url="jdbc:mysql:///companymanager?useSSL=false&characterEncoding=utf8";

    static String user="root";
    static String pwd="root";
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url,user,pwd);
        return conn;
    }
    public static ResultSet query(String sql, Object...keys){
        ResultSet rs = null;
        try {
            Connection conn = getConn();
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            rs = pst.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }
    public static int update(String sql, Object...keys){
        int num=0;
        try {
            Connection conn = getConn();
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            num = pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return num;
    }
}
```

jsp

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>员工列表</title>
</head>
<body>
<form action="/s" method="post">
    姓名： <input type="text" name="user"><input type="submit">
</form>
<table border="1">
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>年龄</th>
        <th>电话</th>
        <th>部门</th>
    </tr>
    <c:forEach items="${list}" var="emp">
        <tr>
            <td>${emp.id}</td>
            <td>${emp.name}</td>
            <td>${emp.sex}</td>
            <td>${emp.age}</td>
            <td>${emp.tel}</td>
            <td>${emp.deptName}</td>
        </tr>
    </c:forEach>

</table>
</body>
</html>
```

