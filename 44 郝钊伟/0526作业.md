~~~mysql
数据库

```
CREATE DATABASE if not EXISTS aaa charset utf8;
use aaa;

CREATE TABLE if not EXISTS student(
	id int,
	name VARCHAR(20),
	age int,
	class VARCHAR(20)
);
insert into student values 
	(1001,'朱逸群',18,'一班'),
	(1002,'范建',19,'二班'),
	(1003,'史珍香',18,'四班'),
	(1004,'范统',17,'三班'),
	(1005,'杜子腾',18,'四班'),
	(1006,'刘产',19,'三班'),
	(1007,'杜琦燕',17,'一班');
```

~~~

~~~java
工具类：

```
package DBUtil;

import java.sql.*;

public class Function {
    private static final String url="jdbc:mysql:///aaa?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String name="root";
    private static final String pwd ="root";

    static {//随着类的加载而加载。即执行有且只有一次  注册驱动
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getCon() throws SQLException {//连接对象方法
        Connection con = DriverManager.getConnection(url, name, pwd);
        return con;

    }

    public static ResultSet getSelect(String sql,String ...keys){//查询方法
        ResultSet res = null;
        try {
            PreparedStatement pre = getCon().prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pre.setString((i+1),keys[i]);
            }
            res = pre.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return res;

    }

    public static int getUpdate(String sql,String ...keys){//sql的修改语句方法
        int j;
        try {
            PreparedStatement pre = getCon().prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pre.setString((i+1),keys[i]);
            }
            j = pre.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return j;
    }
}
~~~

~~~java
添加的java和html

```
package a;

import DBUtil.Funtion;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet("/InsertServlet")
public class InsertServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String age = request.getParameter("age");
        String Class = request.getParameter("class");

        System.out.println(id+name+age+Class);

        String sql ="Insert into student value (?,?,?,?)";
        int update = Funtion.getUpdate(sql, id, name, age, Class);

        if (update>0){
            response.getWriter().write("添加成功");
        }else {
            response.getWriter().write("添加失败");
        }
    }
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加</title>
</head>
<body>
<form action="/InsertServlet" method="post">
    <p>编号：<input  name="id"></p>
    <p>姓名：<input  name="name"></p>
    <p>年龄：<input  name="age"></p>
    <p>班级：<input name="class"></p>
    <p><input type="submit" value="添加"></p>

</form>
</body>
</html>
```

删除

```
package a;

import DBUtil.Funtion;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;


@WebServlet("/DeleteServlet")
public class DeleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        String id = req.getParameter("id");

        String sql="delete from student where id=?";
        int update = Funtion.getUpdate(sql, id);
        if (update>0){
            resp.getWriter().write("删除成功");
        }else {
            resp.getWriter().write("该编号不存在");
        }

    }
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>删除</title>
</head>
<body>
<form action="/DeleteServlet" method="post">
  <p>要删除的编号：<input name="id"></p>
  <p><input type="submit" value="确定"></p>

</form>

</body>
</html>
```

查询

```
package a;

import DBUtil.Funtion;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/SelectServlet")
public class SelectServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        String sql="select * from student where id=?";

        String id = req.getParameter("id");
        ResultSet select = Funtion.getSelect(sql, id);
        PrintWriter out = resp.getWriter();
        out.write("<table border=1>");
        out.write("<tr><th>编号</th><th>姓名</th><th>年龄</th><th>班级</th></tr>");
        try {
            while (select.next()){
                int id1= select.getInt("id");
                String name =select.getString("name");
                int age =select.getInt("age");
                String sex =select.getString("class");
                out.write("<tr><td>"+id1+"</td><td>"+name+"</td><td>"+age+"</td><td>"+sex+"</td></tr>");            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        out.write("</table>");
        out.write("<style>\n" +
                "  table{\n" +
                "    width: 500px;\n" +
                "height: 100px;"+
                "    border-collapse: collapse;\n" +
                "  }\n" +
                "\n" +
                "</style>");
    }
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>查询</title>
</head>
<body>
<form action="SelectServlet" method="post">
  <p>要查询的编号：<input name="id"></p>
  <p><input type="submit" value="查询"></p>
</form>
</body>
</html>
~~~

