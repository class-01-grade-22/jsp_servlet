CREATE DATABASE CarInfo charset utf8;
use CarInfo;
CREATE TABLE Brand(
BrandID int PRIMARY KEY auto_increment,
BrandName VARCHAR(50) not NULL
);

CREATE TABLE CarDetail(
CID int PRIMARY KEY auto_increment,
Cname VARCHAR(20),
Content VARCHAR(200) not NULL,
ltime TIMESTAMP DEFAULT NOW() not NULL,
price int ,
BrandID int,
FOREIGN KEY (BrandID) REFERENCES Brand(BrandID)
);
INSERT INTO brand VALUES
(0,"奥迪"),
(0,"蔡徐坤"),
(0,"爱马仕");

INSERT into cardetail VALUES
(0,"奔驰B1豪车",NULL,99999,1),
(0,"大众A1","适合年轻人的座驾",NULL,12000,2),
(0,"奥迪A4","谭哥的专座",NULL,66566,3);
封装类

package bean;

public class Brand {
    private int BrandID;
    private String BrandName;

    public Brand() {
    }

    public Brand(int brandID, String brandName) {
        BrandID = brandID;
        BrandName = brandName;
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int brandID) {
        BrandID = brandID;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }
}


package bean;

import java.util.Date;

public class CarDetail {
    private int CID;
    private String Cname;
    private String Content;
    private Date lTime;
    private int price;
    private int BrandID;
    private String BrandName;

    public CarDetail() {
    }

    public CarDetail(int CID, String cname, String content, Date lTime, int price, int brandID, String brandName) {
        this.CID = CID;
        Cname = cname;
        Content = content;
        this.lTime = lTime;
        this.price = price;
        BrandID = brandID;
        BrandName = brandName;
    }

    public int getCID() {
        return CID;
    }

    public void setCID(int CID) {
        this.CID = CID;
    }

    public String getCname() {
        return Cname;
    }

    public void setCname(String cname) {
        Cname = cname;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public Date getlTime() {
        return lTime;
    }

    public void setlTime(Date lTime) {
        this.lTime = lTime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int brandID) {
        BrandID = brandID;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }
}
工具类

package utils;

import java.sql.*;

public class DBUtil {
    private static final String url="jdbc:mysql:///CarInfo?useSSL=false&useUnicode=true&characterEncoding=utf-8";
    private static final String username="root";
    private static final String password="root";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConn(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return conn;
    }
    public static ResultSet select(String sql,Object ...bb){
        Connection conn = getConn();
        ResultSet rs = null;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            for (int i = 0; i < bb.length; i++) {
                ps.setObject((i+1),bb[i]);
            }
            rs = ps.executeQuery();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return rs;
    }

    public static int update(String sql,Object ...bb){
        Connection conn = getConn();
        int num = 0;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            for (int i = 0; i < bb.length; i++) {
                ps.setObject((i+1),bb[i]);
            }
            num = ps.executeUpdate();
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
        return num;
    }

    public static void close(Connection conn,PreparedStatement ps,ResultSet rs){
        try {
            if (rs!=null){
                rs.close();
            }
            if (ps!=null){
                ps.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }
}
Dao方法

package dao;

import bean.Brand;
import utils.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BrandDao {
    //查询所有品牌
    public ArrayList<Brand> getAllBrand(){
        ArrayList<Brand> list = new ArrayList<>();
        String sql="select * from Brand";
        ResultSet rs = DBUtil.select(sql);
        try {
            while (rs.next()){
                int brandID = rs.getInt("BrandID");
                String brandName = rs.getString("BrandName");
                Brand brand = new Brand(brandID, brandName);
                list.add(brand);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(null,null,rs);
        }
        return list;
    }
}


package dao;

import bean.CarDetail;
import utils.DBUtil;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CarDao {
    //查询所有
    public ArrayList<CarDetail> getAllCar(){
        ArrayList<CarDetail> list = new ArrayList<>();
        String sql="SELECT * FROM cardetail a,brand b WHERE a.BrandID=b.BrandID";
        ResultSet rs = DBUtil.select(sql);
        try {
            while (rs.next()){
                int cid = rs.getInt("CID");
                String cname = rs.getString("Cname");
                String content = rs.getString("Content");
                Date lTime = rs.getDate("lTime");
                int price = rs.getInt("price");
                int brandID = rs.getInt("BrandID");
                String brandName = rs.getString("BrandName");
                CarDetail car = new CarDetail(cid, cname, content, lTime, price, brandID, brandName);
                list.add(car);
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        } finally {
            DBUtil.close(null,null,rs);
        }
        return list;
    }
    //添加
    public int addCar(CarDetail car){
        String sql="insert into CarDetail values (?,?,?,?,?,?)";
        int i = DBUtil.update(sql,car.getCID(),car.getCname(),car.getContent(), car.getlTime(),car.getPrice(),car.getBrandID());
        return i;
    }
}
测试类

package servlet;

import bean.Brand;
import bean.CarDetail;
import dao.BrandDao;
import dao.CarDao;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@WebServlet("/car/*")
public class CarController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String path = request.getPathInfo();
        System.out.println("path = " + path);
        if (path ==null || path.equals("/")){
            ArrayList<CarDetail> list = new CarDao().getAllCar();
            request.setAttribute("list",list);
            request.getRequestDispatcher("/WEB-INF/all_car.jsp").forward(request,response);
        }else if (path.equals("/add")){
            ArrayList<Brand> list = new BrandDao().getAllBrand();
            request.setAttribute("list",list);
            request.getRequestDispatcher("/WEB-INF/insert.jsp").forward(request,response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String path = request.getPathInfo();
        if (path.equals("/save")){
            String cname = request.getParameter("cname");
            int brandID = Integer.parseInt(request.getParameter("brandID"));
            String content = request.getParameter("content");
            int price = Integer.parseInt(request.getParameter("price"));
            String date = request.getParameter("date");
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate=null;
            try {
                newDate = simpleDateFormat.parse(date);
            } catch (ParseException e) {
                e.printStackTrace();
            }
            CarDetail car = new CarDetail(0, cname, content, newDate, price, brandID, null);
            int i = new CarDao().addCar(car);
            if (i>0){
                request.setAttribute("msg","添加成功");
                request.getRequestDispatcher("/WEB-INF/msg.jsp").forward(request,response);
                response.sendRedirect("/car");
            }else {
                request.setAttribute("msg","添加失败");
                request.getRequestDispatcher("/WEB-INF/msg.jsp").forward(request,response);
                response.sendRedirect("/car");
            }
        }
    }
}
jsp页面

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2023/6/4
  Time: 9:49
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>查看车辆信息</title>
</head>
<body>
<a href="/car/add"><button>添加</button></a>
<hr>
<table border="1">
  <tr>
    <th>序号</th>
    <th>车辆名称</th>
    <th>所属品牌</th>
    <th>车辆简介</th>
    <th>价格</th>
    <th>录入时间</th>
  </tr>
  <c:forEach items="${list}" var="car">
    <tr>
      <td>${car.CID}</td>
      <td>${car.cname}</td>
      <td>${car.brandName}</td>
      <td>${car.content}</td>
      <td>${car.price}</td>
      <td>${car.lTime}</td>

    </tr>
  </c:forEach>
</table>
</body>
</html>


<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2023/6/4
  Time: 10:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加车辆信息</title>
    <style>
        table{
            border-collapse: collapse;
            text-align: center;
            width: 400px;
        }
        tr,th,td{
            border: 1px solid;
        }
    </style>
</head>
<body>
<form action="/car/save" method="post">
    <table>
        <tr>
            <th>车辆名称</th>
            <td><input type="text" name="cname" required></td>
        </tr>
        <tr>
            <th>所属品牌</th>
            <td><select name="brandID" id="" >
                        <option value="">请选择</option>
                        <c:forEach items="${list}" var="list">
                        <option value="${list.brandID}">${list.brandName}</option>
                        </c:forEach>
                    </select>
            </td>
        </tr>
        <tr>
            <th>车辆简介</th>
            <td><input type="text" name="content" required></td>
        </tr>
        <tr>
            <th>价格</th>
            <td><input type="text" name="price" required></td>
        </tr>
        <tr>
            <th>录入时间</th>
            <td><input type="date" name="date" required></td>
        </tr>
        <tr><td colspan="2"><input type="submit" value="确定" ></td></tr>
    </table>

</form>
</body>
</html>


<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2023/6/4
  Time: 10:23
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>提示信息</title>
</head>
<body>
<h1>${msg}</h1>
<a href="/car">返回列表</a>
</body>
</html>