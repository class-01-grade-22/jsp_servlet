```mysql
# 数据库名称：test
create database test char set utf8;
use test;
# 表：house_type (房屋类型表)
create table house_type
(
    id   int primary key auto_increment, # 编号主键,自动增长列
    type varchar(50) not null            # 房屋类型不允许为空
);
insert into house_type
values (null, '两室一厅'),
       (null, '三室两厅'),
       (null, '一室两厅');

# 表：house_info (房源信息表)
# 字段显示	字段名	数据类型	默认值	备注和说明
create table house_info
(
    id             int primary key auto_increment, # 编号	主键,自动增长列
    lease_mode     varchar(50),                    # 租赁方式	可以为空
    rent           double       not null,          # 租金不允许为空
    contacts       varchar(20),                    # 联系人可以为空
    deposit_method varchar(20),                    # 押金方式可以为空
    house_type_id  int,                            # 房屋类型外键
    address        varchar(200) not null,          # 详细地址	不允许为空
    foreign key (house_type_id) references house_type (id)
);
insert into house_info values
                            (0,'合租',800,'张三','',1,'汤臣一品'),
                            (0,'整租',1600,'李四','',2,'汤臣二品'),
                            (0,'整租',1600,'王五','',3,'汤臣三品');
```

bean

```java
package bean;

public class HouseInfo {
    private int id;
    private String lease_mode;
    private double rent;
    private String contacts;
    private String method;
    private int typeId;
    private String address;
    private String type1;

    public HouseInfo(int id, String lease_mode, double rent, String contacts, String method, int typeId, String address, String type1) {
        this.id = id;
        this.lease_mode = lease_mode;
        this.rent = rent;
        this.contacts = contacts;
        this.method = method;
        this.typeId = typeId;
        this.address = address;
        this.type1 = type1;
    }

    @Override
    public String toString() {
        return "HouseInfo{" +
                "id=" + id +
                ", lease_mode='" + lease_mode + '\'' +
                ", rent=" + rent +
                ", contacts='" + contacts + '\'' +
                ", method='" + method + '\'' +
                ", typeId=" + typeId +
                ", address='" + address + '\'' +
                ", type1='" + type1 + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLease_mode() {
        return lease_mode;
    }

    public void setLease_mode(String lease_mode) {
        this.lease_mode = lease_mode;
    }

    public double getRent() {
        return rent;
    }

    public void setRent(double rent) {
        this.rent = rent;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType1() {
        return type1;
    }

    public void setType1(String type1) {
        this.type1 = type1;
    }

    public HouseInfo() {
    }
}
package bean;

public class HouseType {
    private int id;
    private String type;

    public HouseType(int id, String type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public String toString() {
        return "HouseType{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public HouseType() {
    }
}
```

until

```java
package utils;

import java.sql.*;

public class DBUtil {
    private static final String url="jdbc:mysql:///test?characterEncoding=utf8";
    private static final String user="root";
    private static final String psw="root";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url, user, psw);
        return conn;
    }

    public static ResultSet select(String sql,Object...keys){
        try {
            Connection conn = getConn();
            PreparedStatement pst= conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            ResultSet rs= pst.executeQuery();
            return rs;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public static int update(String sql,Object...keys){
        try {
            Connection conn = getConn();
            PreparedStatement pst= conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            int rs= pst.executeUpdate();
            return rs;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


}
```

servlet

```java
package servlet;

import bean.HouseInfo;
import bean.HouseType;
import utils.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/add")
public class AddServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String sql="select * from test.house_type";
        ResultSet rs = DBUtil.select(sql);
        ArrayList<HouseType>list=new ArrayList<>();
        try {
            while (rs.next()){
                int id= rs.getInt("id");
                String type= rs.getString("type");
                HouseType a = new HouseType(id, type);
                list.add(a);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        req.setAttribute("list",list);
        req.getRequestDispatcher("/WEB-INF/All.jsp").forward(req,resp);

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        //获取表单数据
        String lease_mode=req.getParameter("lease_mode");
        String rent=req.getParameter("rent");
        String contacts=req.getParameter("contacts");
        String method=req.getParameter("method");
        String typeId=req.getParameter("typeId");
        String address=req.getParameter("address");
        String sql="insert into house_info values (?,?,?,?,?,?,?)";
        int i = DBUtil.update(sql,null,lease_mode,rent,contacts,method,typeId,address);
        if (i>0){
            resp.sendRedirect("/list");
        }else {
            resp.getWriter().write("添加失败");
        }
    }
}
package servlet;

import bean.HouseInfo;
import utils.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/list")

public class ListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String sql="select * from house_info i,house_type t  where i.house_type_id=t.id";
        ResultSet rs = DBUtil.select(sql);
        ArrayList<HouseInfo>list=new ArrayList<>();
        try {
            while (rs.next()){
                int id= rs.getInt(1);
                String mode=rs.getString(2);
                double rent= rs.getDouble(3);
                String contacts= rs.getString(4);
                String method= rs.getString(5);
                int typeId= rs.getInt(6);
                String address=rs.getString(7);
                int tid= rs.getInt(8);
                String type1= rs.getString(9);
                HouseInfo info = new HouseInfo(id, mode, rent, contacts, method, typeId, address,type1);
                list.add(info);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        req.setAttribute("list",list);
        req.getRequestDispatcher("/WEB-INF/list.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}
```

add

```html
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 17543
  Date: 2023/6/11
  Time: 22:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>'789
    <title>Title</title>
</head>
<body>
<table>
<form action="/add" method="post">
    <tr>
        <td>租赁方式</td>
        <td><input type="text" name="lease_mode"></td>
    </tr>
    <tr>
        <td>租金</td>
        <td><input type="text" name="rent"></td>
    </tr>
    <tr>
        <td>联系人</td>
        <td><input type="text" name="contacts"></td>
    </tr>
    <tr>
        <td>押金方式</td>
        <td><input type="text" name="method"></td>
    </tr>
    <tr>
        <td>房屋类型</td>
        <td><select name="typeId" >
            <c:forEach items="${list}" var="t">
                <option value="${t.id}">${t.type}</option>
            </c:forEach>
        </select>
        </td>
    </tr>
    <tr>
        <td>详细地址</td>
        <td><input type="text" name="address"></td>
    </tr>
    <tr>
        <td colspan="2"><input type="submit" value="提交"></td>
    </tr>
</form>
</table>
</body>
</html>
```

All

```html
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 17543
  Date: 2023/6/11
  Time: 21:31
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<table border="1">
    <a href="/add"><button>添加</button></a>
    <tr>
        <td>编号</td>
        <td>租赁方式</td>
        <td>租金</td>
        <td>联系人</td>
        <td>押金方式</td>
        <td>房屋类型</td>
        <td>详细地址</td>
    </tr>
    <c:forEach items="${list}" var="list">
        <tr>
            <td>${list.id}</td>
            <td>${list.lease_mode}</td>
            <td>${list.rent}</td>
            <td>${list.contacts}</td>
            <td>${list.method}</td>
            <td>${list.type1}</td>
            <td>${list.address}</td>
        </tr>
    </c:forEach>
</table>

</body>
</html>
```