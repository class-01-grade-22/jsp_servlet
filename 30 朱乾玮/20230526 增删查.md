```java
//添加的网页
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加学生</title>
</head>
<body>
<form action="/insert" method="post">
<table>
    <tr><td>学生编号：<input type = text name = id></td></tr>
    <tr><td>学生姓名：<input type = text name = name></td></tr>
    <tr><td>学生年龄：<input type = text name = age></td></tr>
    <tr><td><input type="submit" value="提交"></td></tr>
    <tr><td><a href="/select">返回查询</a></td></tr>
</table>
</form>
</body>
</html>
    
//添加代码
    package demo;

import Tool.DBtool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
@WebServlet("/insert")
public class insert extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter p = resp.getWriter();
        String sql = "insert into massage values (?,?,?)";
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String age = req.getParameter("age");
        int i = DBtool.update(sql,id,name,age);
        System.out.println("id = " + id);
        System.out.println("name = " + name);
        System.out.println("age = " + age);
        if (i > 0){
            p.write("添加成功");
            p.write("<a href = /insert>返回添加</a>");
        }else {
            p.write("添加失败");
            p.write("<a href = /insert>返回添加</a>");
        }
    }
}

//删除页面
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>删除学生</title>
</head>
<body>
<form action="/delete" method="post">
  <table>
    <tr>
      <td>请输入要删除的id：<input type="text" name = id></td>
      <td><input type = submit value="确定"></td>
    </tr>
  </table>
</form>
</body>
</html>
    
//删除代码
    package demo;

import Tool.DBtool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/delete")
public class delete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doGet(req, resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter p = resp.getWriter();
        String sql = "delete from massage where id = ?";
        String id = req.getParameter("id");
        int i = DBtool.update(sql, id);
        System.out.println("id = " + id);
        if (i > 0){
            p.write("删除成功");
        }else {
            p.write("删除失败");
        }
    }
}


//查询
package demo;

import Tool.DBtool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
@WebServlet("/select")
public class select extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter p = resp.getWriter();
        p.write("<table border = 1 solid black width = 300>");
        String sql = "select * from massage";
        ResultSet rs = DBtool.select(sql);
        p.write("<tr><td>编号</td><td>姓名</td><td>年龄</td></tr>");
        try {
            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                p.write("<tr><td>"+id+"</td><td>"+name+"</td><td>"+age+"</td></tr>");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        p.write("<tr><td><a href = insert.html>添加学生</a></td></tr>");
        p.write("<tr><td><a href = delete.html>删除学生</a></td></tr>");
        p.write("</table>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

```

