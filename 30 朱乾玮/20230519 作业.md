### JDBC作业：

1. MySQL中创建一个数据库student_db

   ```mysql
   CREATE DATABASE student_db charset utf8;
   ```

2. 库中创建student表

   ```mysql
   USE student_db;
   
   CREATE TABLE student (
       id INT NOT NULL AUTO_INCREMENT,
       name VARCHAR(50) NOT NULL,
       gender VARCHAR(10) NOT NULL,
       PRIMARY KEY(id)
   );
   
   ```

3. 表中数据如下

4. | 编号 | 姓名 | 性别 |
   | ---- | ---- | ---- |
   | 1    | 张三 | 男   |
   | 2    | 李四 | 女   |
   | 3    | 王五 | 男   |

   ```mysql
   INSERT INTO student (name, gender) VALUES ('张三', '男');
   INSERT INTO student (name, gender) VALUES ('李四', '女');
   INSERT INTO student (name, gender) VALUES ('王五', '男');
   ```
   
   
   
5. 编写java 4个类，分别实现以下功能

   1. 查询功能，查询student中所有数据

      ```java
      import java.sql.Connection;
      import java.sql.DriverManager;
      import java.sql.ResultSet;
      import java.sql.Statement;
      
      public class QueryStudent {
          public static void main(String[] args) {
              String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
              String user = "root";
              String password = "root";
      
              try {
                  // 1. 加载数据库驱动
                  Class.forName("com.mysql.jdbc.Driver");
      
                  // 2. 获取数据库连接
                  Connection connection = DriverManager.getConnection(url, user, password);
      
                  // 3. 创建 SQL 语句并执行
                  String sql = "SELECT * FROM student;";
                  Statement statement = connection.createStatement();
                  ResultSet resultSet = statement.executeQuery(sql);
      
                  // 4. 处理结果集
                  while (resultSet.next()) {
                      int id = resultSet.getInt("id");
                      String name = resultSet.getString("name");
                      String gender = resultSet.getString("gender");
                      System.out.println(id + " " + name + " " + gender);
                  }
      
                  // 5. 关闭连接
                  resultSet.close();
                  statement.close();
                  connection.close();
              } catch (Exception e) {
                  e.printStackTrace();
              }
          }
      }
      ```

   2. 添加功能

      ```java
      import java.sql.Connection;
      import java.sql.DriverManager;
      import java.sql.Statement;
      
      public class InsertStudent {
          public static void main(String[] args) {
              String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
              String user = "root";
              String password = "root";
      
              try {
                  // 1. 加载数据库驱动
                  Class.forName("com.mysql.jdbc.Driver");
      
                  // 2. 获取数据库连接
                  Connection connection = DriverManager.getConnection(url, user, password);
      
                  // 3. 创建 SQL 语句并执行
                  String sql = "INSERT INTO student (name, gender) VALUES ('赵六', '女');";
                  Statement statement = connection.createStatement();
                  int rows = statement.executeUpdate(sql);
                  System.out.println("插入了 " + rows + " 行数据");
      
                  // 4. 关闭连接
                  statement.close();
                  connection.close();
              } catch (Exception e) {
                  e.printStackTrace();
              }
          }
      }
      ```

   3. 修改功能

      ```java
      import java.sql.Connection;
      import java.sql.DriverManager;
      import java.sql.Statement;
      
      public class UpdateStudent {
          public static void main(String[] args) {
              String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
              String user = "root";
              String password = "root";
      
              try {
                  // 1. 加载数据库驱动
                  Class.forName("com.mysql.jdbc.Driver");
      
                  // 2. 获取数据库连接
                  Connection connection = DriverManager.getConnection(url, user, password);
      
                  // 3. 创建 SQL 语句并执行
                  String sql = "UPDATE student SET name='小明', gender='女' WHERE id=1;";
                  Statement statement = connection.createStatement();
                  int rows = statement.executeUpdate(sql);
                  System.out.println("修改了 " + rows + " 行数据");
      
                  // 4. 关闭连接
                  statement.close();
                  connection.close();
              } catch (Exception e) {
                  e.printStackTrace();
              }
          }
      }
      ```

   4. 删除功能

      ```java
      import java.sql.Connection;
      import java.sql.DriverManager;
      import java.sql.Statement;
      
      public class DeleteStudent {
          public static void main(String[] args) {
              String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
              String user = "root";
              String password = "root";
      
              try {
                  // 1. 加载数据库驱动
                  Class.forName("com.mysql.jdbc.Driver");
      
                  // 2. 获取数据库连接
                  Connection connection = DriverManager.getConnection(url, user, password);
      
                  // 3. 创建 SQL 语句并执行
                  String sql = "DELETE FROM student WHERE id=3;";
                  Statement statement = connection.createStatement();
                  int rows = statement.executeUpdate(sql);
                  System.out.println("删除了 " + rows + " 行数据");
      
                  // 4. 关闭连接
                  statement.close();
                  connection.close();
              } catch (Exception e) {
                  e.printStackTrace();
              }
          }
      }
      ```

6. 扩展题【预习题】

   1. 能否实现一个类中，用四个方法来实现上面4个类的功能
   
   2. 能否实现将查询的结果，封装成java对象
   
      ```java
      import java.sql.*;
      
      public class StudentDao {
          private static final String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
          private static final String user = "root";
          private static final String password = "root";
      
          public static void main(String[] args) {
              // 查询所有
              queryAll();
      
              // 添加
              addStudent("赵六", "女");
              queryAll();
      
              // 修改
              updateStudent(1, "小明", "女");
              queryAll();
      
              // 删除
              deleteStudent(3);
              queryAll();
          }
      
          // 查询所有
          private static void queryAll() {
              try {
                  Connection connection = DriverManager.getConnection(url, user, password);
                  String sql = "SELECT * FROM student;";
                  Statement statement = connection.createStatement();
                  ResultSet resultSet = statement.executeQuery(sql);
                  while (resultSet.next()) {
                      int id = resultSet.getInt("id");
                      String name = resultSet.getString("name");
                      String gender = resultSet.getString("gender");
                      System.out.println(id + " " + name + " " + gender);
                  }
                  resultSet.close();
                  statement.close();
                  connection.close();
              } catch (Exception e) {
                  e.printStackTrace();
              }
              System.out.println();
          }
      
          // 添加
          private static void addStudent(String name, String gender) {
              try {
                  Connection connection = DriverManager.getConnection(url, user, password);
                  String sql = String.format("INSERT INTO student (name, gender) VALUES ('%s', '%s');", name, gender);
                  Statement statement = connection.createStatement();
                  int rows = statement.executeUpdate(sql);
                  System.out.println("插入了 " + rows + " 行数据");
                  statement.close();
                  connection.close();
              } catch (Exception e) {
                  e.printStackTrace();
              }
          }
      
          // 修改
          private static void updateStudent(int id, String name, String gender) {
              try {
                  Connection connection = DriverManager.getConnection(url, user, password);
                  String sql = String.format("UPDATE student SET name='%s', gender='%s' WHERE id=%d;", name, gender, id);
                  Statement statement = connection.createStatement();
                  int rows = statement.executeUpdate(sql);
                  System.out.println("修改了 " + rows + " 行数据");
                  statement.close();
                  connection.close();
              } catch (Exception e) {
                  e.printStackTrace();
              }
          }
      
          // 删除
          private static void deleteStudent(int id) {
              try {
                  Connection connection = DriverManager.getConnection(url, user, password);
                  String sql = String.format("DELETE FROM student WHERE id=%d;", id);
                  Statement statement = connection.createStatement();
                  int rows = statement.executeUpdate(sql);
                  System.out.println("删除了 " + rows + " 行数据");
                  statement.close();
                  connection.close();
              } catch (Exception e) {
                  e.printStackTrace();
              }
          }
      
          // 将查询的结果封装成 java 对象
          private static void queryAllObject() {
              try {
                  Connection connection = DriverManager.getConnection(url, user, password);
                  String sql = "SELECT * FROM student;";
                  Statement statement = connection.createStatement();
                  ResultSet resultSet = statement.executeQuery(sql);
                  while (resultSet.next()) {
                      int id = resultSet.getInt("id");
                      String name = resultSet.getString("name");
                      String gender = resultSet.getString("gender");
                      Student student = new Student(id, name, gender);
                      System.out.println(student.toString());
                  }
                  resultSet.close();
                  statement.close();
                  connection.close();
              } catch (Exception e) {
                  e.printStackTrace();
              }
          }
      
          // Student 类
          static class Student {
              private int id;
              private String name;
              private String gender;
      
              public Student(int id, String name, String gender) {
                  this.id = id;
                  this.name = name;
                  this.gender = gender;
              }
      
              public int getId() {
                  return id;
              }
      
              public void setId(int id) {
                  this.id = id;
              }
      
              public String getName() {
                  return name;
              }
      
              public void setName(String name) {
                  this.name = name;
              }
      
              public String getGender() {
                  return gender;
              }
      
              public void setGender(String gender) {
                  this.gender = gender;
              }
      
              @Override
              public String toString() {
                  return "Student{" +
                          "id=" + id +
                          ", name='" + name + '\'' +
                          ", gender='" + gender + '\'' +
                          '}';
              }
          }
      }
      ```