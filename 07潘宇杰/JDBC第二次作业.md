package tool;

import java.sql.*;

public class DBTool {
    private static final String url = "jdbc:mysql:///student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String name = "root";
    private static final String pwd = "root";


    // 0.导包
    //      1.注册驱动
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    //      1.5 编写主机地址，数据库名，关闭SSL，指定字符集和编码

    //      2.获取连接的对象，并用conn对象保存

    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url, name, pwd);
        return conn;
    }


    // 通用的查询方法
    // ...叫可变参数，表示查以没有参数。也可以表示有任意个参数，其实就是相当是一个数组
    // 将？号对应的值放到数组中，
    public static ResultSet select(String sql, String ...keys) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);//
        // 给？号赋值
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+1),keys[i]);
        }

        ResultSet rs = pst.executeQuery();
        return rs;
    }
    // 通用的删，改，增的方法
    public static int update(String sql,String ...keys) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            // 把数组里第0个下标的值，赋值给第0+1个位置的？号
            pst.setString((i+1),keys[i]);
        }
        int i = pst.executeUpdate();
        return i;
    }

    // 通用的关闭资源的方法
    public static void close(Connection conn,PreparedStatement pst,ResultSet rs){
        try {
            if (rs!=null){
                rs.close();
            }
            if (pst!=null){
                pst.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
package tool;

import java.sql.Connection;
import java.sql.SQLException;

public class Insert {
    public static void main(String[] args) throws SQLException {
        Connection conn = DBTool.getConn();
     String   sql = "insert into student values (?,?,?)";
         int i = DBTool.update(sql, "4","赵姐","女");
         if (i>0){
             System.out.println("修改成功");
         }else{
             System.out.println("没有任何内容被修改");
         }

    }
}
package tool;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class Select {
    public static void main(String[] args) throws SQLException {
        Connection conn = DBTool.getConn();
        String sql="select * from student";
        ResultSet rst =DBTool.select(sql);
        while (rst.next()){
            // 有两种方法来提取结果集里的元素，1，通过字段的位置索引（从1开始）【方便遍历】，2.通过字段名【直观】
            int id = rst.getInt("id");
            String name2 = rst.getString("name");
            String sex = rst.getString("sex");
            System.out.println("编号:"+id+","+name2+","+sex);
            // 如何不直接打印，而是将查询的每行结果，封装成一个对应的对象，然后将对象。存入一个集合，最后遍历输出这个集合里的每个对象
        }
        DBTool.close(conn,null,rst);


        // sql = "insert into user values (?,?,?)";
        // int i = DBUtil.update(sql, "0","赵姐","555555");
        // if (i>0){
        //     System.out.println("修改成功");
        // }else{
        //     System.out.println("没有任何内容被修改");
        // }
    }
}