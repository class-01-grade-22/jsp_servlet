数据库

```mysql
-- 建库
create database if not exists student_db character set utf8;
-- 选库
use student_db;
create table if not exists student(
id int primary key auto_increment,          -- 编号
name varchar(5),                            -- 姓名
sex enum('男','女','保密')                   -- 性别
);

insert into student values
(0,'张三','男'),
(0,'李四','女'),
(0,'王五','男');

delete from student where id=10;
UPDATE student set id=1,name='赵六',sex='男' where id=1 ;
SELECT * FROM student;
```

Java

工具类

```java
mport java.sql.*;

public class MySQLUtil {
    private static final String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf-8";
    private static final String user = "root";
    private static final String password = "root";

    static {
//            注册驱动
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("驱动异常！");
        }
    }

    private static Connection conn() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            System.out.println("连接异常！");
        }
        return conn;
    }

    /**
     * 查询
     *
     * @param sql
     * @param date
     * @return
     */
    public static ResultSet select(String sql, String... date) {
        Connection conn = conn();
        ResultSet re = null;
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            for (int i = 0; i < date.length; i++) {
                pr.setString((i + 1), date[i]);
            }
            re = pr.executeQuery();
        } catch (SQLException e) {
            System.out.println("sql查询语句异常");
        }
        return re;
    }

    /**
     * 删除
     *
     * @param sql
     * @param date
     * @return
     */
    public static int delete(String sql, String... date) {
        Connection conn = conn();
        int d = 0;
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            for (int i = 0; i < date.length; i++) {
                pr.setString((i + 1), date[i]);
            }
            d = pr.executeUpdate();
        } catch (SQLException e) {
            System.out.println("sql删除语句异常！");
        }
        return d;
    }

    /**
     * 修改
     *
     * @param sql
     * @param date
     * @return
     */
    public static int update(String sql, String... date) {
        Connection conn = conn();
        int d = 0;
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            for (int i = 0; i < date.length; i++) {
                pr.setString((i + 1), date[i]);
            }
            d = pr.executeUpdate();
        } catch (SQLException e) {
            System.out.println("sql修改语句异常！");
        }
        return d;
    }

    /**
     * 添加
     *
     * @param sql
     * @param date
     * @return
     */
    public static int insert(String sql, String... date) {
        Connection conn = conn();
        int d = 0;
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            for (int i = 0; i < date.length; i++) {
                pr.setString((i + 1), date[i]);
            }
            d = pr.executeUpdate();
        } catch (SQLException e) {
            System.out.println("sql添加语句异常！");
        }
        return d;
    }

    /**
     * 关闭资源
     * @param conn
     * @param pr
     * @param re
     */
    public static void close(Connection conn, PreparedStatement pr, ResultSet re) {
        try {
            if (re != null) {
                re.close();
            }
            if (pr != null) {
                pr.close();
            }
            if (conn != null) {
                conn.close();
            }
        } catch (SQLException e) {
            System.out.println("资源释放异常！");
        }
    }
}
```

测试类

```java
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.io.PrintWriter;

@WebServlet("/Test_1")
public class Test_1 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("访问数据");
//        设置字符集
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
//        获取输出对象
        PrintWriter writer = response.getWriter();
//        获取sql语句
        String sql="insert into student(name,sex) values (?,?)";
//        执行 sql语句
        String name = request.getParameter("name");
        String sex = request.getParameter("sex");
        int i = MySQLUtil.insert(sql,name, sex);
//        返回结果
        if (i>0){
            writer.write("成功添加"+i+"条");
        }else {
            writer.write("添加失败！");
        }
    }
}




        import javax.servlet.*;
        import javax.servlet.http.*;
        import javax.servlet.annotation.*;
        import java.io.IOException;
        import java.io.PrintWriter;

/**
 * 修改
 */
@WebServlet("/Test_2")
public class Test_2 extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //        设置字符集
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
//        获取输出对象
        PrintWriter writer = response.getWriter();
//        获取sql语句
        String sql = "delete from student where id=?";
//        执行 sql语句
        String id = request.getParameter("id");

        int i = MySQLUtil.delete(sql, id);
//        返回结果
        if (i > 0) {
            writer.write("成功删除" + i + "条");
        } else {
            writer.write("删除失败！");
        }
    }
}




        import javax.servlet.*;
        import javax.servlet.http.*;
        import javax.servlet.annotation.*;
        import java.io.IOException;
        import java.io.PrintWriter;
        import java.sql.ResultSet;
        import java.sql.SQLException;

@WebServlet("/Test_3")
public class Test_3 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        doPost(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //        设置字符集
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
//        获取输出对象
        PrintWriter writer = response.getWriter();
//        获取sql语句
        String sql="select * from student where sex=?";
        String sex = request.getParameter("sex");
        try {
//        执行语句
            ResultSet se = MySQLUtil.select(sql,sex);
            while (se.next()){
                int id = se.getInt("id");
                String name = se.getString("name");
                String sex1 = se.getString("sex");
                writer.write("学号："+id+",姓名："+name+",性别："+sex1+"<br>");
            }
        } catch (SQLException e) {
            System.out.println("sql查询语句异常！");
        }


    }
}
```

HTML

```html
<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Title</title>
</head>
<body>
<h1>查看学生</h1>

<form action="/Test_3" method="post">
        请输入查看的性别：<input type="radio" name="sex" value="男">男
<input type="radio" name="sex" value="女">女
<input type="radio" name="sex" value="保密">保密
<input type="submit" value="提交">
</form>

<a href="./select.html">查看学生</a>
<br>
<a href="./insert.html">添加学生</a>
<br>
<a href="./delete.html">删除学生</a>
</body>
</html>



<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>Title</title>
</head>
<body>
<h1>删除学生</h1>
<form action="/Test_2" method="post">
        请输入删除学号： <input type="text" name="id" value="">
<input type="submit" value="提交">

</form>
<a href="./select.html">查看学生</a>
<br>
<a href="./insert.html">添加学生</a>
<br>
<a href="./delete.html">删除学生</a>
</body>
</html>



<!DOCTYPE html>
<html lang="en">
<head>
<meta charset="UTF-8">
<title>添加学生</title>
</head>
<body>
<h1>添加学生</h1>
<form action="/Test_1" method="post">
        姓名：<input type="text" name="name" value=""><br>
    性别: <input type="radio" name="sex" value="男">男
<input type="radio" name="sex" value="女">女
<input type="radio" name="sex" value="保密">保密
<br>
<input type="submit" value="提交">
</form>

<a href="./select.html">查看学生</a>
<br>
<a href="./insert.html">添加学生</a>
<br>
<a href="./delete.html">删除学生</a>
</body>
</html>
```