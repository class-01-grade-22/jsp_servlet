package bean;

import java.util.Date;

public class Attence {
  private int  aid;
  private Date time;
  private int  type;
  private int  sid;
  private String sName;


    public Attence(String sName, String date, String type) {
    }

    public Attence(int aid, Date time, int type, int sid, String sName) {
        this.aid = aid;
        this.time = time;
        this.type = type;
        this.sid = sid;
        this.sName = sName;
    }

    public String getsName() {
        return sName;
    }

    public void setsName(String sName) {
        this.sName = sName;
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public Date getTime() {
        return time;
    }

    public void setTime(Date time) {
        this.time = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    @Override
    public String toString() {
        return "Student{" +
                "aid=" + aid +
                ", time=" + time +
                ", type=" + type +
                ", sid=" + sid +
                ", sName='" + sName + '\'' +
                '}';
    }
}
