package servlet;

import bean.Attence;
import bean.Student;
import utils.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@WebServlet("/list")
public class StudentTest extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        ArrayList<Attence> list = new ArrayList<>();
        String sql ="select * from attence a , student s   where a.sid=s.sid";
        ResultSet rs = DBUtil.query(sql);
        try {
            while (rs.next()){
                int aid = rs.getInt("aid");

                java.sql.Date time = rs.getDate("time");

                int type = rs.getInt("type");
                int sid = rs.getInt("sid");
                String sname = rs.getString("sname");
                Attence student = new Attence(aid, time, type, sid,sname);
                list.add(student);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        req.setAttribute("list",list);
        req.getRequestDispatcher("/WEB-INF/studentList.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
    }

