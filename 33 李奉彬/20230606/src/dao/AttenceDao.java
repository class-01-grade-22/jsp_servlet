package dao;

import bean.Student;
import utils.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AttenceDao {
    public ArrayList<Student> getAllAttence(){
        String sql ="select * from attence";
        ResultSet rs = DBUtil.query(sql);
        ArrayList<Student> list = new ArrayList<>();

        try {
            while (rs.next()){
                int sid = rs.getInt(1);
                String sname = rs.getString(2);
                Student attence = new Student(sid,sname);

                list.add(attence);
            }

        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }
}
