package dao;

import bean.Attence;
import utils.DBUtil;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentDao {
    public ArrayList<Attence> getAllStudent(){
        ArrayList<Attence> list = new ArrayList<>();
        String sql ="select * from attence a , student s   where a.sid=s.sid";
        ResultSet rs = DBUtil.query(sql);
        try {
            while (rs.next()){
                int aid = rs.getInt("aid");

                Date time = rs.getDate("time");

                int type = rs.getInt("type");
                int sid = rs.getInt("sid");
                String sname = rs.getString("sname");
                Attence student = new Attence(aid, time, type, sid,sname);
                list.add(student);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }
    public int addStudent(Attence student){
        String sql="insert into student values(?,?,?,?)";

        int i = DBUtil.update(sql, student.getAid(), student.getSid(), student.getTime(), student.getType());
        return i;
    }
}
