<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-06
  Time: 16:29
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        table{
            text-align: center;
        }
    </style>
</head>
<body>
<a href="/add"><button>添加</button> </a>
<hr>

<table border="1">
    <tr>
        <th>考勤编号</th>
        <th>学生编号</th>
        <th>学生姓名</th>
        <th>出勤时间</th>
        <th>出勤状况</th>

    </tr>
    <c:forEach items="${list}" var="Student">
    <tr>
        <td>${Student.aid}</td>
        <td>${Student.sid}</td>
        <td>${Student.sName}</td>
        <td>${Student.time}</td>
        <c:if test="${Student.type==1}">
            <td>已到</td>
        </c:if>
        <c:if test="${Student.type==2}">
            <td>迟到</td>
        </c:if>
        <c:if test="${Student.type==3}">
            <td>旷课</td>
        </c:if>

    </tr>
    </c:forEach>



</table>
</body>
</html>
