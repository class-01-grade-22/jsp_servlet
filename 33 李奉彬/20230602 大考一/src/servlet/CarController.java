package servlet;

import bean.Brand;
import bean.CarDetail;
import dao.BrandDao;
import dao.CarDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@WebServlet("/car/*")
public class CarController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 1 先处理乱码
        request.setCharacterEncoding("utf-8");
        // 2 获取/*具体的路径
        String path = request.getPathInfo();
        // 3 如果是/car 或 /car/的路径，就显示所有车辆
        if (path==null || path.equals("/")){
            // 1 调用DAO中的查询所有的方法,直接得到所有车辆的集合
            ArrayList<CarDetail> list = new CarDao().getAllCar();
            // 2 将集合放入request请求域的对象中
            request.setAttribute("list",list); // 这里属性名和值，都写list，方便
            // 3 将请求转发给一个负责显示所有车辆的jsp，此时jsp还没有写。所以可以先去写jsp
            // 注意，jsp要写在WEB-INF目录下，再右键复制路径
            request.getRequestDispatcher("/WEB-INF/carList.jsp").forward(request,response);
        }else if (path.equals("/add")){
            // 1 先从数据库查询所有品牌信息
            ArrayList<Brand> list = new BrandDao().getAllBrand();
            // 2 将品牌信息封装后，转发给/WEB-INF/form.jsp
            request.setAttribute("list",list);
            request.getRequestDispatcher("/WEB-INF/form.jsp").forward(request,response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        // 1 先处理乱码,两个方法都要
        request.setCharacterEncoding("utf-8");
        // 2 获取/*具体的路径
        String path = request.getPathInfo();
        // 3 判断路径，做相应的处理
        if (path.equals("/save")){
            // 获取表单的数据
            String name = request.getParameter("name");
            int brandid = Integer.parseInt(request.getParameter("brandid"));
            String content = request.getParameter("content");
            int price = Integer.parseInt(request.getParameter("price"));
            String date = request.getParameter("date");  //date = 2023-06-09
            // 得到的时间是字符串，要解析成时间类型
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate = null;
            try {
                newDate = simpleDateFormat.parse(date);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            // 封装
            CarDetail car = new CarDetail(0, name, content, newDate, price, brandid, null);
            // 调用DAO
            int i = new CarDao().addCar(car);
            if (i>0){
                // response.sendRedirect("/car");
                request.setAttribute("msg","添加成功");
                request.getRequestDispatcher("/WEB-INF/msg.jsp").forward(request,response);
            }else{
                request.setAttribute("msg","添加失败");
                request.getRequestDispatcher("/WEB-INF/msg.jsp").forward(request,response);
            }

        }
    }
}
