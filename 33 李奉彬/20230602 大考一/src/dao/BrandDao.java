package dao;

import bean.Brand;
import utils.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BrandDao {
    // 从数据库查询所有品牌
    public ArrayList<Brand> getAllBrand(){
        String sql ="select * from brand";
        ResultSet rs = DBUtil.query(sql);
        ArrayList<Brand> list = new ArrayList<>();
        try {
            while (rs.next()){
                int id = rs.getInt(1);
                String name = rs.getString(2);
                Brand brand = new Brand(id, name);
                list.add(brand);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return list;



    }
}
