package dao;

import bean.CarDetail;
import utils.DBUtil;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

/**
 * Dao类的工作，就是
 * 1 将数据从数据库取出来，封装成需要的对象   【取值】
 * 2 将封装好的对象，转化成sql操作，将对象的数据 存入数据库  【设值】
 */
public class CarDao {
    // 从数据库中查询所有所有车辆的数据，回到封装好的车辆信息
    // 给数据库发送一个SQL语句，得到所有车辆信息的结果集，将结果集，封装成车辆对象的集合，并将集合返回
    public ArrayList<CarDetail> getAllCar() {
        // 1 定义一个集合，用来存放查到的车辆对象
        ArrayList<CarDetail> list = new ArrayList<>();
        // 2 编写SQL语句
        String sql = "select * from cardetail c , brand b where c.BrandID=b.BrandID";
        // 3 使用工具类的通用查询方法，执行SQL,得到结果集
        ResultSet rs = DBUtil.query(sql);
        // 4 遍历结果集，将结果集中的每行数据，封装成车辆对象
        try {
            while (rs.next()) {
                // 注意，结果集其实是执行SQL而来，所以可以看一下SQL的在Navicat中的执行结果
                // ，所以我们要明白从结果集出取数据，其实是从字段里取数据 rs.getXXX("字段名")
                int cid = rs.getInt("cid");
                String cname = rs.getString("cname");
                String content = rs.getString("content");
                Date ltime = rs.getDate("ltime");// 注意日期有自己的类型
                int price = rs.getInt("price");
                int brandid = rs.getInt("brandid");
                String brandname = rs.getString("brandname");
                // 以上29到34行代码，成功从结果集中把每一行数据取出，并放入java的对应的变量中存储起来
                // 下一步，就是将这些变量的值，用有参构造器，封装到车辆对象中
                CarDetail car = new CarDetail(cid, cname, content, ltime, price, brandid,brandname);
                // 将封装好的车辆对象，放入集合
                list.add(car);
                // 统计处理异常

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        // 最后，将装有车辆对象的集合返回
        return list;
    }

    // 添加新车辆,注意这里要接收一个被添加到数据库的车辆对象
    public int addCar(CarDetail car) {
        // SQL语句要看Navicat中的字段是怎样的
        String sql = "insert into cardetail values (?,?,?,?,?,?)"; // 6个字段，所以有6个？号
        // 调用工具类中，通用的update方法,注意。有6个？号，就要在传SQL的同时，传入6个参数。这些参数就从 车辆对象car中取
        // car对象有get方法可以取到？号需要的值
        // 6个？号对应的字段分别是 cid,cname,content,ltime,price,brandid
        int i = DBUtil.update(sql, car.getCId(), car.getCName(), car.getContent(), car.getLTime(), car.getPrice(), car.getBrandID());
        return i;
    }
}
