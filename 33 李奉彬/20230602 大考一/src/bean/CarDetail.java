package bean;

import java.util.Date;

public class CarDetail {
    private int cId;

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    private String cName;
    private String content;
    private Date lTime;
    private int price;
    private int BrandID;
    private String brandName;

    public CarDetail(int cId, String cName, String content, Date lTime, int price, int brandID, String brandName) {
        this.cId = cId;
        this.cName = cName;
        this.content = content;
        this.lTime = lTime;
        this.price = price;
        BrandID = brandID;
        this.brandName = brandName;
    }


    // // 有参构造器，只有品牌编号，没有品牌名称 是给添加车辆
    // public CarDetail(int cId, String cName, String content, Date lTime, int price, int brandID) {
    //     this.cId = cId;
    //     this.cName = cName;
    //     this.content = content;
    //     this.lTime = lTime;
    //     this.price = price;
    //     BrandID = brandID;
    // }
    //
    // // 有参构造器，只有品牌名称，没有品牌编号 是给添加车辆
    // public CarDetail(int cId, String cName, String content, Date lTime, int price, String brandName) {
    //     this.cId = cId;
    //     this.cName = cName;
    //     this.content = content;
    //     this.lTime = lTime;
    //     this.price = price;
    //     this.brandName = brandName;
    // }

    public CarDetail() {
    }

    @Override
    public String toString() {
        return "CarDetail{" +
                "cId=" + cId +
                ", cName='" + cName + '\'' +
                ", content='" + content + '\'' +
                ", lTime=" + lTime +
                ", price=" + price +
                ", BrandID=" + BrandID +
                '}';
    }

    public int getCId() {
        return cId;
    }

    public void setCId(int cId) {
        this.cId = cId;
    }

    public String getCName() {
        return cName;
    }

    public void setCName(String cName) {
        this.cName = cName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getLTime() {
        return lTime;
    }

    public void setLTime(Date lTime) {
        this.lTime = lTime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int brandID) {
        BrandID = brandID;
    }
}
