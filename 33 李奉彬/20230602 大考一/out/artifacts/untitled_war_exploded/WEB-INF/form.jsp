<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加</title>
</head>
<body>
<form action="/car/save" method="post">
    <%--    自动补全的快捷方式 tab --%>
    <table>
        <tr>
            <th>车辆名称</th>
            <td><input type="text" name="name" required></td>
        </tr>
        <tr>
            <th>所属品牌</th>
            <td>
                <select name="brandid" id="" >
                    <option value="">请选择</option>
                    <c:forEach items="${list}" var="brand">
                    <option value="${brand.brandId}">${brand.brandName}</option>
                    </c:forEach>

                </select>

            </td>
        </tr>
        <tr>
            <th>车辆简介</th>
            <td><input type="text" name="content" required></td>
        </tr>
        <tr>
            <th>价格</th>
            <td><input type="text" name="price" required></td>
        </tr>
        <tr>
            <th>录入时间</th>
            <td><input type="date" name="date" required></td>
        </tr>
        <tr>

            <td><input type="submit" value="确定"></td>
        </tr>
    </table>
</form>
</body>
</html>
