<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-02
  Time: 15:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--jsp 可以用el 表达式配合jstl标签库循环 显示--%>
<%--${list} 表示了所有集合内容--%>
<%--为了条例性，我们可以用表格来显示，可以看试卷中的示例--%>
<a href="/car/add"><button>添加</button></a>
<hr>

<table border="1">
<%--    第一行是表格头--%>
    <tr>
<%--  th 是加粗且居中的td      --%>
        <th>序号</th>
        <th>车辆名称</th>
        <th>所属品牌</th>
        <th>车辆简介</th>
        <th>价格</th>
        <th>录入时间</th>
    </tr>
<%--    第二行开始就是从list中循环读取 使用<c:forEach--%>
<c:forEach items="${list}"  var="car">
    <tr>
<%--        car 表示一个车辆对象，对象有属性，这时属性名，可以通用car.属性名的方式取值，属性名要从类中找 --%>
<%--       而且要注意大小写--%>
        <td>${car.CId}</td>
        <td>${car.CName}</td>
        <td>${car.brandName}</td>
<%--   brandID 改成中文品牌名称  --%>
        <td>${car.content}</td>
        <td>${car.price}</td>
        <td>${car.LTime}</td>
    </tr>
</c:forEach>


</table>


</body>
</html>
