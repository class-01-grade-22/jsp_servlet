<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-10
  Time: 10:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="add" method="post">
     <input type="submit" value="添加房源">
</form>
<table border="1">
    <tr>
        <th>编号</th>
        <th>租赁方式</th>
        <th>租金</th>
        <th>联系人</th>
        <th>押金方式</th>
        <th>房屋类型</th>
        <th>详细地址</th>
    </tr>
    <c:forEach items="${list}" var="info">
        <tr>
            <td>${info.id}</td>
            <td>${info.mode}</td>
            <td>${info.rent}</td>
            <td>${info.contacts}</td>
            <td>${info.method}</td>
            <td>${info.typeId}</td>
            <td>${info.address}</td>
        </tr>
    </c:forEach>
    </table>
</body>
</html>
