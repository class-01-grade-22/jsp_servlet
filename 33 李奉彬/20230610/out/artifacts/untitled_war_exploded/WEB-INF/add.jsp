<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-11
  Time: 11:41
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="/add" method="post">

    <table border="1">
        <tr>
            <td>租赁方式</td>
            <td><input type="text" name="leaseMode"></td>
        </tr>
        <tr>
            <td>租金</td>
            <td><input type="text" name="rent"></td>
        </tr>
        <tr>
            <td>联系人</td>
            <td><input type="text" name="contacts"></td>
        </tr>
        <tr>
            <td>押金方式</td>
            <td><input type="text" name="method"></td>
        </tr>
        <tr>
            <td>房屋类型</td>
            <td>
                <select  name="typeId">

                    <c:forEach items="${list}" var="t">
                        <option value="${t.id}">${t.type}</option>
                    </c:forEach>
                </select>
            </td>
        </tr>
        <tr>
            <td>详细地址</td>
            <td><input type="text" name="address"></td>
        </tr>
        <tr>

            <td colspan="2"><input type="submit"></td>
        </tr>
    </table>

</form>
</body>
</html>
