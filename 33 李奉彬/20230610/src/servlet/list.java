package servlet;

import bean.House_info;
import util.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/list")
public class list extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //language=MySQL
        String sql="select * FROM house_info a,house_type b where a.house_type_id=b.id";

        ResultSet rs = DBUtil.select(sql);
        ArrayList<Object> list = new ArrayList<>();
        try {
            while (rs.next()){
                int id = rs.getInt(1);
                String mode = rs.getString(2);
                double rent = rs.getDouble(3);
                String contacts = rs.getString(4);
                String method = rs.getString(5);
                int typeId = rs.getInt(6);
                String address = rs.getString(7);
                House_info houseInfo = new House_info(id, mode, rent, contacts, method, typeId, address);
                list.add(houseInfo);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        System.out.println(list);
        req.setAttribute("list",list);
        req.getRequestDispatcher("/WEB-INF/list.jsp").forward(req,resp);
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
