package bean;

public class House_type {
   private int id;
   private String type;

    public House_type() {
    }

    public House_type(int id, String type) {
        this.id = id;
        this.type = type;
    }

    @Override
    public String toString() {
        return "House_type{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }
}
