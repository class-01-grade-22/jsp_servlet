package bean;

public class House_info {
private int id;
private String mode;
private double rent;
private String contacts;
private String method;
private int typeId;
private String address;

    @Override
    public String toString() {
        return "House_info{" +
                "id=" + id +
                ", mode='" + mode + '\'' +
                ", rent=" + rent +
                ", contacts='" + contacts + '\'' +
                ", method='" + method + '\'' +
                ", typeId=" + typeId +
                ", address='" + address + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public double getRent() {
        return rent;
    }

    public void setRent(double rent) {
        this.rent = rent;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getTypeId() {
        return typeId;
    }

    public void setTypeId(int typeId) {
        this.typeId = typeId;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public House_info() {
    }

    public House_info(int id, String mode, double rent, String contacts, String method, int typeId, String address) {
        this.id = id;
        this.mode = mode;
        this.rent = rent;
        this.contacts = contacts;
        this.method = method;
        this.typeId = typeId;
        this.address = address;
    }
}
