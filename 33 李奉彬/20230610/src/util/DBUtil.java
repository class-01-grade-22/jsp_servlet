package util;

import java.sql.*;

public class DBUtil {
  private static final   String url = "jdbc:mysql:///test?characterEncoding=utf8";
  private static final   String user="root";
  private static final   String pwd="root";


    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    private static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url, user, pwd);
        return conn;
    }
    public static ResultSet select(String sql,Object...keys){
        ResultSet rs =null;
        try {
            Connection conn = getConn();
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            rs= pst.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }


    public static int update(String sql,Object...keys){
        int rs =0;
        try {
            Connection conn = getConn();
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            rs= pst.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

}
