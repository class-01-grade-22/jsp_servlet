package servlet;

import bean.Emp;
import util.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/list")
public class List extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        //1.编写查询所有员工地sql语句，要连表
        String sql = "select * from emp e,dept d where e.DeptID =d.DeptID";
        //2.调用工具类，将sql语句给通用地查询方法，得到结果集
        ResultSet rs = DBUtil.select(sql);
        //3.创建一个集合，遍历结果集，将结果封装成对象，添加到集合
        ArrayList<Object> list = new ArrayList<>();
        try {
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String sex = rs.getString(3);
                int age = rs.getInt(4);
                String tel = rs.getString(5);
                String pwd = rs.getString(6);
                int deptId = rs.getInt(7);
                String deptName = rs.getString(9);
                Emp emp = new Emp(id, name, sex, age, tel, pwd, deptId, deptName);
                list.add(emp);

            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        //4.将集合添加到request域中
        request.setAttribute("list",list);
        //5.将请求转发给jsp
        request.getRequestDispatcher("/WEB-INF/list.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
