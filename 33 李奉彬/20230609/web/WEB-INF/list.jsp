<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-09
  Time: 10:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>员工列表</title>
</head>
<body>
<form action="so" method="post">
    姓名：<input type="text" name="user"> <input type="submit">
</form>
<table>
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>年龄</th>
        <th>电话</th>
        <th>所属部门</th>
    </tr>
    <c:forEach items="${list}" var="emp">
        <tr>
            <td>${emp.id}</td>
            <td>${emp.name}</td>
            <td>${emp.sex}</td>
            <td>${emp.age}</td>
            <td>${emp.tel}</td>
            <td>${emp.deptName}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
