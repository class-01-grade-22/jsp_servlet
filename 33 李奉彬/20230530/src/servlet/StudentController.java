package servlet;

import bean.Student;
import dao.StudentDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/student/*") // * 通配符，代表所有字符  //student/11
public class StudentController extends HttpServlet {
    StudentDao dao = new StudentDao();

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // 先取到*号的路径的值
        String path = req.getPathInfo();
        System.out.println("path = " + path);
        if (path==null|| path.equals("/")){
//            System.out.println("/student/ 查看所有 查看所有学生");
            ArrayList<Student> list = dao.getAllStudent();
            req.setAttribute("list",list);
            req.getRequestDispatcher("/all_student.jsp").forward(req,resp); // 请求转发
        }else if (path.matches("/\\d+")) {
            String id = path.substring(1);
            System.out.println("/student/111 查看"+id+"编号的学生");
        } else if (path.matches("/delete/\\d+")) {
            String id = path.substring(8);
            System.out.println("/student/delete/111 删除"+id+"编号的学生");
        }else if (path.matches("/update/\\d+")){
            String id = path.substring(8);
            System.out.println("/student/update/111 修改"+id+"编号的学生");
        } else if (path.matches("/add")) {
            System.out.println("/student/add 正在添加学生");
        }
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        doGet(req,resp);
    }
}
