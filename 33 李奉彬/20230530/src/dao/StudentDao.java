package dao;

import bean.Student;
import utils.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentDao {

    /**
     * 查看所有学生
     * @return 返回所有学生对象的集合
     */
    public  ArrayList<Student> getAllStudent(){
        String sql = "select * from student";
        ResultSet rs = DBUtil.query(sql);
        ArrayList<Student> list = new ArrayList<>();
        try {
            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                Student stu = new Student(id, name, sex);
                // 每封装一个学生，就装入一个集合
                list.add(stu);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(null,null,rs);
        }
        return list;

    }

    /**
     * 根据 ID 查看某个学生
     * @param id 学生的编号
     * @return 根据ID查到的学生对象
     */
    public  Student getStudentById(int id){
        String sql = "select * from student where id=?";
        ResultSet rs = DBUtil.query(sql,id);
        Student stu=null;
        try {
            if (rs.next()){
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                stu = new Student(id, name, sex);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }finally {
            DBUtil.close(null,null,rs);
        }
        return stu;

    }

    /**
     * 根据 ID 删除 某个学生
     * @param id 学生的编号
     * @return 根据ID查到的学生对象
     */
    public  int deleteStudentById(int id){
        String sql = "delete from student where id=?";
        int i = DBUtil.update(sql,id);
        return i;
    }

    /**
     * 修改某个学生
     * @param student 要被修改的学生对象
     * @return 受影响的行数
     */
    public int updateStudent(Student student){
        String sql ="update student set sex =?,name=? where id=?";
        int i = DBUtil.update(sql, student.getSex(), student.getName(), student.getId());
        return i;
    }

    /**
     * 新增某个学生
     * @param student 要被添加的学生对象
     * @return 受影响的行数
     */
    public int addStudent(Student student){
        String sql ="insert into  student values (?,?,?)";
        int i = DBUtil.update(sql,0,student.getName(),student.getSex());
        return i;
    }




}
