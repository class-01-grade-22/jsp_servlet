<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生列表</title>
</head>
<body>
<h1>查看所有学生</h1>
<hr>
<table>
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>操作</th>
    </tr>
    <c:forEach var="stu" items="${list}">
        <tr>
            <td>${stu.id}</td>
            <td>${stu.name}</td>
            <td>${stu.sex}</td>
            <td>
                <a href="/student/${stu.id}">查看</a>
                <a href="/student/update/${stu.id}">编辑</a>
                <a href="/student/delete/${stu.id}" onclick="return confirm('确定要永久删除吗？')">删除</a>
            </td>
        </tr>
    </c:forEach>
</table>
<hr>
<a href="/student/add">添加学生</a>


</body>
</html>
