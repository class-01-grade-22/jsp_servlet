<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
传递过来的姓名：${name} <br>
所有姓名如下： ${allName} <br>
取名单中第三个${allName.get(2)}

如何遍历这个集合：每个姓名各一行
<c:forEach items="${allName}" var="s">
    ${s}<br>
</c:forEach>
</body>
</html>
