```sql
CREATE DATABASE shop charset utf8;
USE shop;
CREATE TABLE student(
id int PRIMARY KEY auto_increment,
`name` VARCHAR(20),
`sex` VARCHAR(20)
);
INSERT INTO student VALUES
(0,'张三','男'),
(0,'李四','男'),
(0,'傻傻罗','男'),
(0,'呆呆罗','男'),
(0,'罗猪头','女'),
(0,'刘大傻','女'),
(0,'李重名','女'),
(0,'小小罗','女');
```

