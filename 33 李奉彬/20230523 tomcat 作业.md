```markdown
# 作业，
 * 1 数据库相关的操作，使用封装的工具类
 * 2 编写四个servlet，使用注解指定相关的访问路径，分别对应查询，修改，删除，添加的操作
 * 3 从浏览器中，访问这中个路径，显示响应的信息，查询显示结果，其它的显示成功或是失败
 * 4 预习题：如何通过浏览器传送请求参数给servlet，servlet如何接收这些参数，并使用这些参数，去影响数据库的操作？

```