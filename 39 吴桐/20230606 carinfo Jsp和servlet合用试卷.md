jsp和servlet合用

```java

//servlet
package servlet;

import bean.Car;
import dao.CarDao;
import util.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@WebServlet("/TestCar/*")
public class TestCar extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        //取出 / * 的值
        String path = req.getPathInfo();
        System.out.println(path);

        CarDao dao = new CarDao();
        if (path ==null || path.matches("/") ){

            ArrayList<Car> list = dao.getAllCar();

            req.setAttribute("list",list);
            req.getRequestDispatcher("/WEB-INF/all_cars.jsp").forward(req,resp);
        }else if (path.matches("/update")) {
            req.setCharacterEncoding("utf-8");

            ArrayList<Car> list = dao.getCname();

            req.setAttribute("list",list);
//            req.setAttribute("a",a);
            req.getRequestDispatcher("/WEB-INF/update.jsp").forward(req, resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String path = req.getPathInfo();
        req.setCharacterEncoding("utf-8");


        if (path.matches("/update")) {

            String BrandName = req.getParameter("BrandName");
            String cname = req.getParameter("pinPai");
            String content = req.getParameter("Content");
            int price = Integer.parseInt(req.getParameter("price"));
            String ltime = req.getParameter("ltime");

            // 得到的时间是字符串，要解析成时间类型
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate = null;

            try {
                newDate = simpleDateFormat.parse(ltime);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }


            CarDao dao = new CarDao();

            int brandId = dao.getID(BrandName);
            int i = dao.insertCar(cname, content, newDate, price, brandId);

            if (i > 0)
//                req.getRequestDispatcher("/TestCar/");
                resp.sendRedirect("/TestCar");
//                req.getRequestDispatcher("/testCar").forward(req,resp);
            else {
                req.setAttribute("msg", "添加失败！");

                req.getRequestDispatcher("/WEB-INF/msg.jsp").forward(req, resp);
            }
        }

    }
}
```

```java
//dao
package dao;

import bean.Car;
import util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Date;

public class CarDao {
    //查看所有车辆信息
    public ArrayList<Car> getAllCar() {
        String sql = "select * from cardetail a,brand b WHERE a.BrandID=b.BrandID;";


        ArrayList<Car> list = new ArrayList<>();
        ResultSet rs = DBUtil.query(sql);
        try {
            while (rs.next()) {
                int CID = rs.getInt("CID");
                String BrandName = rs.getString("BrandName");
                String Cname = rs.getString("Cname");
                String Content = rs.getString("Content");
                Timestamp ltime = rs.getTimestamp("ltime");
                int price = rs.getInt("price");
                int BrandID = rs.getInt("BrandID");
                Car car = new Car(CID, Cname, Content, ltime, BrandName, price, BrandID);
                list.add(car);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(rs, null, null);
        }
        return list;
    }

    //添加车辆信息
    public int insertCar(String cname, String content, Date ltime, int price, int brandId) {

        //编写sql语句
        String sql = "insert into carDetail(Cname,Content,ltime,price,BrandID) values(?,?,?,?,?)";

        //给？赋值
        return DBUtil.update(sql, cname, content, ltime, price, brandId);
        //执行
//    DBUtil.update()
    }

    //根据BrandName查询id
    public int getID(String BranName) {
        String sql = "select * from brand where brandName = ?";

        ResultSet query = DBUtil.query(sql, BranName);
        int brandID = 0;
        try {
            if (query.next()) {
                brandID = query.getInt("brandID");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return brandID;
    }

    //获取Cname
    public ArrayList<Car> getCname(){
        ArrayList<Car> list = new ArrayList<>();
        ResultSet query = DBUtil.query("select DISTINCT Cname from carDetail;");
        try {
            while (query.next()){
                String cname = query.getString("Cname");
                Car car = new Car();
                car.setCname(cname);
                list.add(car);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }
}

```

```java
// Car 封装
package bean;

import java.sql.Timestamp;
import java.util.Date;

public class Car {
    private int CID;
    private String Cname;
    private String Content;
    private Date ltime;
private String BrandName;
    private int price;
    private int BrandID;


    public Car() {
    }

    public Car(int CID, String cname, String content, Date ltime, String brandName, int price, int brandID) {
        this.CID = CID;
        Cname = cname;
        Content = content;
        this.ltime = ltime;
        BrandName = brandName;
        this.price = price;
        BrandID = brandID;
    }

    public int getCID() {
        return CID;
    }

    public void setCID(int CID) {
        this.CID = CID;
    }

    public String getCname() {
        return Cname;
    }

    public void setCname(String cname) {
        Cname = cname;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public Date getLtime() {
        return ltime;
    }

    public void setLtime(Date ltime) {
        this.ltime = ltime;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int brandID) {
        BrandID = brandID;
    }

    @Override
    public String toString() {
        return "Car{" +
                "CID=" + CID +
                ", Cname='" + Cname + '\'' +
                ", Content='" + Content + '\'' +
                ", ltime=" + ltime +
                ", BrandName='" + BrandName + '\'' +
                ", price=" + price +
                ", BrandID=" + BrandID +
                '}';
    }
}
```

```jsp
// WEB/WEB-INF/all_cars.jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-03
  Time: 08:47
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
     #tj {
         margin-left: 20px;
         margin-top: 10px;
         width: 100px;
     }
    </style>
</head>
<body>
<form action="TestCar/update">
    <input type="submit" value="添加" id="tj">
</form>
<table border="1">
    <tr>
        <th>序号</th>
        <th>车辆名称</th>
        <th>所属品牌</th>
        <th>车辆简介</th>
        <th>价格</th>
        <th>录入时间</th>
    </tr>
    <c:forEach items="${list}" var="car">
        <tr>
            <td>${car.getCID()}</td>
            <td>${car.getBrandName()}</td>
            <td>${car.getCname()}</td>
            <td>${car.getContent()}</td>
            <td>${car.getPrice()}</td>
            <td>${car.getLtime()}</td>


        </tr>
    </c:forEach>
</table>
<table>

</table>
</body>
</html>


```

```jsp
// 添加 // WEB/WEB-INF/update.jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-03
  Time: 11:13
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
    <style>
        table {
            margin: auto;
            border-collapse: collapse;
        }

        th {
            width: 150px;
        }
        td{
            width: 250px;
            height: 35px;
        }

        div{
            text-align: center;
        }

        #input_qd {
            /*margin-right: 200px;*/

            float: right;
            margin-right: 20px;
        }
    </style>
</head>
<body style="text-align: center">

    <table border="1">
        <form action="/TestCar/update" method="post">
        <div>
        <tr>
            <th>车辆名称</th>
            <td><div><input type="text" name="BrandName" required></div></td>
        </tr>
        <tr>
            <th>所属品牌</th>
            <td><div><select name="pinPai" id="" required>
                <option value="">请选择</option>
                <c:forEach items="${list}" var="car">
                <option value="${car.getCname()}">${car.getCname()}</option>
                </c:forEach>
            </select></div></td>
        </tr>
        <tr>
            <th>车辆简介</th>
            <td><div><input type="text" name="Content" required></div></td>
        </tr>
        <tr>
            <th>价格</th>
            <td><div><input type="text" name="price" required></div></td>
        </tr>
        <tr>
            <th>录入时间</th>
            <td><div><input type="date" name="ltime" required></div></td>
        </tr>
        <tr>
            <%--        <td></td>--%>
            <td colspan="2"><input type="submit" id="input_qd" value="确定"></td>
        </tr>
    </div>
        </form>
    </table>
</body>
</html>

```

```jsp
<%--添加update--%>//WEB/WEB-INF/update.jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
${msg}
</body>
</html>

```

```java
//工具类
package util;

import java.sql.*;

public class DBUtil {

    private static final String url = "jdbc:mysql://localhost/carInfo?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String useName = "root";
    private static final String password = "root";

    private static Connection getConn() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            Connection conn = DriverManager.getConnection(url, useName, password);
            return conn;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }


    //通用查询
    public static ResultSet query(String sql, Object... keys) {
        try {
            PreparedStatement pst = getConn().prepareStatement(sql);

            //给问号赋值
            for (int i = 0; i < keys.length; i++) {
                pst.setObject(i + 1, keys[i]);
            }
            //执行sql语句
            ResultSet rs = pst.executeQuery();
            return rs;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    //关闭资源
    public static void close(ResultSet rs, PreparedStatement pst, Connection conn) {
        try {
            if (rs != null) rs.close();
            if (pst != null) pst.close();
            if (conn != null) conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    //通用删改增
    public static int update(String sql, Object... keys) {
        try {
            PreparedStatement pst = getConn().prepareStatement(sql);

            //给？赋值
            for (int i = 0; i < keys.length; i++) {
                pst.setObject(i + 1, keys[i]);
            }

            //执行sql语句
            int i = pst.executeUpdate();
            return i;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }
}

```

