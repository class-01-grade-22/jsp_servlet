```jsp
//Wed 更改界面
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-05-27
  Time: 11:20
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h2>修改界面</h2>
<form action="/UpdateServlet">
i d  ： <input type="text" value=<%=request.getParameter("id")%>  disabled><br>
<input type="hidden" value=<%=request.getParameter("id")%> name="id">
姓名： <input type="text" name="nam" ><br>
性别： <input type="text" name="se"><br>
<input type="submit" value="修改">
</form>

<%!

void run(){
    System.out.println("run");
}
%>

<%
run();

%>
name
<%=  22%>
<form action="">

</form>
</body>
</html>

```

```jsp
//Web 用户界面
<%@ page import="util.DBUtil" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %><%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-05-27
  Time: 11:14
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>$Title$</title>
    <style>
        table {
            width: 259px;
            border-collapse: collapse;
        }
    </style>
</head>
<body>
<h1>用户页面</h1>
<table border="1">


    <%
        try {
            ResultSet rs = DBUtil.select("select * from emp");
    %>
    <tr>
        <th><%="id"%>
        </th>
        <th><%="姓名"%>
        </th>
        <th><%="性别"%>
        </th>
        <th><%="操作"%>
        </th>
    </tr>

    <%
        while (rs.next()) {
            int id = rs.getInt("id");
            String name = rs.getString("name");
            String sex = rs.getString("sex");
    %>
    <tr>
        <td><%=id%>
        </td>
        <td><%=name%>
        </td>
        <td><%=sex%>
        </td>
        <td>
            <a href="./dd.jsp?id=<%=id%>" target="_blank">修改</a>
            <a href="/delete?id=<%=id%>" target="_blank">删除</a>
        </td>
    </tr>
    <%
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    %>

</table>
</body>
</html>

```

```java
//更改java代码
import util.DBU2;
import util.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/UpdateServlet")
public class UpdateServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html;charset=utf8");
        req.setCharacterEncoding("utf8");

        String id = req.getParameter("id");
        String name =req.getParameter("nam");
        String sex = req.getParameter("se");
        String sql = "update emp set name=?,sex=? where id=?";

        int i = DBUtil.update(sql, name, sex, id);

        if (i>0)resp.getWriter().write("修改成功！");
        else resp.getWriter().write("修改失败!");

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

```

```java
//java 删除代码
import util.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;

@WebServlet("/delete")
public class delete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf8");
        String id = req.getParameter("id");
        String sql = "delete from emp where id = ?";
        int i = DBUtil.update(sql, id);
        DBUtil.update(sql,id);
        if (i>0) resp.getWriter().write("删除成功！");
        else resp.getWriter().write("删除失败！");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

```

```java
//工具类
package util;

import java.sql.*;

public class DBUtil {

    /**
     * 注册驱动
     * 获取连接对象
     * 连接数据库
     * 写出要执行的sql语句
     * 获取执行sql的对象
     */

    private static String url = "jdbc:mysql://localhost:3306/bbc?useSSL=false&useUnicode=true&characterEncoding=utf-8";
    private static String useName = "root";
    private static String pwd = "root";

    private static Connection conn = null;
    //    Statement stat = null;
    private static PreparedStatement pstat = null;

    private static ResultSet rs = null;

    private static Connection getConn() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection(url, useName, pwd);
            return conn;
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    //通用查询
    public static ResultSet select(String sql, String... keys) {
        try {
            //预编译
            pstat = getConn().prepareStatement(sql);
            //给?赋值
            for (int i = 0; i < keys.length; i++) {
                pstat.setString(i + 1, keys[i]);
            }
            //执行sql语句
            rs = pstat.executeQuery();
            return rs;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    //增删改
    public static int update(String sql, String... keys) {
        try {
            //预编译
            pstat = getConn().prepareStatement(sql);
            //给？号赋值
            for (int i = 0; i < keys.length; i++) {
                pstat.setString(i + 1, keys[i]);
            }
            //执行sql语句
            int i = pstat.executeUpdate();
            return i;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public void close() {
        //集合
        if (rs != null) rs = null;
        //执行sql的对象
        if (pstat != null) pstat = null;
        //获取连接的对象
        if (conn != null) conn = null;
    }

    public static void main(String[] args) {


    }
}

```

