```markdown
# 作业，
 * 1 数据库相关的操作，使用封装的工具类
 * 2 编写四个servlet，使用注解指定相关的访问路径，分别对应查询，修改，删除，添加的操作
 * 3 从浏览器中，访问这中个路径，显示响应的信息，查询显示结果，其它的显示成功或是失败
 * 4 预习题：如何通过浏览器传送请求参数给servlet，servlet如何接收这些参数，并使用这些参数，去影响数据库的操作？

```

```java
/*
select ：查看Emp表中数据            @Webservlet（"/select"）
Update ：查看执行更改语句后的结果    @Webservlet（"/Update"）
delete ： 查看执行删除语句后的结果   @Webservlet（"/delete"）
insert ： 查看执行添加语句之后的结果 @Webservlet（"/insert"）
*/

package servlet;

import util.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/select")
public class StudentServlet2 extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf8");
        PrintWriter out = resp.getWriter();
//        out.write("试试");
        out.write("    <style>\n" +
                "      table{\n"  +
                "        width: 500px;\n" +
                "        border-collapse: collapse;\n" +
                "      }\n" +
                "    </style>" +
                "<table border=1px>" +
                "<tr><th>编号</th><th>姓名</th><th>工资</th></tr>");
        try {
            ResultSet rst = DBUtil.select("SELECT * FROM emp");
            while(rst.next()){
                int id = rst.getInt("id");
                String name = rst.getString("name");
                double money = rst.getDouble("money");
                out.write("<tr><td>"+id+"</td>"+"<td>"+name+"</td>"+"<td>"+money+"</td></tr>");
            }
            out.write("</table>");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        //        super.doGet(req, resp);
    }
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
    @WebServlet("/insert")
    public static class EmpInsert extends HttpServlet {
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            resp.setContentType("text/html;charset=utf8");
            int i = DBUtil.update("insert into emp values (0,'高齐强',1000);");
            if (i > 0) resp.getWriter().write("改变" + i + "行");
            else resp.getWriter().write("未改变！");
            //        super.doGet(req, resp);
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            super.doPost(req, resp);
        }
    }

    @WebServlet("/delete")
    public static class EmpDelete extends HttpServlet{
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            resp.setContentType("text/html;charset=utf8");
            int i = DBUtil.update("delete from emp where name='高齐强'");
            if (i > 0) resp.getWriter().write("改变" + i + "行");
            else resp.getWriter().write("未改变！");
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            super.doPost(req, resp);
        }
    }

    @WebServlet("/Update")
    public static class EmpUpdate extends HttpServlet{
        @Override
        protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            resp.setContentType("text/html;charset=utf8");
            int i = DBUtil.update("Update emp set name='高启强' where name = '高齐强'");
            if (i > 0) resp.getWriter().write("改变" + i + "行");
            else resp.getWriter().write("未改变！");
        }

        @Override
        protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
            super.doPost(req, resp);
        }
    }
}

```

