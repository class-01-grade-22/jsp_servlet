```java
//servlet

package servlet;

import bean.Student;
import dao.StudentDao;
import util.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

import static java.lang.Thread.sleep;

@WebServlet("/TestServlet/*")
public class TestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //响应的字符集在jsp文件中，此处可不写。
        req.setCharacterEncoding("utf8");
	    String path = req.getPathInfo();
        //查看所有学生
        if (path == null || path.equals("/")) {
            StudentDao dao = new StudentDao();
            ArrayList<Student> list = dao.getAllStudent("emp");
            req.setAttribute("list", list);

            req.getRequestDispatcher("/all_student.jsp").forward(req, resp);

        } else if (path.matches("/\\d+")) {
            //根据ID查看学生
            String id = path.substring(1);
            StudentDao dao = new StudentDao();
            ArrayList<Student> list = dao.getStudentById("emp", Integer.parseInt(id));

            req.setAttribute("index", 1);
            req.setAttribute("list", list);
			//根据id更改学生
        } else if (path.matches("/update/\\d")) {
            String id = path.substring(8);
            StudentDao dao = new StudentDao();
            ArrayList<Student> list = dao.getStudentById("emp", Integer.parseInt(id));
            req.setAttribute("list", list);
            req.getRequestDispatcher("/update_student.jsp").forward(req, resp);
            //删除学生
        } else if (path.matches("/delete/\\d")) {
            String id = path.substring(8);
            String sql ="delete from emp where id =?";
            DBUtil.update(sql,id);
            resp.sendRedirect("/TestServlet");
            //增加学生
        }else if (path.equals("/add")){
            req.setAttribute("index",1);
            req.getRequestDispatcher("/update_student.jsp").forward(req,resp);
        }

    }

    //post用于存储jsp传输来数据
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf8");
        req.setCharacterEncoding("utf8");

        String id = req.getParameter("id");
        StudentDao dao = new StudentDao();
        ArrayList<Student> list = dao.getAllStudent("emp");
        for (Student stu : list) {
            if (Integer.parseInt(id) == stu.getId()) {
                String name = req.getParameter("name");
                String sex = req.getParameter("sex");
                //执行sql语句
                int i = dao.updateById("emp", Integer.parseInt(id), name, sex);
                if (i > 0) resp.getWriter().write("修改成功！");
                else resp.getWriter().write("修改失败！");
                resp.sendRedirect("/TestServlet");

                return;
            } else {
                String sql = "insert into emp values (?,?,?)";
                String name = req.getParameter("name");
                String sex = req.getParameter("sex");
                int i = DBUtil.update(sql, id, name, sex);

                if (i > 0) resp.getWriter().write("增加成功！");
                else resp.getWriter().write("增加失败!");
                return;
            }
        }
    }

```

```java
//dao
package dao;

import bean.Student;
import util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentDao {
    //查看所有学生
    public ArrayList<Student> getAllStudent(String table) {
        String sql = "select * from " + table;
        ResultSet rs = DBUtil.query(sql);
        try {
            ArrayList<Student> list = new ArrayList<>();
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                Student stu = new Student(id, name, sex);
                list.add(stu);
            }
            return list;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(rs, null, null);
        }
    }

    //根据ID查看某个学生
    public ArrayList<Student> getStudentById(String table, int id) {
        ArrayList<Student> list = new ArrayList<>();
        String sql = "select * from " + table + " where id = ?";
        ResultSet rs = DBUtil.query(sql, id);

        try {
            if (rs.next()) {
                int id2 = rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                Student stu = new Student(id2, name, sex);
                list.add(stu);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    //修改某个学生
    public int updateById(String table, int id, String name, String sex) {
        String sql = "update " + table + " set name=?,sex=? where id=?";
        int i = DBUtil.update(sql, name, sex, id);
        return i;
    }

    //新增某个学生
    public int addStudent(String table, int id, String name, String sex) {
        //编写sql语句
        String sql = "insert into " + table + "values (?,?,?)";
        int i = DBUtil.update(sql, id, name, sex);
        return i;
    }

}

```

```jsp
<%--all_student.jsp--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
<%--    <%=request.getParameter("i") %>--%>
    <%--测试写Java代码获取id--%>
    <title>Title</title>
    <style>
        table {
            border-collapse: collapse;
            width: 700px;
            text-align: center;
        }
    </style>
</head>
<body>
<table border="1">
    <caption><h1>学生信息</h1></caption><hr>

<%--    ${name}--%>
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>操作</th>
    </tr>

    <c:forEach items="${list}" var="stu">
        <tr>
            <td>${stu.getId()}</td>
            <td>${stu.getName()}</td>
            <td>${stu.getSex()}</td>
            <td>
                <c:if test="${index!=1}">
                <a href="/TestServlet/${stu.getId()}">查</a>
                </c:if>
                <a href="/TestServlet/update/${stu.getId()}">编辑</a>
                <a href="/TestServlet/delete/${stu.getId()}" onclick="return confirm('ok?')">删除</a>
                <c:if test="${index==1}">
                    <a href="/TestServlet">返回</a>
                </c:if>a
            </td>
        </tr>
    </c:forEach>
</table>
<hr>
<a href="<c:url value="/TestServlet/add"/>">添加学生</a>
</body>
</html>

```

```jsp
<%--update_studnet.jsp--%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="<c:url value="/TestServlet/"/>" method="post">
    <c:if test="${index!=1}">
    <p>i&nbsp;d &nbsp; &nbsp;:<input type="text" value="${list.get(0).getId()}" disabled></p>
    <input type="hidden" name="id" value="${list.get(0).getId()}">
    </c:if>
    <c:if test="${index==1}">
    <p>i d :<input type="text" name="id"></p>
    </c:if>
    <p>姓名: <input type="text" name="name" value="${list.get(0).getName()}"></p>
    <p>性别: <input type="text" name="sex" value="${list.get(0).getSex()}"></p>
    <p><input type="submit" value="确定"></p>

</form>

</body>
</html>

```

```java
//bean
package bean;

public class Student {
    //static的调用是唯一的，编写则所有对象为只能拥有一个的值
    private int id;
    private String name;
    private String sex;

    public Student(int id, String name, String sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
    }

    public Student() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}

```

```java
//util
package util;

//import java.sql.Connection;

import java.sql.*;

public class DBUtil {
    private static final String url = "jdbc:mysql://localhost:3306/bbc?useSSL=false&Unicode=ture&characterEncoding=utf8";
    private static final String useName = "root";
    private static final String password = "root";


    //关闭DB中的资源,运行完直接弹出，也就关闭了。

    //连接库
    private static Connection getConn() {
        try {
            Class.forName("com.mysql.jdbc.Driver");
            return DriverManager.getConnection(url, useName, password);
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }



    //通用查询
    public static ResultSet query(String sql, Object... keys) {
//        PreparedStatement
        try {
            PreparedStatement pst = getConn().prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject(i + 1, keys[i]);
            }
            return pst.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }


    }


    //关闭的方法
    public static void close(ResultSet rs,PreparedStatement pst,Connection conn){
        try {
            if (rs!=null) rs.close();
            if (pst!=null) pst.close();
            if (conn!=null) conn.close();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static int update(String sql,Object ...keys){
        try {
            PreparedStatement pst = getConn().prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject(i+1,keys[i]);
            }
            return pst.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}
```

