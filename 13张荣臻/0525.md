CREATE DATABASE student_db charset utf8;
USE student_db;
CREATE TABLE student(
	id int PRIMARY KEY auto_increment,
	name VARCHAR(26),
	sex VARCHAR(10)
);
INSERT INTO student VALUES 
(0,"张三","男"),
(0,"李四","女"),
(0,"王五","男");
SELECT * FROM student;
工具类

package util;

import java.sql.*;

public class DBUtil {
    private static final String url="jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String username="root";
    private static final String password="root";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }
    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }
    public static ResultSet select(String sql,String ...keys) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+1),keys[i]);
        }
        ResultSet rest = pst.executeQuery();
        return rest;

    }
    //增删改
    public static int update(String sql,String ...key) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < key.length; i++) {
            pst.setString((i+1),key[i]);
        }
        int i = pst.executeUpdate();
        return i;
    }
    public static void close(Connection conn,PreparedStatement pst,ResultSet rest) throws SQLException {
        if (conn!=null){
            conn.close();
        }
        if (pst!=null){
            pst.close();
        }
        if (rest!=null){
            rest.close();
        }
    }
}
查询

package servlet;

import util.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/select")
public class SelectServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //http默认发起的是get请求
        System.out.println("我收到了请求,ok 666");

        //为了避免乱码，要在响应之前，设置响应的文档的类型，告诉浏览器要以什么样的形式来接受和显示这个内容
        resp.setContentType("text/html;charset=utf-8");

        //如果要将显示的信息直接打印在浏览器，就要通过响应的对象
        PrintWriter out = resp.getWriter();
//        out.write("我收到了请求,ok 666");

        out.write("<style\n>"+
                " table{\n" +
                "  width: 500px;\n" +
                "  border-collapse: collapse;\n" +
                " }\n" +
                "</style>" +
                "<table border=1>");
        out.write("<tr><th>编号</th><th>姓名</th><th>性别</th></tr>");
        //下一步，使用DBUtil工具类，去将数据库中的数据读取出来
        String select="select * from student";
        try {
            ResultSet rs = DBUtil.select(select);
            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                out.write("<tr><th>"+id+"</th><th>"+name+"</th><th>"+sex+"</th></tr>");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
添加

package servlet;

import util.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet("/insert")
public class InsertServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("成功添加！");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
//        out.write("成功添加！");
        String insert="insert into student values (0,?,?)";
        int i = 0;
        try {
            i = DBUtil.update(insert, "小八哥", "男");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (i>0){
            out.write("添加成功");
        }else {
            out.write("添加失败");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
修改

package servlet;

import util.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
@WebServlet("/update")
public class Update extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("成功修改！");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        String update="update student set sex=? where name=?";
        int i = 0;
        try {
            i = DBUtil.update(update, "女", "小八哥");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (i > 0) {
            out.write("修改成功");
        }else {
            out.write("修改失败");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
删除

package servlet;

import util.DBUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet("/delete")
public class DeleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        System.out.println("成功删除！");
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        String delete="delete from student where name=?";
        int i = 0;
        try {
            i = DBUtil.update(delete, "小八哥");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        if (i>0){
            out.write("删除成功");
        }else {
            out.write("删除失败");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}