### JDBC作业：

1. MySQL中创建一个数据库student_db

2. 库中创建student表

3. 表中数据如下

4. | 编号 | 姓名 | 性别 |
   | ---- | ---- | ---- |
   | 1    | 张三 | 男   |
   | 2    | 李四 | 女   |
   | 3    | 王五 | 男   |

5. 编写java 4个类，分别实现以下功能

   1. 查询功能，查询student中所有数据

      ```java
      import java.sql.*;
      public class chaxun {
          public static void main(String[] args) {
                      Connection conn = null;
                      Statement st = null;
                      ResultSet rs = null;
                      try {
                          Class.forName("com.mysql.jdbc.Driver");//注册驱动
                          String url = "jdbc:mysql://localhost:3306/student";//定义链接对象
                          String user = "root";
                          String password = "root";
                          conn = DriverManager.getConnection(url, user, password);
                          String sql = "SELECT * FROM st";//输入sql语句
                          st = conn.createStatement();//获取sql执行的对象
                          rs = st.executeQuery(sql);//执行sql
                          while (rs.next()) {
                              String name = rs.getString("name");
                              int age = rs.getInt("age");
                              String sex = rs.getString("sex");
                              System.out.println(name+age+sex);
                          }
                      } catch (ClassNotFoundException e) {//处理返回结果
                          System.out.println("导包数据异常");
                          e.printStackTrace();
                      } catch (SQLException e) {
                          System.out.println("sql异常");
                          e.printStackTrace();
                      } finally {//释放资源
                          try {
                              rs.close();
                              st.close();
                              conn.close();
                          } catch (SQLException e) {
                              System.out.println("资源释放异常");;
                  }
                  }
                  }
                  }
      ```

      

   2. 添加功能

      ```java
      import java.sql.*;
      
      public class tianjia {
          public static void main(String[] args) {
              Connection conn = null;
              Statement st = null;
      
              try {
                  Class.forName("com.mysql.jdbc.Driver");
                  String url="jdbc:mysql://localhost:3306/student?characterEncoding=utf8&useSSl=false";
                  String user="root";
                  String possword="root";
                  conn = DriverManager.getConnection(url, user, possword);
                  String sql="INSERT into st VALUES('小明',20,'男')";
                  st = conn.createStatement();
                  long l = st.executeLargeUpdate(sql);
                  if (l>0){
                      System.out.println("成功添加了"+l+"行数据");
                  }else {System.out.println("成功添加了"+l+"行数据");
      
                  }
              } catch (ClassNotFoundException e) {
                  System.out.println("导包数据异常");
                  e.printStackTrace();
              } catch (SQLException e) {
                  System.out.println("sql异常");
                  e.printStackTrace();
              } finally {
                  try {
      
                      st.close();
                      conn.close();
                  } catch (SQLException e) {
                      System.out.println("资源释放异常");;
                  }
              }
      
          }
      }
      
      ```

      

   3. 修改功能

      ```java
      import java.sql.Connection;
      import java.sql.DriverManager;
      import java.sql.SQLException;
      import java.sql.Statement;
      
      public class xiugai {
          public static void main(String[] args) {
              Connection conn = null;
              Statement st = null;
      
              try {
                  Class.forName("com.mysql.jdbc.Driver");
                  String url="jdbc:mysql://localhost:3306/student?characterEncoding=utf8&useSSl=false";
                  String user="root";
                  String possword="root";
                  conn = DriverManager.getConnection(url, user, possword);
                  String sql="UPDATE st SET NAME='小白' WHERE age=17";
                  st = conn.createStatement();
                  long l = st.executeLargeUpdate(sql);
                  if (l>0){
                      System.out.println("成功了"+l+"行数据");
                  }else {System.out.println("成功修改了"+l+"行数据");
      
                  }
              } catch (ClassNotFoundException e) {
                  System.out.println("导包数据异常");
                  e.printStackTrace();
              } catch (SQLException e) {
                  System.out.println("sql异常");
                  e.printStackTrace();
              } finally {
                  try {
      
                      st.close();
                      conn.close();
                  } catch (SQLException e) {
                      System.out.println("资源释放异常");;
                  }
              }
      
          }
      }
      
      
      ```

      

   4. 删除功能

      ```java
      import java.sql.Connection;
      import java.sql.DriverManager;
      import java.sql.SQLException;
      import java.sql.Statement;
      
      public class shangchu {
          public static void main(String[] args) {
              Connection conn = null;
              Statement st = null;
      
              try {
                  Class.forName("com.mysql.jdbc.Driver");
                  String url="jdbc:mysql://localhost:3306/student?characterEncoding=utf8&useSSl=false";
                  String user="root";
                  String possword="root";
                  conn = DriverManager.getConnection(url, user, possword);
                  String sql="DELETE FROM st WHERE age=20";
                  st = conn.createStatement();
                  long l = st.executeLargeUpdate(sql);
                  if (l>0){
                      System.out.println("成功删除了"+l+"行数据");
                  }else {System.out.println("成功删除了"+l+"行数据");
      
                  }
              } catch (ClassNotFoundException e) {
                  System.out.println("导包数据异常");
                  e.printStackTrace();
              } catch (SQLException e) {
                  System.out.println("sql异常");
                  e.printStackTrace();
              } finally {
                  try {
      
                      st.close();
                      conn.close();
                  } catch (SQLException e) {
                      System.out.println("资源释放异常");;
                  }
              }
      
          }
      }
      
      
      ```

      

6. 扩展题【预习题】

   1. 能否实现一个类中，用四个方法来实现上面4个类的功能
   2. 能否实现将查询的结果，封装成java对象