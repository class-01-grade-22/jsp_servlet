# 成员类

```java
package bean;

public class Brand {
   private int BrandID;
   private String BrandName;

    @Override
    public String toString() {
        return "Brand{" +
                "BrandID=" + BrandID +
                ", BrandName='" + BrandName + '\'' +
                '}';
    }

    public Brand() {
    }

    public Brand(int brandID, String brandName) {
        BrandID = brandID;
        BrandName = brandName;
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int brandID) {
        BrandID = brandID;
    }

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        BrandName = brandName;
    }
}

```

```java
package bean;

import javax.xml.crypto.Data;
import java.util.Date;

public class CarDetail {
   private int CID;
   private String Cname;
   private String Content;
   private Date ltime;
   private int price;
   private int BrandID;
   private String BrandName;

    public String getBrandName() {
        return BrandName;
    }

    public void setBrandName(String brandName) {
        this.BrandName = brandName;
    }

    @Override
    public String toString() {
        return "CarDetail{" +
                "CID=" + CID +
                ", Cname='" + Cname + '\'' +
                ", Content='" + Content + '\'' +
                ", ltime=" + ltime +
                ", price=" + price +
                ", BrandID=" + BrandID +
                ", BrandName='" + BrandName + '\'' +
                '}';
    }

    public CarDetail() {
    }

    public CarDetail(int CID, String cname, String content, Date ltime, int price, int brandID, String brandName) {
        this.CID = CID;
        Cname = cname;
        Content = content;
        this.ltime = ltime;
        this.price = price;
        BrandID = brandID;
        this.BrandName = brandName;
    }

    //  public CarDetail(int CID, String cname, String content, Date ltime, int price, int brandID) {
  //      this.CID = CID;
  //      Cname = cname;
  //      Content = content;
  //      this.ltime = ltime;
  //      this.price = price;
  //      BrandID = brandID;
  //  }

    public int getCID() {
        return CID;
    }

    public void setCID(int CID) {
        this.CID = CID;
    }

    public String getCname() {
        return Cname;
    }

    public void setCname(String cname) {
        Cname = cname;
    }

    public String getContent() {
        return Content;
    }

    public void setContent(String content) {
        Content = content;
    }

    public Date getLtime() {
        return ltime;
    }

    public void setLtime(Date ltime) {
        this.ltime = ltime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int brandID) {
        BrandID = brandID;
    }
}

```

# 工具类

```java
package utils;

import org.gjt.mm.mysql.Driver;

import java.sql.*;

public class DButil {
    //1.定义数据地址
   public static final String url="jdbc:mysql:///CarInfo?characterEncoding=utf8";
    public static final String name="root";
    public static final String psw="root";

    //2.注册驱动
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    //3.获取连接对象
    public  static Connection getconn(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, name, psw);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }
    //通用查询方法
    public  static  ResultSet  se(String sql,Object...keys ){
        Connection conn = getconn();
        ResultSet rs = null;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length ; i++) {
                pst.setObject((i+1),keys[i]);
            }
            rs = pst.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return rs;
    }
    public  static  int  up(String sql,Object...keys ) {
        Connection conn = getconn();
        int a;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i + 1), keys[i]);
            }
            a = pst.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return a;
    }
    public  static void close(Connection conn,PreparedStatement pst,ResultSet rs){
        try {
            if (rs!=null){
                rs.close();
            }
            if (pst!=null){
                pst.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    }


```

# dao类

```java
package dao;

import bean.CarDetail;
import utils.DButil;

import javax.xml.crypto.Data;
import java.awt.*;
import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class catdao {
    public ArrayList<CarDetail> getcar() {
        //定义一个集合
        ArrayList<CarDetail> List;
        try {
            List = new ArrayList<>();
            //输入需要的代码
            String sql = "select * from CarDetail c , brand b where c.BrandID=b.BrandID";;
            //使用工具类
            ResultSet rs = DButil.se(sql);
            //遍历输出结果
            while (rs.next()) {
                int CID = rs.getInt("CID");
                String Cname = rs.getString("cname");
                String Content = rs.getString("Content");
                Date ltime = rs.getDate("ltime");
                int price = rs.getInt("price");
                int BrandID = rs.getInt("BrandID");
                String brandname = rs.getString("BrandName");
                CarDetail car = new CarDetail(CID, Cname, Content, ltime, price, BrandID,brandname);
                List.add(car);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return List;
    }
  public int addcar(CarDetail car){
        String sql="INSERT INTO CarDetail VALUES(?,?,?,?,?,?)";
        int up = DButil.up(sql,car.getCID(),car.getCname(),car.getContent(),car.getLtime(),car.getPrice(),car.getBrandID());
        return up;

        }
  }


```

```java
package dao;

import bean.Brand;
import utils.DButil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ping {
    public ArrayList<Brand> getband(){
        ArrayList<Brand> br = new ArrayList<>();
        try {
            String sql="SELECT * FROM Brand";
            ResultSet se = DButil.se(sql);
            while (se.next()){
                int brandID = se.getInt("BrandID");
                String BrandName = se.getString("BrandName");
                Brand BRAND = new Brand(brandID, BrandName);
                br.add(BRAND);

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return br;
    }
}

```

# servlet

```java
package servlet;

import bean.Brand;
import bean.CarDetail;
import dao.catdao;
import dao.ping;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@WebServlet("/test/*")
public class catservlete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        String path = req.getPathInfo();
        if (path==null||path.equals("/")){
            ArrayList<CarDetail> list = new catdao().getcar();
            req.setAttribute("list",list);
            req.getRequestDispatcher("/WEB-INF/test.jsp").forward(req,resp);
        }else if (path.equals("/add")){
            ArrayList<Brand> list = new ping().getband();
            req.setAttribute("list",list);
            req.getRequestDispatcher("/WEB-INF/add.jsp").forward(req,resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        // 2 获取/*具体的路径
        String path = req.getPathInfo();
        // 3 判断路径，做相应的处理
        if (path.equals("/save")){
            // 获取表单的数据
            String name = req.getParameter("name");
            int brandid2 = Integer.parseInt(req.getParameter("brandid"));
            String content = req.getParameter("content");
            int price = Integer.parseInt(req.getParameter("price"));
            String date = req.getParameter("date");  //date = 2023-06-09
            // 得到的时间是字符串，要解析成时间类型
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date newte = null;
            try {
                newte = simpleDateFormat.parse(date);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            // 封装
            CarDetail car = new CarDetail(0, name, content, newte, price, brandid2, null);
            // 调用DAO
            int i = new catdao().addcar(car);
            if (i>0){
                // response.sendRedirect("/car");
                req.setAttribute("msg","添加成功");
                req.getRequestDispatcher("/WEB-INF/msg.jsp").forward(req,resp);
            }else{
                req.setAttribute("msg","添加失败");
                req.getRequestDispatcher("/WEB-INF/msg.jsp").forward(req,resp);
            }
        }

    }
}

```

# 网页jsp

```html
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%--
  Created by IntelliJ IDEA.
  User: 24727
  Date: 2023/6/4
  Time: 21:46
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加</title>
</head>
<body >
<form action="/test/save" method="post">
<table border="1">
  <tr>
    <th>车辆名称</th>
    <th><input type="text" name="name"></th>
  </tr>
  <tr>
    <th>所属品牌</th>
    <td>
      <select name="brandid" id="" >

        <option value="">请选择</option>
        <c:forEach items="${list}" var="brand">
          <option value="${brand.getBrandID()}">${brand.getBrandName()}</option>
        </c:forEach>
      </select>
    </td>
  </tr>
  <tr>
    <td>车辆介绍</td>
    <td><input type="text" name="content"></td>
  </tr>
  <tr>
    <td>价格</td>
    <td><input type="text" name="price"></td>
  </tr>
  <tr>
    <td>录入时间</td>
    <td><input type="date" name="date"></td>
  </tr>
  <tr>
    <td></td>
    <td><input type="submit" value="确定"></td>
  </tr>
</table>
</form>
</body>
</html>

```

```html
<%--
  Created by IntelliJ IDEA.
  User: 24727
  Date: 2023/6/5
  Time: 21:00
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>${msg}</h1>
</body>
</html>

```

```html
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 24727
  Date: 2023/6/4
  Time: 19:39
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<a href="/test//add"><button>添加</button></a>
<table>
<tr>
    <th>序号</th>
    <th>车型名称</th>
    <th>车型简介</th>
    <th>录入时间</th>
    <th>参加价格</th>
    <th>所属品牌编号</th>
</tr>
<c:forEach items="${list}" var="car">
<tr>
    <td>${car.CID}</td>
    <td>${car.cname}</td>
    <td>${car.content}</td>
    <td>${car.ltime}</td>
    <td>${car.price}</td>
    <td>${car.brandName}</td>
</tr>
</c:forEach>
</table>
</body>
</html>

```

