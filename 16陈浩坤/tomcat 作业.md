```markdown
# 作业，
 * 1 数据库相关的操作，使用封装的工具类
 * 2 编写四个servlet，使用注解指定相关的访问路径，分别对应查询，修改，删除，添加的操作
 * 3 从浏览器中，访问这中个路径，显示响应的信息，查询显示结果，其它的显示成功或是失败
 * 4 预习题：如何通过浏览器传送请求参数给servlet，servlet如何接收这些参数，并使用这些参数，去影响数据库的操作？

```

# mysql库

```mysql
CREATE DATABASE student CHARSET utf8;
use student;
CREATE TABLE st (
age int,
name VARCHAR(20),
sex VARCHAR(2)
)
INSERT into st VALUES
(18,'李华','女'),
(20,'小明','男'),
(17,'小白','男')
```

# 方法

```java
package servlet;

import java.sql.*;



import java.sql.*;

    public class quan {
        private static final String url="jdbc:mysql:///student?useSSL=false&useUnicode=true&characterEncoding=utf8";
        private static final String name="root";
        private static final String psw="root";
        static {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }

        }
        public static Connection getci() throws SQLException {
            Connection conn = DriverManager.getConnection(url, name, psw);
            return  conn;
        }
        public static ResultSet sel(String sql, String ...kiss) throws SQLException {
            Connection conn = getci();
            PreparedStatement st = conn.prepareStatement(sql);
            for (int i = 0; i <kiss.length ; i++) {
                st.setString((i+1),kiss[i]);
            }
            ResultSet rs = st.executeQuery();
            return rs;
        }
        public static int up(String sql,String ...kiss) throws SQLException {
            Connection conn = getci();
            PreparedStatement st = conn.prepareStatement(sql);
            for (int i = 0; i <kiss.length ; i++) {
                st.setString((i+1),kiss[i]);
            }
            int i = st.executeUpdate();
            return i;
        }
        public static void guan( Connection conn,PreparedStatement st, ResultSet rs){
            try {
                if (conn!=null){
                    conn.close();
                }
                if (rs!=null){
                    rs.close();
                }
                if (st!=null){
                    st.close();
                }
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

    }

```



# 查询

```java
package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
@WebServlet("/stu")
public class StudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
      out.write("<table border=1>");
      out.write("<tr><td>"+"年龄"+"</td><td>"+"姓名"+"</td><td>"+"性别"+"</td></tr>");
        String sql="SELECT * FROM  st";
        try {
            ResultSet sel = quan.sel(sql);
            while (sel.next()){
                int age=sel.getInt("age");
                String name=sel.getString("name");
                String sex=sel.getString("sex");
                out.write("<tr><td>"+age+"</td><td>"+name+"</td><td>"+sex+"</td></tr>");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

```

# 修改

```java
package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
@WebServlet("/up")
public class update extends StudentServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        String sql = "UPDATE st SET name='大笔都' WHERE age=17";
        try {
            int i = quan.up(sql);
            if (i > 1) {
                out.write("成功修改了" + i + "行数据");
            } else {
                out.write("成功修改了" + i + "行数据");

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}


```

# 删除

```java
package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
@WebServlet("/de")
public class delet extends StudentServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        String sql = "DELETE FROM st WHERE name='小土'";
        try {
            int i = quan.up(sql);
            if (i > 1) {
                out.write("成功删除了" + i + "行数据");
            } else {
                out.write("成功删除了" + i + "行数据");

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}


```



# 添加



```java
package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet("/ti")
public class tian extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        String sql = "INSERT into st VALUES(21,'小土','男')";
        try {
            int i = quan.up(sql);
            if (i > 0) {
                out.write("成功添加了" + i + "行数据");
            } else {
                out.write("成功添加了" + i + "行数据");

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

```

