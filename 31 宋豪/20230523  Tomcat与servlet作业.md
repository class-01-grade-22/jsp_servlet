```java
package servlet;

import util.uptil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

//Servlet类继承HttpServlet，这个Servlet 官方不是自带的！所以要导包！
@WebServlet("/selet")
public class StudentServlet extends HttpServlet {
    //导包后要重写两个方法：一个是doGet 另一个是doPost

    //让浏览器通过 网址路径 访问到我们指定类的方法 有两种形式：一种是xml配置文件：另一种是注解的形式（主要）！


    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        // http默认发起的是get请求
        System.out.println("我收到了请求，ok！");//直接在控制台打印！

        //为了避免乱码，要在响应之前设置响应的文档的类型，告诉浏览器要以什么的形式来接收和显示这个内容！
        resp.setContentType("text/html;charset=utf-8");

       
        //如果要将显示的信息打印在浏览器上，就要通过响应的对象！
        PrintWriter out = resp.getWriter(); //得到一个输出流对象！
//        out.write("我收到了请求，ok！666"); //将这句话响应给浏览器。


        out.write("<style>" +
                "  table{" +
                "    width: 500px;" +
                "    border-collapse: collapse;" +
                "  }" +
                "</style>" +
                "  <table border=1>");
        out.write("<tr><th>编号</th><th>姓名</th><th>性别</th></tr>");
        
        // 下一步，使用uptil工具类，去将数据库中数据读取出来！
        String sql="select * from student";
        try {
            ResultSet rs = uptil.select(sql);
            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
//                out.write("编号"+id+","+name+","+sex+"<br>");
                out.write("<tr><td>"+id+"</td><td>"+name+"</td><td>"+sex+"</td></tr>");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        out.write("</border>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
```