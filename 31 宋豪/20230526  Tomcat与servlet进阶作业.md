```java
//数据库：


CREATE DATABASE student_db CHARSET utf8;
USE student_db;

CREATE TABLE student(
id  int,
name  VARCHAR(20),
sex  VARCHAR(20)
);

INSERT INTO student VALUES
(1,"张三","男"),
(2,"李四","女"),
(3,"王五","男");
```

```java
//工具类：


import java.sql.*;

public class uptil {
        private static final String url="jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
        private static final String usename="root";
        private static final String password="root";

        static {
            try {
                Class.forName("com.mysql.jdbc.Driver");
            } catch (ClassNotFoundException e) {
                throw new RuntimeException(e);
            }
        }

        public static Connection getconn() throws SQLException {
            Connection conn = DriverManager.getConnection(url,usename,password);
            return conn;
        }
        public static ResultSet select(String sql, String ...keys) throws SQLException {
            Connection conn = getconn();
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setString((i+1),keys[i]);
            }
            ResultSet rs = pst.executeQuery();
            return rs;
        }
        public static int update(String sql,String ...keys) throws SQLException {
            Connection conn = getconn();
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setString((i+1),keys[i]);
            }
            int i = pst.executeUpdate();
            return i;
        }
        public static void close(Connection conn,ResultSet rs,PreparedStatement pst){
            try {
                if (conn!=null){
                    conn.close();
                }
                if (rs!=null){
                    rs.close();
                }
                if (pst!=null){
                    pst.close();
                }
            } catch (SQLException e) {
                System.out.println("数据释放异常");
            }
        }
    }

```

```java
//查询：

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/SelectServlet")
public class SelectServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("已成功请求！");

        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

        response.getWriter().write("<style>\n" +
                "  table{\n" +
                "    width: 500px;\n" +
                "    border-collapse: collapse;\n" +
                "  }\n" +
                "</style>\n" +
                "<table border=\"1\">");
        response.getWriter().write("<tr><th>编号</th><th>姓名</th><th>性别</th></tr>");
        String sql="select * from student";
        try {
            ResultSet rs = uptil.select(sql);
            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                response.getWriter().write("<tr><td>"+id+"</td><td>"+name+"</td><td>"+sex+"</td></tr>");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        response.getWriter().write("</border>");
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

```

```java
//添加学生：

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/addServlet")
public class addServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("已成功请求！");
        //避免乱码，设置编码！提前打出！
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

        //servlet中如何获取请求中的参数 request.getParameter（要接收表单项的名称 name）
        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String sex = request.getParameter("sex");

        //调试的时候最好将接收的参数打印一下
        System.out.println("id="+id);
        System.out.println("name="+name);
        System.out.println("sex="+sex);

        //获取浏览器传递的参数后，就可以把参数放入sql语句！
        String sql="insert into student values(?,?,?)";
        try {
            int i = uptil.update(sql,id,name,sex);
            if (i>0){
                response.getWriter().write("添加成功！");
            }else {
                response.getWriter().write("添加失败");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //因为add.html中method的运行方式是POST所以转型与post
        doGet(request,response);
    }
}

```

```java
//删除：

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/deleteServlet")
public class deleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        System.out.println("已请求成功！");

        //避免乱码，设置编码！提前打出！
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");

        //servlet中如何获取请求中的参数 request.getParameter（要接收表单项的名称 name）
        String id = request.getParameter("id");

        //调试的时候最好将接收的参数打印一下！
        System.out.println("id="+id);

        //获取浏览器传递的参数后，就可以把参数放入sql语句！
        String sql="delete from student where id=?";
        try {
            int i = uptil.update(sql,id);
            if (i>0){
                response.getWriter().write("删除成功！");
            }else {
                response.getWriter().write("删除失败！");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //因为delete.html中method的运行方式是POST所以转型与post
        doGet(request,response);
    }
}

```

```html
//添加学生的html

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加学生</title>
</head>
<body>
<h1>添加学生</h1>
<form action="/addServlet" method="post">
    编号：<input type="text" name="id"> <br>
    姓名：<input type="text" name="name"><br>
    性别：<input type="text" name="sex"><br>
    <input type="submit">
</form>

</body>
</html>
```

```html
//删除学生的html

<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>删除学生</title>
</head>
<body>
<h1>删除学生</h1>
<form action="/deleteServlet" method="post">
      删除的学生编号：<input type="text" name="id"><br>
      <input type="submit">
</form>
</body>
</html>
```