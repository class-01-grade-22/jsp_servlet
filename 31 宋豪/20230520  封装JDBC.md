```java
package util;

import java.sql.*;

public class ty {
    private static final String url="jdbc:mysql://localhost:3306/student?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String name="root";
    private static final String password="root";

    //0.导包
    //    1.注册驱动
    static{
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    //      1.5 编写主机地址，数据库名，关闭SSL，指定字符集和编码

    //      2.获取连接的对象，并用conn对象保存

    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url,name,password);
        return conn;
    }
    public static ResultSet select(String sql,String ...keys) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        // 给？号赋值
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+1),keys[i]);
        }
        ResultSet rs = pst.executeQuery();
        return rs;
    }
    public static int update(String sql,String ...keys) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        // 给？号赋值
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+1),keys[i]);
        }
        int i = pst.executeUpdate();
        return i;
    }
    public static void close(ResultSet rst,PreparedStatement pst,Connection conn){
        try {
            if (rst!=null){
                rst.close();
            }
            if (pst!=null){
                pst.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            System.out.println("数据释放异常");
        }

    }
}

```

```java
//查询：

package util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class test {
    public static void main(String[] args) throws SQLException {
        Connection conn = ty.getConn();
        String name = "老大";
        String name2 = "老四";
        String sql = "select * from stu where name=? or name=?";
        ResultSet rst = ty.select(sql, name, name2);
        while (rst.next()) {
            int id = rst.getInt("id");
            String names = rst.getString("name");
            String pwd = rst.getString("password");
            System.out.println("编号：" + id + "," + names + "," + pwd);
            ty.close(rst,null,conn);
            
        }
    }
}
```

```java
//删除：
package util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class test {
    public static void main(String[] args) throws SQLException {
        String sql = "DELETE FROM stu WHERE name=? or name=? or password=?";
        int i = ty.update(sql, "老二","老大","123");
        if (i > 0) {
            System.out.println("成功删除");
        } else {
            System.out.println("没有任何东西被删除");
        }
    }
}
```

```java
//添加：

package util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class test {
    public static void main(String[] args) throws SQLException {
        String sql = "insert into stu values (?,?,?)";
        int i = ty.update(sql, "21","老十","123456");
        if (i > 0) {
            System.out.println("成功添加");
        } else {
            System.out.println("没有任何东西被添加");
        }
    }
}
```

```java
//修改：

package util;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;

public class test {
    public static void main(String[] args) throws SQLException {
        String sql = "update stu set name=? where id=? ";
        int i = ty.update(sql, "老五","4");
        if (i > 0) {
            System.out.println("成功修改");
        } else {
            System.out.println("没有任何东西被修改");
        }
    }
}
```