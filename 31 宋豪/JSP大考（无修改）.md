创建数据库：

CREATE DATABASE student CHARSET utf8;
USE student;

CREATE TABLE stu(
id int,
name VARCHAR(20),
sex VARCHAR(20),
dz VARCHAR(20)
);
INSERT into stu VALUES
(1,'老八','男','东北'),
(2,'秀儿','女','美国'),
(3,'老狗','女','福建南平'),
(4,'傻逼','男','福建闽大'),
(5,'宋豪','男','汤臣一品');

创建工具类：

```java
package uptil;

import java.sql.*;

public class Util {
    private static final String url="jdbc:mysql://localhost:3306/student?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String usename="root";
    private static final String password="root";
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    public static Connection getConn(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, usename, password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }
    public static ResultSet select(String sql,Object ...keys){
        Connection conn = getConn();
        ResultSet rs = null;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            rs = pst.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return rs;
    }
    public static int update(String sql,Object ...keys){
        Connection conn = getConn();
        int num = 0;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            num = pst.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return num;
    }
    public static void close(ResultSet rs,Connection conn,PreparedStatement pst){
        try {
            if(rs!=null){
                rs.close();
            }
            if(conn!=null){
                conn.close();
            }
            if(pst!=null){
                pst.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
```

创建dao类：

```java
package dao;

import student.Student;
import uptil.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentDao {
    //查看所有学生：
    public ArrayList<Student> getAllStudent(){
        String sql="select * from stu";
        ArrayList<Student> list = new ArrayList<>();
        ResultSet rs = Util.select(sql);
        try {
            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                String dz = rs.getString("dz");
                Student stu = new Student(id, name, sex, dz);
                list.add(stu);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            Util.close(rs,null,null);
        }
        return list;
    }
    //根据id查看某个学生：
    public Student selectById(int id){
        String sql="select * from stu where id=?";
        Student stu=null;
        ResultSet rs = Util.select(sql,id);
        try {
            while (rs.next()){
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                String dz = rs.getString("dz");
                stu = new Student(id, name, sex, dz);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return stu;
    }
    //根据ID删除某个学生：
    public int deleteById(int id){
        String sql="delete from stu where id=?";
        int i = Util.update(sql, id);
        return i;
    }
    //根据id修改某个学生：
    public int updateById(Student student){
        String sql="update stu set name=?,sex=?,dz=? where id=?";
        int i = Util.update(sql, student.getName(), student.getSex(), student.getDz(), student.getId());
        return i;
    }
    //添加学生：
    public int addById(Student student){
        String sql="insert into stu values(?,?,?,?)";
        int i = Util.update(sql, student.getId(), student.getName(), student.getSex(), student.getDz());
        return i;
    }
}
```

创建servlet：

```java
package servlet;

import dao.StudentDao;
import student.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/student/*")
public class StudentCon extends HttpServlet {
    StudentDao dao = new StudentDao();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getPathInfo();
        System.out.println("path = " + path);
        if (path==null||path.equals("/")){
//            System.out.println("/student/ 查看所有学生");
            StudentDao dao = new StudentDao();
            ArrayList<Student> list = dao.getAllStudent();
            request.setAttribute("list",list);
            request.getRequestDispatcher("/editById.jsp").forward(request,response);
        } else if (path.matches("/\\d+")) {
            int id = Integer.parseInt(path.substring(1));
//            System.out.println("/student/111 查看");
            Student stu = new StudentDao().selectById(id);
            request.setAttribute("stu",stu);
            request.getRequestDispatcher("/selectById.jsp").forward(request,response);
        } else if (path.matches("/update/\\d+")) {
            String id = path.substring(8);
            System.out.println("修改");
        } else if (path.matches("/delete/\\d+")) {
            int id = Integer.parseInt(path.substring(8));
//            System.out.println("删除");
            dao.deleteById(id);
            response.sendRedirect("/student");
        } else if (path.matches("/add")) {
//            System.out.println("添加");
            request.getRequestDispatcher("/addById.jsp").forward(request,response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getPathInfo();
        System.out.println("path = " + path);
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        if (path.equals("/save")){
            int id = Integer.parseInt(request.getParameter("id"));
            String name = request.getParameter("name");
            String sex = request.getParameter("sex");
            String dz = request.getParameter("dz");
            Student stu = new Student(id,name,sex,dz);
            int i = new StudentDao().addById(stu);
            if (i>0){
                request.setAttribute("msl","添加成功");
                request.getRequestDispatcher("/msl.jsp").forward(request,response);
            }else {
                request.setAttribute("msl","添加失败");
                request.getRequestDispatcher("/msl.jsp").forward(request,response);
            }
        }
    }
}
```

查看所有学生的页面加删除：

```html
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生页面</title>
</head>
<body>
<style>
    table{
        border-collapse: collapse;
        text-align: center;
    }
</style>
<h1>学生页面</h1>
<hr>
<table border="1" width="500px">
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>地址</th>
        <th>操作</th>
    </tr>
    <c:forEach var="stu" items="${list}">
        <tr>
            <td>${stu.id}</td>
            <td>${stu.name}</td>
            <td>${stu.sex}</td>
            <td>${stu.dz}</td>
            <td>
                <a href="/student/${stu.id}">查看</a>
                <a href="/student/update/${stu.id}">修改</a>
                <a href="/student/delete/${stu.id}" onclick="return confirm('确定要永久删除吗？')">删除</a>
            </td>
        </tr>
    </c:forEach>
</table>
<hr>
<a href="/student/add">添加同学</a>
</body>
</html>
```

添加学生的jsp：

```html

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加学生</title>
</head>
<body>
<h1>添加学生</h1>
<hr>
<form action="/student/save" method="post">
<table>
    <tr><th>编号</th><td><input type="text" name="id"></td></tr>
    <tr><th>姓名</th><td><input type="text" name="name"></td></tr>
    <tr><th>性别</th><td><input type="text" name="sex"></td></tr>
    <tr><th>地址</th><td><input type="text" name="dz"></td></tr>
    <td colspan="2"><center><input type="submit" value="提交"></center></td>
</table>
</form>
</body>
</html>

```

```html

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>add的提示信息</title>
</head>
<body>
<h1>${msl}</h1>
<hr>
<a href="/student">返回列表</a>
</body>
</html>
```

查看学生的jsp：

```html
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title></title>
</head>
<body>
<h1>正在查看${stu.name}学生</h1>
<hr>
    <table>
        <tr><th>编号：</th><td>${stu.id}</td></tr>
        <tr><th>姓名：</th><td>${stu.name}</td></tr>
        <tr><th>性别：</th><td>${stu.sex}</td></tr>
        <tr><th>地址：</th><td>${stu.dz}</td></tr>
    </table>
<hr>
<a href="/student">返回列表</a>
</body>
</html>
```

输入学生姓名查找信息：

```java
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生页面</title>
</head>
<body>
<style>
    table{
        border-collapse: collapse;
        text-align: center;
    }
</style>
<h1>学生页面</h1>
<form action="/student/search" method="post">
    请输入要查询的姓名：<input type="text" name="username"><input type="submit" value="搜索">
</form>
<hr>
<table border="1" width="500px">
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>地址</th>
        <th>操作</th>
    </tr>
    <c:forEach var="stu" items="${list}">
        <tr>
            <td>${stu.id}</td>
            <td>${stu.name}</td>
            <td>${stu.sex}</td>
            <td>${stu.dz}</td>
            <td>
                <a href="/student/${stu.id}">查看</a>
                <a href="/student/update/${stu.id}">修改</a>
                <a href="/student/delete/${stu.id}" onclick="return confirm('确定要永久删除吗？')">删除</a>
            </td>
        </tr>
    </c:forEach>
</table>
<hr>
<a href="/student/add">添加同学</a>
</body>
</html>
```

```java
package dao;

import student.Student;
import uptil.Util;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentDao {
    //查看所有学生：
    public ArrayList<Student> getAllStudent(){
        String sql="select * from stu";
        ArrayList<Student> list = new ArrayList<>();
        ResultSet rs = Util.select(sql);
        try {
            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                String dz = rs.getString("dz");
                Student stu = new Student(id, name, sex, dz);
                list.add(stu);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            Util.close(rs,null,null);
        }
        return list;
    }
    //根据id查看某个学生：
    public Student selectById(int id){
        String sql="select * from stu where id=?";
        Student stu=null;
        ResultSet rs = Util.select(sql,id);
        try {
            while (rs.next()){
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                String dz = rs.getString("dz");
                stu = new Student(id, name, sex, dz);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return stu;
    }
    //根据ID删除某个学生：
    public int deleteById(int id){
        String sql="delete from stu where id=?";
        int i = Util.update(sql, id);
        return i;
    }
    //根据id修改某个学生：
    public int updateById(Student student){
        String sql="update stu set name=?,sex=?,dz=? where id=?";
        int i = Util.update(sql, student.getName(), student.getSex(), student.getDz(), student.getId());
        return i;
    }
    //添加学生：
    public int addById(Student student){
        String sql="insert into stu values(?,?,?,?)";
        int i = Util.update(sql, student.getId(), student.getName(), student.getSex(), student.getDz());
        return i;
    }
    //输入所要查看的姓名：
    public ArrayList<Student> searchStudent(String username){
        String sql="select * from stu where name like ? ";
        ArrayList<Student> list = new ArrayList<>();
        ResultSet rs = Util.select(sql,"%"+username+"%");
        try {
            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                String dz = rs.getString("dz");
                Student stu = new Student(id, name, sex, dz);
                list.add(stu);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }finally {
            Util.close(rs,null,null);
        }
        return list;
    }
}
```

```java
package servlet;

import dao.StudentDao;
import student.Student;
import uptil.Util;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/student/*")
public class StudentCon extends HttpServlet {
    StudentDao dao = new StudentDao();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getPathInfo();
        System.out.println("path = " + path);
        if (path==null||path.equals("/")){
//            System.out.println("/student/ 查看所有学生");
            StudentDao dao = new StudentDao();
            ArrayList<Student> list = dao.getAllStudent();
            request.setAttribute("list",list);
            request.getRequestDispatcher("/editById.jsp").forward(request,response);
        } else if (path.matches("/\\d+")) {
            int id = Integer.parseInt(path.substring(1));
//            System.out.println("/student/111 查看");
            Student stu = new StudentDao().selectById(id);
            request.setAttribute("stu",stu);
            request.getRequestDispatcher("/selectById.jsp").forward(request,response);
        } else if (path.matches("/update/\\d+")) {
            String id = path.substring(8);
            System.out.println("修改");
        } else if (path.matches("/delete/\\d+")) {
            int id = Integer.parseInt(path.substring(8));
//            System.out.println("删除");
            dao.deleteById(id);
            response.sendRedirect("/student");
        } else if (path.matches("/add")) {
//            System.out.println("添加");
            request.getRequestDispatcher("/addById.jsp").forward(request,response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String path = request.getPathInfo();
        System.out.println("path = " + path);
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        if (path.equals("/save")){
            int id = Integer.parseInt(request.getParameter("id"));
            String name = request.getParameter("name");
            String sex = request.getParameter("sex");
            String dz = request.getParameter("dz");
            Student stu = new Student(id,name,sex,dz);
            int i = new StudentDao().addById(stu);
            if (i>0){
                request.setAttribute("msl","添加成功");
                request.getRequestDispatcher("/msl.jsp").forward(request,response);
            }else {
                request.setAttribute("msl","添加失败");
                request.getRequestDispatcher("/msl.jsp").forward(request,response);
            }
        } else if (path.equals("/search")) {
            String username = request.getParameter("username");
            ArrayList<Student> list = new StudentDao().searchStudent(username);
            request.setAttribute("list",list);
            request.getRequestDispatcher("/editById.jsp").forward(request,response);
        }
    }
}
```