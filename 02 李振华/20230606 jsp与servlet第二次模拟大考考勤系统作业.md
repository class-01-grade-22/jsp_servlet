# MySQL代码



```mysql
create database AttDB charset utf8;
use AttDB;
create table Student(
	sid	int		primary key auto_increment,#学号 主键,自动增长列 
	sname	varchar(20)		unique key not null#学生姓名 唯一,非空
);
#表：Attence (考勤表)
#字段显示	字段名	数据类型	默认值	备注和说明

create table Attence(
	aid	  int		primary key auto_increment,#考勤编号 主键,自动增长列
	time  varchar(20)		not null,#出勤时间 非空
	type	int		comment "1:已到；2:迟到；3旷课",#出勤状况1:已到；2:迟到；3旷课
	sid	  int,
	foreign key(sid) REFERENCES Student(sid)		#学号外键
);
insert into student VALUES
(0,"张三"),
(0,"李四"),
(0,"王五");
insert into attence VALUES
(0,"2022-05-20 08:20:00",1,1),
(0,"2022-05-23 08:20:00",2,1),
(0,"2022-05-23 13:40:00",2,2),
(0,"2022-05-27 08:20:00",3,2),
(0,"2022-05-30 08:20:00",2,3);
```

#  bean

```java
package bean;

public class Student {
   private int sid;
   private String sname;

    public Student() {
    }

    public Student(int sid, String sname) {
        this.sid = sid;
        this.sname = sname;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    @Override
    public String toString() {
        return "Student{" +
                "sid=" + sid +
                ", sname='" + sname + '\'' +
                '}';
    }
}

-----------------------------------------------------------------------------------------------------------------
package bean;

public class Attence {
   private int aid;
   private String time;
   private int type;
   private int sid;
   private String sname;

    public Attence() {
    }

    public Attence(int aid, String time, int type, int sid, String sname) {
        this.aid = aid;
        this.time = time;
        this.type = type;
        this.sid = sid;
        this.sname = sname;
    }

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    @Override
    public String toString() {
        return "Attence{" +
                "aid=" + aid +
                ", time='" + time + '\'' +
                ", type=" + type +
                ", sid=" + sid +
                ", sname='" + sname + '\'' +
                '}';
    }
}

```

# 工具类代码

```java
package utils;

import java.sql.*;

public class DBUtil {
    private static final String url="jdbc:mysql:///attdb?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String username="root";
    private static final String password="root";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("驱动注册失败");
            throw new RuntimeException(e);
        }
    }

    public static Connection getConn(){
        Connection conn=null;
        try {
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }
    public static void close(Connection conn, PreparedStatement pst, ResultSet rs){
        try {
            if (rs!=null){
                rs.close();
            }
            if (pst!=null){
                pst.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public static ResultSet query(String sql,Object ...keys){
        Connection conn = getConn();
        ResultSet rs=null;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            rs = pst.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return rs;
    }
    public static int update(String sql,Object ...keys){
        Connection conn = getConn();
        int num=0;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            num = pst.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return num;
    }
}

```

# dao

```java
package dao;

import bean.Student;
import utils.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentDao {
    public ArrayList<Student> getAllStu(){
        ArrayList<Student> list = new ArrayList<>();
        String sql="select * from Student";
        ResultSet rs = DBUtil.query(sql);
        try {
            while (rs.next()){
                int sid = rs.getInt("sid");
                String sname = rs.getString("sname");
                Student stu = new Student(sid, sname);
                list.add(stu);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(null,null,rs);
        }
        return list;
    }
}
-------------------------------------------------------------------------------------------------------------------



package dao;

import bean.Attence;
import utils.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class AttenceDao {
    public ArrayList<Attence> getAllAtt(){
        ArrayList<Attence> list = new ArrayList<>();
        String sql="SELECT * FROM attence a,student s WHERE a.sid=s.sid order by aid ";
        ResultSet rs = DBUtil.query(sql);
        try {
            while (rs.next()){
                int aid = rs.getInt("aid");
                String time = rs.getString("time");
                int type = rs.getInt("type");
                int sid = rs.getInt("sid");
                String sname = rs.getString("sname");
                Attence att = new Attence(aid, time, type, sid,sname);
                list.add(att);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(null,null,rs);
        }
        return list;
    }


}

```

# servlet

```java
package servlet;

import bean.Attence;
import bean.Student;
import dao.AttenceDao;
import dao.StudentDao;
import utils.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet( "/stu/*")
public class StuController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String path = request.getPathInfo();
        if (path==null || path.equals("/")){
            ArrayList<Attence> list = new AttenceDao().getAllAtt();
            request.setAttribute("list",list);
            request.getRequestDispatcher("/WEB-INF/stuList.jsp").forward(request,response);
        }else if (path.equals("/add")){
            ArrayList<Student> list = new StudentDao().getAllStu();
            request.setAttribute("list",list);
            request.getRequestDispatcher("/WEB-INF/stuAdd.jsp").forward(request,response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
  request.setCharacterEncoding("utf-8");
        String path = request.getPathInfo();
        if (path.equals("/save")){
            String sid = request.getParameter("sid");
            String time = request.getParameter("time");
            String type = request.getParameter("type");
            String sql="insert into attence values(?,?,?,?)";
            int i = DBUtil.update(sql, null, time, type, sid);
            if (i>0){
               request.setAttribute("msg","添加成功");
               request.getRequestDispatcher("/WEB-INF/msg.jsp").forward(request,response);
            }else {
                request.setAttribute("msg","添加失败");
                request.getRequestDispatcher("/WEB-INF/msg.jsp").forward(request,response);
            }
        }
    }
}

```

# jsp

```jsp

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>提示信息</title>
</head>
<body>
<h1>${msg}</h1>
<hr>
<a href="/stu">返回列表</a>
</body>
</html>
---------------------------------------------------------------------
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加</title>
    <style>
        table{
            border-collapse: collapse;
            width: 400px;
            text-align: center;
        }
        tr,td,th{
            border: 1px  solid;
        }
    </style>
</head>
<body>

<form action="/stu/save" method="post">
    <table>
        <caption><h1>学生考勤系统</h1></caption>

        <tr>
            <th>学生姓名</th>
            <td><select name="sid" id="">
                <option value="">请选择</option>
                <c:forEach items="${list}" var="student">
                    <option value="${student.sid}">${student.sname}</option>
                </c:forEach>
            </select></td>
        </tr>
        <tr>
            <th>考勤时间</th>
            <td><input type="datetime-local" name="time" required></td>
        </tr>
        <tr>
            <th>考勤状况</th>
            <td>
                <%--name要一样--%>
               已到:<input type="radio"id="yd" name="type" value="1" required>
               迟到:<input type="radio"id="cd" name="type" value="2" required>
               旷课:<input type="radio"id="kk" name="type" value="3" required>
            </td>
        </tr>
        <tr>
            <td><input type="submit" value="添加"></td>
        </tr>
    </table>
</form>
</body>
</html>
-------------------------------------------------------------------------------------------------------------------

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>考勤系统</title>
    <style>
        table{
            border-collapse: collapse;
            width: 800px;
            text-align: center;
        }
        tr,td,th{
            border: 1px darkblue solid;
        }
    </style>
</head>
<a href="/stu/add"><button>添加</button></a>
<hr>
<body>
<table>
    <tr>
        <th>考勤编号</th>
        <th>学生编号</th>
        <th>学生姓名</th>
        <th>出勤时间</th>
        <th>出勤状况</th>
    </tr>
    <c:forEach items="${list}" var="att">
        <tr>
            <td>${att.aid}</td>
            <td>${att.sid}</td>
            <td>${att.sname}</td>
            <td>${att.time}</td>
            <td><c:if test="${att.type ==1}">
                已到
            </c:if>
                <c:if test="${att.type ==2}">
                    迟到
                </c:if>
                <c:if test="${att.type ==3}">
                    旷课
                </c:if></td>

        </tr>

    </c:forEach>
</table>

</body>
</html>



```

