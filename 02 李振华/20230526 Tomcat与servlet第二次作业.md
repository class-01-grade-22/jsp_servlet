~~~java
添加
    import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/addServlet")
public class addServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String id=request.getParameter("id");
        String name=request.getParameter("name");
        String sex=request.getParameter("sex");
        System.out.println("id = " + id);
        System.out.println("name = " + name);
        System.out.println("sex = " + sex);
        String sql="insert into student values (?,?,?)";
        try {
            int i=DBUtil.update(sql,id,name,sex);
            if (i>0){
                System.out.println("添加成功");
            }else {
                System.out.println("输入失败");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加学生</title>
</head>
<body>
<h1>添加学生</h1>
<form action="/addServlet" method="get">
    学生编号 <input type="text" name="id"><br><br>
    学生姓名 <input type="text" name="name"><br><br>
    学生性别 <input type="text" name="sex"><br><br>
    <input type="submit">
</form>
</body>
</html>
```

```java
删除
import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/deleteServlet")
public class deleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        String id=request.getParameter("id");
        String sql="delete from student where id=?";
        try {
            int i = DBUtil.update(sql, id);
            if (i>0){
                response.getWriter().write("删除成功");
            }else {
                response.getWriter().write("删除失败");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>删除学生</title>
</head>
<body>
<h1>删除学生</h1>
<form action="/deleteServlet" method="get">
  编号 <input type="text" name="id"><br><br>
  <input type="submit" value="删除">
</form>
</body>
</html>
```

```java
查询
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/test")
public class Test extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        String sql="select * from student";
        resp.setContentType("text/html;charset=utf-8");
        try {
            ResultSet rs=DBUtil.select(sql);
            while(rs.next()){
                int id=rs.getInt(1);
                String name=rs.getString(2);
                String sex=rs.getString(3);
                resp.getWriter().write(id+name+sex);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
~~~



