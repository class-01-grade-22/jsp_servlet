# 增



```java

import java.sql.*;

public class Insert {
    public static void main(String[] args) {
        ResultSet rs = null;
        Statement st = null;
        Connection conn = null;
        try {
            //1 注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            //2 获取连接对象
            String url = "jdbc:mysql:///student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
            String username = "root";
            String password = "root";
            conn = DriverManager.getConnection(url, username, password);

            //3 编写SQL语句
            String sql = "insert into student VALUES(1,'张三','男'),(2,'李四','女'),(3,'王五','男')";
            //4 获取执行SQL的对象
            st = conn.createStatement();
            //5 执行SQL语句
            int i = st.executeUpdate(sql);

            //6 处理返回的结果
           if (i>0){
               System.out.println("成功添加了"+i+"行数");

           }
        } catch (ClassNotFoundException e) {
            System.out.println("驱动导入异常");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("SQL执行异常");
            e.printStackTrace();
        } finally {
            //7 释放资源
            try {

                if (st!=null){
                    st.close();
                }
                if (conn!=null){
                    conn.close();
                }

            } catch (SQLException e) {
                System.out.println("资源释放异常");
            }
        }

    }
}

```

# 删



```java

import java.sql.*;

public class Delete {
    public static void main(String[] args) {
        ResultSet rs = null;
        Statement st = null;
        Connection conn = null;
        try {
            //1 注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            //2 获取连接对象
            String url = "jdbc:mysql:///student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
            String username = "root";
            String password = "root";
            conn = DriverManager.getConnection(url, username, password);

            //3 编写SQL语句
            String sql = "delete from student where id=2 or 3";
            //4 获取执行SQL的对象
            st = conn.createStatement();
            //5 执行SQL语句
            int i = st.executeUpdate(sql);

            //6 处理返回的结果
           if (i>0){
               System.out.println("成功删除了"+i+"行数");
            }else {
               System.out.println("删除了"+i+"行数");
           }
        } catch (ClassNotFoundException e) {
            System.out.println("驱动导入异常");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("SQL执行异常");
            e.printStackTrace();
        } finally {
            //7 释放资源
            try {

                if (st!=null){
                    st.close();
                }
                if (conn!=null){
                    conn.close();
                }

            } catch (SQLException e) {
                System.out.println("资源释放异常");
            }
        }

    }
}

```

# 改



```java
import java.sql.*;

public class Update {
    public static void main(String[] args) {
        ResultSet rs = null;
        Statement st = null;
        Connection conn = null;
        try {
            //1 注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            //2 获取连接对象
            String url = "jdbc:mysql:///student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
            String username = "root";
            String password = "root";
            conn = DriverManager.getConnection(url, username, password);

            //3 编写SQL语句
            String sql = "UPDATE student set name='张三' WHERE id=1;";
            //4 获取执行SQL的对象
            st = conn.createStatement();
            //5 执行SQL语句
            int i = st.executeUpdate(sql);

            //6 处理返回的结果
           if (i>0){
               System.out.println("成功修改了"+i+"行数");

           }
        } catch (ClassNotFoundException e) {
            System.out.println("驱动导入异常");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("SQL执行异常");
            e.printStackTrace();
        } finally {
            //7 释放资源
            try {

                if (st!=null){
                    st.close();
                }
                if (conn!=null){
                    conn.close();
                }

            } catch (SQLException e) {
                System.out.println("资源释放异常");
            }
        }

    }
}

```

# 查



```java
import java.sql.*;

public class Select {
    public static void main(String[] args) {
        ResultSet rs = null;
        Statement st = null;
        Connection conn = null;
        try {
            //1 注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            //2 获取连接对象
            String url = "jdbc:mysql:///student_db?useSSL=false";
            String username = "root";
            String password = "root";
            conn = DriverManager.getConnection(url, username, password);

            //3 编写SQL语句
            String sql = "SELECT * FROM student";
            //4 获取执行SQL的对象
            st = conn.createStatement();
            //5 执行SQL语句
            rs = st.executeQuery(sql);

            //6 处理返回的结果
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                System.out.println(id +"\t" +name +"\t\t"+ sex);
            }
        } catch (ClassNotFoundException e) {
            System.out.println("驱动导入异常");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("SQL执行异常");
            e.printStackTrace();
        } finally {
            //7 释放资源
            try {
               if(rs!=null){
                   rs.close();
               }
                if (st!=null){
                    st.close();
                }
                if (conn!=null){
                    conn.close();
                }

            } catch (SQLException e) {
                System.out.println("资源释放异常");
            }
        }

    }
}

```

