```mysql
create database student_db charset utf8;
use student_db;
create table student(
id int primary key auto_increment,
name varchar(20),
sex varchar(20)    
);
insert into student values
(null,"韩立","男"),
(null,"南宫婉儿","女"),
(null,"厉飞雨","男"),
(null,"墨彩环","女");
```

```java
//封装工具类
package util;

import java.sql.*;

public class DBTool {
    private static final String url = "jdbc:mysql:///student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String name = "root";
    private static final String pwd = "root";


    // 0.导包
    //      1.注册驱动
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    //      1.5 编写主机地址，数据库名，关闭SSL，指定字符集和编码

    //      2.获取连接的对象，并用conn对象保存

    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url, name, pwd);
        return conn;
    }


    // 通用的查询方法
    // ...叫可变参数，表示查以没有参数。也可以表示有任意个参数，其实就是相当是一个数组
    // 将？号对应的值放到数组中，
    public static ResultSet select(String sql, String ...keys) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);//
        // 给？号赋值
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+1),keys[i]);
        }

        ResultSet rs = pst.executeQuery();
        return rs;
    }
    // 通用的删，改，增的方法
    public static int update(String sql,String ...keys) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            // 把数组里第0个下标的值，赋值给第0+1个位置的？号
            pst.setString((i+1),keys[i]);
        }
        int i = pst.executeUpdate();
        return i;
    }

    // 通用的关闭资源的方法
    public static void close(Connection conn,PreparedStatement pst,ResultSet rs){
        try {
            if (rs!=null){
                rs.close();
            }
            if (pst!=null){
                pst.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

```

```java
//查找
package servelt;

import util.DBTool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/test")
public class StudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();

        String sql="SELECT * FROM student";
        try {
            ResultSet rs = DBTool.select(sql);
            while (rs.next()){
                int id=rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                out.write(id+name+sex+"<br>");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

```

```java
//修改
package servelt;

import util.DBTool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/test")
public class StudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();

        String sql="update student set name='张铁' where id=3";
        try {
            int i = Fz.update(sql);
            if (i>0){
                out.write("修改成功");
            }else {
                out.write("修改失败");
            }
        } catch (SQLException e) {
            throwables.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

```

```java
//添加
package servelt;

import util.DBTool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/test")
public class StudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();

        String sql="insert into student values(5,'陈师姐','女'),(6,'耳根','男')";
        try {
            int i = Fz.update(sql);
            if (i>0){
                out.write("添加成功");
            }else {
               out.write("添加失败");
            }
        } catch (SQLException e) {
            throwables.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
```

```java
//删除
package servelt;

import util.DBTool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/test")
public class StudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();

        String sql="delete from student where id = 6";
        try {
            int i = Fz.update(sql);
            if (i>0){
                out.write("删除成功");
            }else {
               out.write("删除失败");
            }
        } catch (SQLException e) {
            throwables.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
```

