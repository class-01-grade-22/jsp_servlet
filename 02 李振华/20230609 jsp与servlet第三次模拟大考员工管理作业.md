# MySQL作业

```mysql
# 数据库名称：CompanyManager
create database if not exists CompanyManager charset utf8;
use CompanyManager;
# 表： Dept (部门信息表)
create table Dept
(
    DeptID   int primary key auto_increment, # 部门编号	主键,自动增长列
    DeptName varchar(20) not null            # 部门名称	不允许为空
);
# 表：Emp (员工信息表)

create table Emp
(
    EmpID    int primary key auto_increment,     # 员工编号主键,自动增长列
    EmpName  varchar(20) not null,               # 员工姓名不允许为空
    Sex      char(2)     not null,               # 性别不允许为空
    Age      int         not null,               # 员工年龄不允许为空
    Tel      varchar(20) not null unique,        # 联系电话	不允许为空（唯一约束）
    PassWord varchar(12) not null,               # 密码不允许为空
    DeptID   int,                                # 部门编号外键，关联部门表DeptID字段
    foreign key (DeptID) references Dept (DeptID)
);
insert into dept values
                        (0,'开发部'),
                        (0,'测试部'),
                        (0,'技术部'),
                        (0,'后勤部');
insert into emp values
                      (0,'张三','男',18,'15078454454',123,1),
                      (0,'李四','女',25,'15078454544',123,2),
                      (0,'王五','男',22,'15065454454',123,4),
                      (0,'张三风','男',26,'15258454454',123,3),
                      (0,'皮五','男',23,'15066454454',123,2),
                      (0,'风姐','女',22,'15011454454',123,1);
```

# bean



```java
package bean;

public class Dept {
   private int id;
   private String name;

    public Dept() {
    }

    public Dept(int id, String name) {
        this.id = id;
        this.name = name;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    @Override
    public String toString() {
        return "Dept{" +
                "id=" + id +
                ", name='" + name + '\'' +
                '}';
    }
}
-------------------------------------------------------------------------------------------------------------------
package bean;

public class Emp {
   private int id;
   private String name;
   private String sex;
   private int age;
   private String tel;
   private String pwd;
   private int deptID;
   private String deptName;

    public Emp() {
    }

    public Emp(int id, String name, String sex, int age, String tel, String pwd, int deptID, String deptName) {
        this.id = id;
        this.name = name;
        this.sex = sex;
        this.age = age;
        this.tel = tel;
        this.pwd = pwd;
        this.deptID = deptID;
        this.deptName = deptName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPwd() {
        return pwd;
    }

    public void setPwd(String pwd) {
        this.pwd = pwd;
    }

    public int getDeptID() {
        return deptID;
    }

    public void setDeptID(int deptID) {
        this.deptID = deptID;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    @Override
    public String toString() {
        return "Emp{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", tel='" + tel + '\'' +
                ", pwd='" + pwd + '\'' +
                ", deptID=" + deptID +
                ", deptName='" + deptName + '\'' +
                '}';
    }
}

```

# 工具类代码

```java
package utils;

import java.sql.*;

public class DBUtil {
    private static final String url="jdbc:mysql:///companymanager?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String username="root";
    private static final String password="root";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("驱动注册失败");
            throw new RuntimeException(e);
        }
    }
    public static Connection getConn()  {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }
    public static void close(Connection conn, PreparedStatement pst, ResultSet rs){
        try {
            if (rs!=null){
                rs.close();
            }
            if (pst!=null){
                pst.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    public static ResultSet qurey(String sql, Object... keys) {
        Connection conn = getConn();
        ResultSet rs = null;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i + 1), keys[i]);
            }
            rs = pst.executeQuery();
            return rs;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public static int update(String sql, Object... keys) {
        Connection conn = getConn();
        int num=0;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i + 1), keys[i]);
            }
            num = pst.executeUpdate();
            return num;
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

}

```

# servlet

```java
package servlet;

import bean.Emp;
import utils.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/list")
public class ListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //1 编写查询所有员工的SQL语句,要连表
        String sql = "select * from emp e,dept d where e.DeptID=d.DeptID";
        //2 调用工具类,将SQL语句给通用的查询方法，得到结果集
        ResultSet rs = DBUtil.qurey(sql);
        //3 创建一个集合,遍历结果集,将结果封装成对象,添加到集合
        ArrayList<Emp> list = new ArrayList<>();
        try {
            while (rs.next()) {
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String sex = rs.getString(3);
                int age = rs.getInt(4);
                String tel = rs.getString(5);
                String password = rs.getString(6);
                int deptID = rs.getInt(7);
                String deptName = rs.getString(9);
                Emp emp = new Emp(id, name, sex, age, tel, password, deptID, deptName);
                list.add(emp);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        //4 将集合添加到request域中
        request.setAttribute("list", list);
        //5  将请求转发给jsp
        request.getRequestDispatcher("/WEB-INF/empList.jsp").forward(request, response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
-------------------------------------------------------------------------------------------------------------------
package servlet;

import bean.Emp;
import utils.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/select")
public class SelectServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
      //1 处理乱码
        request.setCharacterEncoding("utf-8");
      //2 接收表单数据
        String user = request.getParameter("user");
        //3 编写SQL加上表单数据
        String sql="select * from emp e,dept d where e.DeptID=d.DeptID and EmpName like ? ";
        //2 调用工具类,将SQL语句给通用的查询方法，得到结果集
        ResultSet rs = DBUtil.qurey(sql,"%"+user+"%");
        //3 创建一个集合,遍历结果集,将结果封装成对象,添加到集合
        ArrayList<Emp> list = new ArrayList<>();
        try {
            while (rs.next()){
                int id = rs.getInt(1);
                String name = rs.getString(2);
                String sex = rs.getString(3);
                int age = rs.getInt(4);
                String tel = rs.getString(5);
                String password = rs.getString(6);
                int deptID = rs.getInt(7);
                String deptName = rs.getString(9);
                Emp emp = new Emp(id, name, sex, age, tel, password, deptID, deptName);
                list.add(emp);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        //4 将集合添加到request域中
        request.setAttribute("list",list);
        //5  将请求转发给jsp
        request.getRequestDispatcher("/WEB-INF/empList.jsp").forward(request,response);

    }
}

```

# jsp



```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-09
  Time: 11:22
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>员工列表</title>
    <style>
        table{
      border-collapse: collapse;
            width: 500px;
            text-align: center;
        }
        tr,td,th{
            border: 1px solid;
        }
    </style>
</head>
<body>
<form action="/select" method="post">
    姓名:<input type="text"name="user">
    <input type="submit" value="查询">
</form>

<table>
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>年龄</th>
        <th>电话</th>
        <th>所属部门</th>
    </tr>
<c:forEach items="${list}" var="emp">
    <tr>
        <td>${emp.id}</td>
        <td>${emp.name}</td>
        <td>${emp.sex}</td>
        <td>${emp.age}</td>
        <td>${emp.tel}</td>
        <td>${emp.deptName}</td>

    </tr>
</c:forEach>

</table>

</body>
</html>

```

