# MySQL代码

```mysql
# 数据库名称：test
create database test charset utf8;
use test;
# 表：house_type (房屋类型表)
create table house_type
(
    id   int primary key auto_increment, # 编号主键,自动增长列
    type varchar(50) not null            # 房屋类型	不允许为空
);
insert into house_type values
                           (0,'3室1厅'),
                           (0,'3室2厅'),
                           (0,'2室1厅'),
                           (0,'2室2厅');
# 表：house_info (房源信息表)
create table house_info
(
    id             int primary key auto_increment, # 编号主键,自动增长列
    lease_mode     varchar(50),                    # 租赁方式可以为空
    rent           double       not null,          # 租金不允许为空
    contacts       varchar(20),                    # 联系人可以为空
    deposit_method varchar(20),                    # 押金方式可以为空
    house_type_id  int,                            # 房屋类型外键
    address        varchar(200) not null,          # 详细地址不允许为空
    foreign key (house_type_id) references house_type (id)
);

insert into house_info values
                           (0,'整租',2230,'王晓明','押一付三',1,'泉州柳城区1号'),
                           (0,'整租',2180,'王晓明','押一付三',4,'龙岩新罗区2号'),
                           (0,'合租',1120,'张小飞','押一付一',2,'泉州柳城区3号'),
                           (0,'整租',2280,'王晓明','押一付二',3,'泉州柳城区2号'),
                           (0,'合租',1200,'张小飞','押一付一',2,'泉州鲤城区1号');

```

# bean 

```java
package bean;

public class HouseType {
    int id;
    String type;

    public HouseType() {
    }

    public HouseType(int id, String type) {
        this.id = id;
        this.type = type;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    @Override
    public String toString() {
        return "HouseType{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }
}
-------------------------------------------------------------------------------------------------------------------
package bean;

public class HouseInfo {
    int id;
    String mode;
    double rent;
    String contacts;
    String method;
    int typeID;
    String address;
    String typeName;

    public HouseInfo() {
    }

    public HouseInfo(int id, String mode, double rent, String contacts, String method, int typeID, String address, String typeName) {
        this.id = id;
        this.mode = mode;
        this.rent = rent;
        this.contacts = contacts;
        this.method = method;
        this.typeID = typeID;
        this.address = address;
        this.typeName = typeName;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public double getRent() {
        return rent;
    }

    public void setRent(double rent) {
        this.rent = rent;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getTypeID() {
        return typeID;
    }

    public void setTypeID(int typeID) {
        this.typeID = typeID;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getTypeName() {
        return typeName;
    }

    public void setTypeName(String typeName) {
        this.typeName = typeName;
    }

    @Override
    public String toString() {
        return "HouseInfo{" +
                "id=" + id +
                ", mode='" + mode + '\'' +
                ", rent=" + rent +
                ", contacts='" + contacts + '\'' +
                ", method='" + method + '\'' +
                ", typeID=" + typeID +
                ", address='" + address + '\'' +
                ", typeName='" + typeName + '\'' +
                '}';
    }
}

```

# 工具类代码

```java
package utils;

import java.sql.*;

public class DBUtil {
    private static final String url="jdbc:mysql:///test?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String user="root";
    private static final String pwd="root";
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    public static Connection getConn(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, user, pwd);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }
    public static ResultSet query(String sql,Object ...keys){
        Connection conn = getConn();
        ResultSet rs=null;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            rs = pst.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return rs;
    }
    public static int update(String sql,Object ...keys){
        Connection conn = getConn();
        int num=0;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            num = pst.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return num;
    }
}

```

# servlet

```java
package servlet;

import bean.HouseInfo;
import utils.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/list")
public class ListServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
    String sql="select * from house_type t,house_info i where t.id=i.house_type_id";
        ResultSet rs = DBUtil.query(sql);
        ArrayList<HouseInfo> list = new ArrayList<>();
        try {
            while (rs.next()){
                int id = rs.getInt("i.id");
                String mode = rs.getString("lease_mode");
                double rent = rs.getDouble("rent");
                String contacts = rs.getString("contacts");
                String method = rs.getString("deposit_method");
                int typeId = rs.getInt("house_type_id");
                String address = rs.getString("address");
                String typeName = rs.getString("type");
                HouseInfo info = new HouseInfo(id, mode, rent, contacts, method, typeId, address, typeName);
                list.add(info);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        request.setAttribute("list",list);
        request.getRequestDispatcher("/WEB-INF/list.jsp").forward(request,response)
        ;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
------------------------------------------------------------------------------------------------------------------

package servlet;

import bean.HouseInfo;
import bean.HouseType;
import utils.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/add")
public class AddServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sql="select * from house_type";
        ResultSet rs = DBUtil.query(sql);
        ArrayList<HouseType> list = new ArrayList<>();
        try {
            while (rs.next()){
                int id = rs.getInt("id");
                String type = rs.getString("type");
                HouseType houseType = new HouseType(id, type);
                list.add(houseType);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        request.setAttribute("list",list);
        request.getRequestDispatcher("/WEB-INF/add.jsp").forward(request,response)
        ;
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
         request.setCharacterEncoding("utf-8");
        String mode = request.getParameter("mode");
        String rent = request.getParameter("rent");
        String contacts = request.getParameter("contacts");
        String method = request.getParameter("method");
        String typeID = request.getParameter("typeID");
        String address = request.getParameter("address");
        String sql="insert into house_info values(?,?,?,?,?,?,?)";
        int i = DBUtil.update(sql,null,mode,rent,contacts,method,typeID,address);
        if (i>0){
            response.sendRedirect("/list");
        }else {
            request.setAttribute("msg","添加失败");
            request.getRequestDispatcher("/WEB-INF/msg.jsp").forward(request,response);
        }

    }
}

```

# jsp

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>提示信息</title>
</head>
<body>
<h1>${msg}</h1>
<hr>
<a href="/list">返回列表</a>
</body>
</html>
-------------------------------------------------------------------------------------------------------------------
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>房屋列表</title>
    <style>
        table{
            width: 700px;
            border-collapse: collapse;
            text-align: center;
        }
        tr,th,td{
            border: 1px cadetblue solid;
        }
    </style>
</head>
<body>
<a href="/add"><button>添加房源</button></a>
<hr>
<table>
<tr>
    <th>编号</th>
    <th>租赁方式</th>
    <th>租金（元)</th>
    <th>联系人</th>
    <th>押金方式</th>
    <th>房屋类型</th>
    <th>详细地址</th>
</tr>
<c:forEach items="${list}" var="info">
    <tr>
        <td>${info.id}</td>
        <td>${info.mode}</td>
        <td>${info.rent}</td>
        <td>${info.contacts}</td>
        <td>${info.method}</td>
        <td>${info.typeName}</td>
        <td>${info.address}</td>
    </tr>
</c:forEach>
</table>
</body>
</html>
-------------------------------------------------------------------------------------------------------------------
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-10
  Time: 11:32
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加房源</title>
</head>
<body>
<form action="/add" method="post">
    <table border="1">
        <tr>
            <th>租赁方式</th>
            <td><input type="text" name="mode"></td>
        </tr>
        <tr>
            <th>租金</th>
            <td><input type="text" name="rent"></td>
        </tr>
        <tr>
            <th>联系人</th>
            <td><input type="text" name="contacts"></td>
        </tr>
        <tr>
            <th>押金方式</th>
            <td><input type="text" name="method"></td>
        </tr>
        <tr>
            <th>房屋类型</th>
            <td><select name="typeID" id="">
                <c:forEach items="${list}" var="t">
                    <option value="${t.id}">${t.type}</option>
                </c:forEach>
            </select></td>
        </tr>
        <tr>
            <th>详细地址</th>
            <td><input type="text" name="address"></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="确定"></td>
        </tr>
    </table>
</form>
</body>
</html>

```

