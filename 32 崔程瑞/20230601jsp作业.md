```java
mysql代码

CREATE DATABASE student_db charset utf8;
use student_db;
CREATE TABLE student(
id int PRIMARY KEY auto_increment,
`name` VARCHAR(33),
sex VARCHAR(11)
);
INSERT into student VALUES
(0,"商标","男"),
(0,"金刚","女"),
(0,"李四","男"),
(0,"居居","女"),
(0,"小白","女"),
(0,"小李","男"),
(0,"大王","男");
SELECT * FROM student;
封装类

package bean;

public class Student {
    private int id;
    private String name;
    private String sex;

    public Student() {
    }

    public Student(int id, String name, String sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}
工具类

package util;

import java.sql.*;

public class DBUtil {
    private static final String url="jdbc:mysql:///student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String username="root";
    private static final String password="root";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    public static  Connection getConn(){
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }
    public static ResultSet select(String sql,Object ...kk){
        Connection conn = getConn();
        ResultSet rs = null;
        try {
            PreparedStatement ps = conn.prepareStatement(sql);
            for (int i = 0; i < kk.length; i++) {
                ps.setObject((i+1),kk[i]);
            }
            rs = ps.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return rs;
    }
    public static int update(String sql,Object ...bb){
        Connection conn = getConn();
        int num = 0;
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            for (int i = 0; i < bb.length; i++) {
               pr.setObject((i+1),bb[i]);
            }
            num = pr.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return num;
    }
    public static void close(Connection conn,PreparedStatement ps,ResultSet rs){
        try {
            if (conn!=null){
                conn.close();
            }
            if (ps!=null){
                ps.close();
            }
            if (rs!=null){
                rs.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
增删改查方法

package dao;

import bean.Student;
import util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentDao {

    //查询所有学生的信息
    public ArrayList<Student> getAllStudent(){
        String sql="select * from student";
        ResultSet rs = DBUtil.select(sql);
        ArrayList<Student> list = new ArrayList<>();
        try {
            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                Student stu = new Student(id, name, sex);
                list.add(stu);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(null,null,rs);
        }
        return list;
    }

    //根据ID查询学生的信息
    public Student getStudentById(int id){
        String sql="select * from student where id=?";
        ResultSet rs = DBUtil.select(sql, id);
        Student stu=null;
        try {
            while (rs.next()){
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                stu = new Student(id, name, sex);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBUtil.close(null,null,rs);
        }
        return stu;
    }

    //根据ID删除学生
    public int deleteStudentById(int id){
        String sql="delete from student where id=?";
        int i = DBUtil.update(sql, id);
        return i;
    }

    //修改学生信息
    public int updateStudentById(Student stu){
        String sql="update student set name=?,sex=? where id=?";
        int i = DBUtil.update(sql, stu.getName(), stu.getSex(), stu.getId());
        return i;
    }

    //添加学生信息
    public int addStudentById(Student stu){
        String sql="insert into student values (?,?,?)";
        int i = DBUtil.update(sql, 0, stu.getName(), stu.getSex());
        return i;
    }

}
测试类

package servlet;

import bean.Student;
import dao.StudentDao;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/student/*")
public class StudentController extends HttpServlet {
        StudentDao dao = new StudentDao();
        Student s = new Student();
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        String path = request.getPathInfo();
        System.out.println("path = " + path);
        if (path == null || path.equals("/")){
            ArrayList<Student> list = dao.getAllStudent();
            request.setAttribute("list",list);
            request.getRequestDispatcher("all_student.jsp").forward(request,response);
        }else if (path.matches("/\\d+")){ //正则表达式
            String id = path.substring(1);
//            System.out.println("/student/111 查看"+id+"编号的学生");
            Student student = dao.getStudentById(Integer.parseInt(id));
            request.setAttribute("student",student);
            request.getRequestDispatcher("/getStudentById.jsp").forward(request,response);

        }else if (path.matches("/update/\\d+")){
            String id = path.substring(12);
            System.out.println("/student/update/111 编辑"+id+"学号的学生");
//
//            request.setAttribute("stu",s);
//            request.getRequestDispatcher("/update.jsp").forward(request,response);
//            response.sendRedirect("/student");
        }else if (path.matches("/delete/\\d+")){
            String id = path.substring(8);
            dao.deleteStudentById(Integer.parseInt(id));
            response.sendRedirect("/student");
        }else if (path.matches("/addStudent.jsp")){
            System.out.println("/student/add 正在添加学生信息");
//            dao.addStudentById(null);
            request.getRequestDispatcher("/addStudent.jsp").forward(request,response);

        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
       request.setCharacterEncoding("utf-8");
       response.setContentType("text/html;charset=utf-8");

        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String sex = request.getParameter("sex");
        Student stu = new Student(Integer.parseInt(id), name, sex);
        int i = dao.addStudentById(stu);
        if (i>0){
            response.getWriter().write("添加成功！");
            response.sendRedirect("/student");
        }else {
            response.getWriter().write("添加成功！");
        }
//        String path = request.getPathInfo();
//        path.substring()
//        dao.updateStudentById(stu);
//        if (i>0){
//            response.getWriter().write("修改成功！");
//            response.sendRedirect("/student");
//        }else {
//            response.getWriter().write("修改成功！");
//        }
//        String id1 = request.getParameter("id");
//        String name1 = request.getParameter("name");
//        String sex1 = request.getParameter("sex");
//        if (id1!=null){
//            Student student = new Student(Integer.parseInt(id1), name1, sex1);
//            int i1 = dao.updateStudentById(stu);
//            dao.getStudentById(Integer.parseInt(id1));
//            id1="";
//            name1="";
//            sex1="";
//            if (i1>0){
//                response.getWriter().write("修改成功！");
//            }else {
//                response.getWriter().write("修改失败！");
//            }
//        }


    }
}
HTML代码

查看所有

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-05-31
  Time: 19:17
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生列表</title>
</head>
<body>
<h1>查看所有学生信息</h1>
<hr>
<table border="1" width="500">
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>操作</th>
    </tr>
    <c:forEach var="stu" items="${list}">
        <tr>
            <td>${stu.id}</td>
            <td>${stu.name}</td>
            <td>${stu.sex}</td>
            <td>
                <a href="/student/${stu.id}">查看</a>
                <a href="/student/update/${stu.id}">编辑</a>
                <a href="/student/delete/${stu.id}" onclick="return confirm('确定要永久删除吗？')">删除</a>
            </td>
        </tr>
    </c:forEach>
</table>
<hr>
<a href="/student/add">添加学生</a>
</body>
</html>


添加学生

<%--
  Created by IntelliJ IDEA.
  User: Lenovo
  Date: 2023/6/1
  Time: 11:59
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<h1>添加学生</h1>
<form action="/student/" method="post">
    <table>
        <tr>
            <td>编号</td>
            <td><input type="text" name="id"></td>
        </tr>
        <tr>
            <td>姓名</td>
            <td><input type="text" name="name"></td>
        </tr>
        <tr>
            <td>性别</td>
            <td><input type="text" name="sex"></td>
        </tr>
        <tr>
            <td><input type="submit" value="添加"></td>
        </tr>
    </table>
</form>

</body>
</html>
```

