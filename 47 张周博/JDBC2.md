```java
package tool;

import java.sql.*;

public class DBtool {
    //1.注册
    static{
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("驱动注册异常");
        }
    }
    //2.获取链接
    private static final String url =
            "jdbc:mysql://localhost:3306/student?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String username = "root";
    private static final String password = "root";

    //3.连接
    public static Connection getConn() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            System.out.println("驱动连接异常");
        }
        return conn;
    }
    //通用查询方法
    public static ResultSet select(String sql) {
        ResultSet rs = null;
        try {
            Connection conn = getConn();
            PreparedStatement st = conn.prepareStatement(sql);
            rs = st.executeQuery();
        } catch (SQLException e) {
            System.out.println("sql语言异常");
        }
        return rs;
    }

    //通用的修改方法
    public static int update(String sql,String ...more) {
        int i = 0;
        try {
            Connection conn = getConn();
            PreparedStatement st = conn.prepareStatement(sql);
            for (int j = 0; j < more.length; j++) {
                st.setString((j + 1),more[j]);
            }
            i = st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("sql语言异常");
            e.printStackTrace();
        }
        return i;
    }
    public static void close(Connection conn,PreparedStatement st,ResultSet rs){
        try {
            if (rs != null){
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null){
                conn.close();
            }
        } catch (SQLException e) {
            System.out.println("资源关闭异常");
        }
    }
}


package tool;

import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

public class Student {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        out:
        while (true) {
            System.out.println("***********欢迎来到学生管理系统**************\n" +
                    "输入1添加学生\n" +
                    "输入2删除学生\n" +
                    "输入3修改学生\n" +
                    "输入4查询学生");
            int num = sc.nextInt();
            switch (num) {
                case 1:
                    intster();
                    break;
                case 2:
                    delect();
                    break;
                case 3:
                    update();
                    break;
                case 4:
                    select();
                    break;
                default:
                    System.out.println("您输入的操作不存在");
                    break out;
            }
        }
    }
    public static void select() {
        ResultSet rs;
        Connection conn;
        try {
            conn = DBtool.getConn();
            String sql = "select * from massage";
            rs = DBtool.select(sql);
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String sex = rs.getString("sex");
                System.out.println(id + "\t" + name + "\t" + age + "\t" + sex);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        DBtool.close(conn, null, rs);
    }
    
    public static void intster(){
        Scanner sc = new Scanner(System.in);
        Connection conn = DBtool.getConn();
        System.out.println("请输入要添加的学生的学号");
        String id = sc.next();
        System.out.println("请输入要添加学生的姓名");
        String name = sc.next();
        System.out.println("请输入要添加学生的年龄");
        String age = sc.next();
        System.out.println("请输入要添加学生的性别");
        String sex = sc.next();
        String sql = "insert into massage values(?,?,?,?)";
        int i = DBtool.update(sql,id,name,age,sex);
        if (i > 0){
            System.out.println("添加成功");
        }else {
            System.out.println("没有任何东西被添加");
        }
        DBtool.close(conn,null,null);
    }

    public static void delect(){
        Scanner sc = new Scanner(System.in);
        Connection conn = DBtool.getConn();
        System.out.println("请输入要删除的学生学号");
        String id = sc.next();
        String sql = "DELETE from massage where id = ?";
        int i = DBtool.update(sql, id);
        if (i > 0) {
            System.out.println("删除成功");
        }else {
            System.out.println("没有删除任何数据");
        }
        DBtool.close(conn,null,null);
    }

    public static void update(){
        Scanner sc = new Scanner(System.in);
        Connection conn = DBtool.getConn();
        System.out.println("请输入要修改的学生学号");
        String id = sc.next();
        System.out.println("请输入修改的学生学号");
        String id2 = sc.next();
        System.out.println("请输入修改的学生姓名");
        String name = sc.next();
        System.out.println("请输入修改学生的年龄");
        String age = sc.next();
        System.out.println("请输入修改学生的性别");
        String sex = sc.next();
        String sql = "update massage set id = ?,name = ?,age = ?,sex = ? where id = ?";
        int i = DBtool.update(sql, id2, name, age, sex,id);
        if (i > 0){
            System.out.println("修改成功");
        }else {
            System.out.println("没有修改任何数据");
        }
        DBtool.close(conn,null,null);
    }
}
```
