# 数据库

~~~mysql
-- 建库
create database if not exists student_db character set utf8;
-- 选库
use student_db;
-- 建表
create table if not exists student(
id int primary key auto_increment, -- 学号
name varchar(20),                  -- 姓名
sex enum('男','女','保密')         -- 性别
);
-- 添加数据
insert into student values 
(0,'张三','男'),
(0,'李四','女'),
(0,'王五','男');
-- 删除数据
-- delete from student where id=10;
-- UPDATE student set id=1,name='赵六',sex='男' where id=1 ;
-- SELECT * FROM student;
~~~

# java代码

#  工具类

~~~java
package utils;

import java.sql.*;

public class MySQLUtil {
    private static final String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf-8";
    private static final String user = "root";
    private static final String password = "root";

    static {
//        注册驱动
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("驱动异常！");
        }
    }

    /**
     * 连接数据库
     *
     * @return
     */
    public static Connection conn() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, user, password);
        } catch (SQLException e) {
            System.out.println("连接异常！");
        }

        return conn;

    }


    /**
     * sql查询语句
     * @param sql
     * @param keys
     * @return
     */
    public static ResultSet select(String sql,String ... keys){
        Connection conn = conn();
        ResultSet re = null;
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pr.setString((i+1),keys[i]);
            }
            re = pr.executeQuery();
        } catch (SQLException e) {
            System.out.println("sql语句异常");
        }
        return re;
    }

    /**
     * sql 添加 修改 删除 语句
     * @param sql
     * @param keys
     * @return
     */
    public static int general(String sql,String ... keys) {
        Connection conn = conn();
        int i1 = 0;
        try {
            PreparedStatement pr = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pr.setString((i + 1), keys[i]);
            }
            i1 = pr.executeUpdate();
        } catch (SQLException e) {
            System.out.println("sql增删改语句异常！");
        }
        return i1;
    }
~~~

# 测试类

~~~java
package test;

import utils.MySQLUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
@WebServlet("/delete")
public class Test1 extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //        设置字符集
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        resp.setCharacterEncoding("utf-8");
        //        获取sql语句
        int i = 0;
        try {
            String sql = "delete from student where id= ?";
            String id = req.getParameter("id");
            i = MySQLUtil.general(sql, id);
        } catch (Exception e) {
            System.out.println("sql语句异常！");
        }
        if (i > 0) {
            resp.getWriter().write("删除成功！");
        } else {
            resp.getWriter().write("删除失败！");
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //        设置字符集
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        resp.setCharacterEncoding("utf-8");
        doPost(req, resp);
    }
}
~~~

~~~java
package test;

import bean.Student;
import utils.MySQLUtil;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/update")
public class Test extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //        设置字符集
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        resp.setCharacterEncoding("utf-8");
        //        获取sql语句
        String sql="update student set name=?,sex=? where id=?";
        String sex = req.getParameter("sex");
        String name = req.getParameter("name");
        String id = req.getParameter("id");
        int i = MySQLUtil.general(sql, name, sex, id);
        PrintWriter out = resp.getWriter();
        if (i>0){
            out.write("修改成功！");
        }else{
            out.write("修改失败！");
        }
    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //        设置字符集
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        resp.setCharacterEncoding("utf-8");
        ArrayList<Student> stu = new ArrayList<>();
        //        获取sql语句
        try {
            String sql ="select * from student where id =?";
            String id = req.getParameter("id");
            ResultSet se = MySQLUtil.select(sql, id);
            while (se.next()){
                int id1 = se.getInt("id");
                String name1 = se.getString("name");
                String sex1 = se.getString("sex");
                Student student = new Student(id1, name1, sex1);
                stu.add(student);
            }
        } catch (SQLException e) {
            System.out.println("sql语句执行错误！");
        }
        Student std = stu.get(0);
        req.setAttribute("std",std);
        req.getRequestDispatcher("/update.jsp").forward(req,resp);
    }
}

~~~



# jsp代码

~~~jsp
<%@ page import="bean.Student" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改</title>
</head>
<body>
<%
     Student std = (Student) request.getAttribute("std");
%>
<h1>更新学生信息</h1>
<form action="/update" method="post">
    <input type="hidden" name="id" value="<%=std.getId()%>"><br>
    <input type=text"" name="name" value="<%=std.getName()%>"><br>
    <input type="radio" name="sex" value="男">男
    <input type="radio" name="sex" value="女">女
    <input type="radio" name="sex" value="保密">保密<br>
    <input type="submit">

</form>
</body>
</html>
~~~

~~~jsp
<%@ page import="utils.MySQLUtil" %>
<%@ page import="java.sql.ResultSet" %>
<%@ page import="java.sql.SQLException" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>$Title$</title>
</head>
<body>
<table border="5" width="300" align="center">
    <caption>学生表</caption>
    <tr>
        <td>
            学号
        </td>
        <td>
            姓名
        </td>
        <td>
            性别
        </td>
        <td>
            操作
        </td>
    </tr>

    <%
        String sql = "select * from student";
        try {
            ResultSet se = MySQLUtil.select(sql);
            while (se.next()) {
                String id = se.getString("id");
                String name = se.getString("name");
                String sex = se.getString("sex");
    %>
    <tr>
        <td>
            <%=id%>
        </td>
        <td>
            <%=name%>
        </td>
        <td>
            <%=sex%>
        </td>
        <td>
            <a href="/update?id=<%=id%>">修改</a>
            <a href="/delete?id=<%=id%>">删除</a>
        </td>
    </tr>
    <%


            }
        } catch (SQLException e) {
            System.out.println("sql查询语句错误");
        }

    %>


</table>
</body>
</html>
~~~


