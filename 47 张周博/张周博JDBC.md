1. MySQL创建一个数据库student_db

2. 创建student表

3. 表中数据

4. | 编号 | 姓名 | 性别 |
   | ---- | ---- | ---- |
   | 1    | 张三 | 男   |
   | 2    | 李四 | 女   |
   | 3    | 王五 | 男   |

5. 编写java 4个类，分别实现以下功能

   1. 查询功能，查询student中所有数据
   2. 添加功能
   3. 修改功能
   4. 删除功能

6. 扩展题【预习题】

   1. 能否实现一个类中，用四个方法来实现上面4个类的功能
   2. 能否实现将查询的结果，封装成java对象

```mysql
CREATE DATABASE student_db charset utf8;
USE student_db;
CREATE TABLE student (
    id INT,
    name VARCHAR(10),
    sex VARCHAR(2) 
);
INSERT INTO student VALUES 
(1, '张三', '男'), 
(2, '李四', '女'), 
(3, '王五', '男');
/* 
数据库
*/
```

```java
//学生类
public class Student {
    private int id;
    private String name;
    private String sex;    
}
 public Student(int id, String name, String gender) {
        this.id = id;
        this.name = name;
        this.gender = gender;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getGender() {
        return sex;
    }

    public void setGender(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "id: " + id + ", name: " + name + ", sex: " + sex;
    }
}

```

```java
import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class StudentDao {
    private static final String URL = "jdbc:mysql://localhost:3306/student_db";
    private static final String username = "root";
    private static final String password = "root";

    public List<Student> cxAll() {
        List<Student> students = new ArrayList<>();

        try (Connection conn = DriverManager.getConnection(URL, username, password);
             Statement stmt = conn.createStatement();
             ResultSet rs = stmt.executeQuery("SELECT * FROM student")) {

            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String gender = rs.getString("sex");
                Student student = new Student(id, name, sex);
                students.add(student);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return students;
    }
//查询所有学生
    public boolean add(Student student) {
        try (Connection conn = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
             PreparedStatement stmt = conn.prepareStatement("insert into student(name, sex) values (?, ?)")) {

            stmt.setString(1, student.getName());
            stmt.setString(2, student.getSex());
            int count = stmt.executeUpdate();

            return count > 0;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
//修改学生信息
    public boolean update(Student student) {
        try (Connection conn = DriverManager.getConnection(URL, USERNAME, PASSWORD);
             PreparedStatement stmt = conn.prepareStatement("UPDATE student SET name =? , sex = ? WHERE id = ?")) {

            stmt.setString(1, student.getName());
            stmt.setString(2, student.getGender());
            stmt.setInt(3, student.getId());
            int count = stmt.executeUpdate();

            return count > 0;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }

    public boolean delete(int id) {
        try (Connection conn = DriverManager.getConnection(URL, USER_NAME, PASSWORD);
             PreparedStatement stmt = conn.prepareStatement("DELETE FROM student WHERE id = ?")) {

            stmt.setInt(1, id);
            int count = stmt.executeUpdate();

            return count > 0;

        } catch (SQLException e) {
            e.printStackTrace();
        }

        return false;
    }
}
```

```java
//运行类
import java.util.List;

public class Main {
    public static void main(String[] args) {
        StudentDAO dao = new StudentDAO();
        
        // 查询所有学生
        List<Student> students = dao.getAllStudents();
        System.out.println("所有学生信息：");
        for (Student student : students) {
            System.out.printf("编号：%d，姓名：%s，性别：%s\n", student.getId(), student.getName(), student.getGender());
        }
        
        // 添加学生
        Student newStudent = new Student();
        newStudent.setId(4);
        newStudent.setName("傻狗");
        newStudent.setSex("男");
        dao.addStudent(newStudent);
        System.out.println("添加学生成功");
        
        // 修改学生信息
        Student updateStudent = new Student();
        updateStudent.setId(4);
        updateStudent.setName("大傻狗");
        updateStudent.setSex("男");
        dao.updateStudent(updateStudent);
        System.out.println("修改学生信息成功");
        
        // 删除学生
        dao.deleteStudent(2);
        System.out.println("删除学生成功");
    }
}
```
