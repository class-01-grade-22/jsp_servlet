```java
package yuangong;

import java.sql.*;
import java.util.ArrayList;

public class D1 {
     private static String url="jdbc:mysql:///YG?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static String name="root";
    private static String pwd="root";
     static{
         try {
             Class.forName("com.mysql.jdbc.Driver");
         } catch (ClassNotFoundException e) {
             throw new RuntimeException(e);
         }
     }
     public static Connection getConn() throws SQLException {
         Connection conn= DriverManager.getConnection(url,name,pwd);
         return conn;
     }
     public static ResultSet select(String sql,String...keys)throws  SQLException{
         Connection conn=getConn();
         PreparedStatement pst= conn.prepareStatement(sql);

         for (int i = 0; i < keys.length; i++) {
             pst.setString((i+1),keys[i]);
         }
         ResultSet rs=pst.executeQuery();
         return rs;
     }

     public  static  int update(String sql,String...keys)throws  SQLException{
         Connection conn=getConn();
         PreparedStatement pst=conn.prepareStatement(sql);
         for (int i = 0; i < keys.length; i++) {
             pst.setString((i+1),keys[i]);
         }
         int i=pst.executeUpdate();
         return i;
     }
    public static void close(Connection conn,PreparedStatement pst,ResultSet rs){
        try {
            if (rs!=null){
                rs.close();
            }
            if (pst!=null){
                pst.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public static void main(String[] args) {
        ArrayList<D1>list=new ArrayList<>();

    }
}
```