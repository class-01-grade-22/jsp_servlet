```mysql
# 数据库名称：CompanyManager
create database CompanyManager char set utf8;
use CompanyManager;
# 表： Dept (部门信息表)
create table Dept
(
    DeptID   int primary key auto_increment, # 部门编号主键,自动增长列
    DeptName varchar(20) not null            # 部门名称不允许为空

);

insert into Dept values
                     (0,'技术部'),
                     (0,'后勤部'),
                     (0,'财务部');

# 表：Emp (员工信息表)
create table Emp
(
    EmpID    int primary key auto_increment, # 员工编号主键,自动增长列
    EmpName  varchar(20) not null,           # 员工姓名不允许为空
    Sex      char(2)     not null,           # 性别不允许为空
    Age      int         not null,           # 员工年龄不允许为空
    Tel      varchar(20) not null unique,    # 联系电话不允许为空（唯一约束）
    PassWord varchar(12) not null,           # 密码不允许为空
    DeptID   int,                            # 部门编号外键，关联部门表DeptID字段
    foreign key (DeptID) references Dept (DeptID)
);


insert into Emp values
                    (0,'张三','女',18,'10086','123',1),
                    (0,'李四','女',19,'10087','123',2),
                    (0,'王五','男',17,'10088','123',3);
```

bean

```java
package bean;

public class Dept {
    private int deptId;
    private String deptName;

    public Dept(int deptId, String deptName) {
        this.deptId = deptId;
        this.deptName = deptName;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Dept() {
    }

    @Override
    public String toString() {
        return "Dept{" +
                "deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                '}';
    }
}

```

```java
package bean;

public class Emp {
    private int empId;
    private String empName;
    private String sex;
    private int age;
    private String tel;
    private String passWord;
    private int deptId;
    private String deptName;

    @Override
    public String toString() {
        return "Emp{" +
                "empId=" + empId +
                ", empName='" + empName + '\'' +
                ", sex='" + sex + '\'' +
                ", age=" + age +
                ", tel='" + tel + '\'' +
                ", passWord='" + passWord + '\'' +
                ", deptId=" + deptId +
                ", deptName='" + deptName + '\'' +
                '}';
    }

    public int getEmpId() {
        return empId;
    }

    public void setEmpId(int empId) {
        this.empId = empId;
    }

    public String getEmpName() {
        return empName;
    }

    public void setEmpName(String empName) {
        this.empName = empName;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getTel() {
        return tel;
    }

    public void setTel(String tel) {
        this.tel = tel;
    }

    public String getPassWord() {
        return passWord;
    }

    public void setPassWord(String passWord) {
        this.passWord = passWord;
    }

    public int getDeptId() {
        return deptId;
    }

    public void setDeptId(int deptId) {
        this.deptId = deptId;
    }

    public String getDeptName() {
        return deptName;
    }

    public void setDeptName(String deptName) {
        this.deptName = deptName;
    }

    public Emp() {
    }

    public Emp(int empId, String empName, String sex, int age, String tel, String passWord, int deptId, String deptName) {
        this.empId = empId;
        this.empName = empName;
        this.sex = sex;
        this.age = age;
        this.tel = tel;
        this.passWord = passWord;
        this.deptId = deptId;
        this.deptName = deptName;
    }
}

```

untils

```java
package utils;

import javax.swing.*;
import java.net.ConnectException;
import java.sql.*;

public class DBUtil {
    //定义主机、用户名、密码、字符集
    public static final String url="jdbc:mysql:///companymanager?characterEncoding=utf8";
    public static final String user="root";
    public static final String password="root";
    //1.注册驱动
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }
    //2.获取连接对象
    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url, user, password);
        return conn;
    }
    //通用查询方法
    public  static ResultSet select(String sql,Object...keys){
        ResultSet rs = null;
        try {
            Connection conn = getConn();
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            rs = pst.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return rs;
    }
    //修改查询方法
    public static int upadte(String sql,Object...keys){
        int num = 0;
        try {
            Connection conn = getConn();
            PreparedStatement pst= conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            num = pst.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return num;
    }
    //释放资源
    public static void close(PreparedStatement pst,ResultSet rs,Connection conn){
        try {
            if (conn!=null){
                conn.close();
            }
            if (rs!=null){
                rs.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

```

servlet

```java
package servlet;

import bean.Emp;
import utils.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/One")
public class OneServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        
        String sql="select * from Emp e,Dept d where e.deptId=d.deptId";
        ArrayList<Emp>list=new ArrayList<>();
        ResultSet rs= DBUtil.select(sql);
        try {
            while (rs.next()){
                int empId=rs.getInt(1);
                String empName=rs.getString(2);
                String sex=rs.getString(3);
                int age= rs.getInt(4);
                String tel= rs.getString(5);
                String passWord=rs.getString(6);
                int deptId=rs.getInt(7);
                String deptName=rs.getString(8);
                Emp emp = new Emp(empId, empName, sex, age, tel, passWord, deptId, deptName);
                list.add(emp);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        request.setAttribute("list",list);
        request.getRequestDispatcher("/WEB-INF/allemp.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

```

```java
package servlet;

import bean.Emp;
import utils.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

import static utils.DBUtil.user;

@WebServlet("/user")
//要与表单的name同名
public class TwoServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

        request.setCharacterEncoding("utf8");
        String sql="select * from Emp e,Dept d where e.deptId=d.deptId and empName like ?";
        ArrayList<Emp> list=new ArrayList<>();
        //获取表单数据,要与
        String user = request.getParameter("user");
        ResultSet rs= DBUtil.select(sql,"%"+user+"%");
        try {
            while (rs.next()){
                int empId=rs.getInt(1);
                String empName=rs.getString(2);
                String sex=rs.getString(3);
                int age= rs.getInt(4);
                String tel= rs.getString(5);
                String passWord=rs.getString(6);
                int deptId=rs.getInt(7);
                String deptName=rs.getString(8);
                Emp emp = new Emp(empId, empName, sex, age, tel, passWord, deptId, deptName);
                list.add(emp);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        request.setAttribute("list",list);
        request.getRequestDispatcher("/WEB-INF/allemp.jsp").forward(request,response);
    }
}

```

