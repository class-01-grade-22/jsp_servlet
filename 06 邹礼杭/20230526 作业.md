数据库

```sql
create database student_db character set utf8;
use student_db;

create table student (
id int PRIMARY KEY auto_increment,
name VARCHAR(20),
sex VARCHAR(20)
);
select * from student;
```

工具类

```java
package day0526_post;
import java.sql.*;

public class Util {
    private static final String url="jdbc:mysql:///student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String user="root";
    private static final String pwd="root";

    //注册驱动
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("驱动导入异常");
        }
    }


    //获取连接对象
    public static Connection getConn() throws SQLException {
        Connection conn= DriverManager.getConnection(url,user,pwd);
        return conn;
    }

    //通用的查询方法
    public static ResultSet select(String sql, String ...keys) throws SQLException {
        Connection conn=getConn();
        PreparedStatement pst = conn.prepareStatement(sql); //prepare 预编译
        //给？赋值
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+i),keys[i]);
        }

        ResultSet rs = pst.executeQuery();
        return rs;
    }

    //通用的增 删 改 方法
    public static  int update(String sql,String ...keys) throws SQLException {
        Connection conn=getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+1),keys[i]);
        }

        int i=pst.executeUpdate();
        return i;

    }
    //通用的关闭资源方法
    public static void close(Connection conn,PreparedStatement pst,ResultSet rs) throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (pst != null) {
            pst.close();
        }
        if (conn != null) {
            conn.close();
        }

    }
}

```

添加

```java
package day0526_post;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/addServlet")
public class addServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置编码
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        //获取请求中的参数
        String id=req.getParameter("id");
        String name=req.getParameter("name");
        String sex=req.getParameter("sex");
        //打印
        System.out.println("id="+id);
        System.out.println("name="+name);
        System.out.println("sex="+sex);
        //3执行sql语句
        String sql="insert into student values (?,?,?)";
        try {
            int i = Util.update(sql, id, name, sex);
            if (i>0){
                resp.getWriter().write("添加成功");
            }else{
                resp.getWriter().write("添加失败");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

```

删除

```java
package day0526_post;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/deleteServlet")
public class deleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        //设置编码
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        //获取请求中的参数
        String id =request.getParameter("id");
        System.out.println("id = "+id);
        //编写sql语句
        String sql="delete from student where id=?";
        try {
            int i = Util.update(sql, id);
            if (i>0){
                response.getWriter().write("删除成功");
            }else{
                response.getWriter().write("删除失败");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

```

查询

```java
package day0526_post;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/selectServlet")
public class selectServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //设置编码格式
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        //执行sql语句
        resp.getWriter().write("<style\n>"+
                " table{\n" +
                "  width: 500px;\n" +
                "  border-collapse: collapse;\n" +
                " }\n" +
                "</style>" +
                "<table border=1>");
        resp.getWriter().write("<tr><th>编号</th><th>姓名</th><th>性别</th></tr>");

        String sql="select * from student";
        try {
            ResultSet rs = Util.select(sql);
            while (rs.next()){
                int eid=rs.getInt("id");
                String name=rs.getString("name");
                String sex=rs.getString("sex");
                resp.getWriter().write("<tr><th>"+eid+"</th><th>"+name+"</th><th>"+sex+"</th></tr>");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

```

html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>学生</title>
</head>
<body>
<h1>添加学生</h1>
<form action="/addServlet" method="post">
    <table>
        <tr>
            <td>编号</td>
            <td><input type="text" name="id"></td>
        </tr>
        <tr>
            <td>姓名</td>
            <td><input type="text" name="name"></td>
        </tr>
        <tr>
            <td>性别</td>
            <td><input type="text" name="sex"></td>
        </tr>
        <tr>
            <td><input type="submit" value="提交"></td>
        </tr>
    </table>
</form>

    
<h1>删除学生</h1>
<form action="/deleteServlet" method="post">
    <table>
        <tr>
            <td>请输入要删除的学生的编号</td>
            <td><input type="text" name="id"></td>
        </tr>
        <tr>
            <td><input type="submit" value="删除"></td>
        </tr>
    </table>
</form>

    
<h1>查看学生</h1>
<form action="/selectServlet" method="post">
    <table>
        <tr>
            <td><input type="submit" value="查看所有学生"></td>
        </tr>
    </table>
</form>
</body>
</html>
```

