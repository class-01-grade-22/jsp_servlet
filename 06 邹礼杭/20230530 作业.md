数据库

```sql
create database student_db character set utf8;
use student_db;

create table student (
id int PRIMARY KEY auto_increment,
name VARCHAR(20),
sex VARCHAR(20)
);
insert into student VALUES
(0,'张三','男'),
(0,'李四','女'),
(0,'王五','男');
select * from student;
```

util

```java
package util;

import java.sql.*;

public class DBUtil {
    private static final String url="jdbc:mysql:///student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String username="root";
    private static final String password="root";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    //获取连接用户
    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url, username, password);
        return conn;
    }
    //通用的查询方法
    public static ResultSet select(String sql,Object...keys) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            pst.setObject((i+1),keys[i]);
        }
        ResultSet rs = pst.executeQuery();
        return rs;
    }
    //通用的增删改方法
    public static int update(String sql,Object...keys) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            pst.setObject((i+1),keys[i]);
        }
        int i = pst.executeUpdate();
        return i;
    }

    //关闭资源
    public static void close(Connection conn, PreparedStatement pst, ResultSet rs) throws SQLException {
        if (rs!=null){
            rs.close();
        }
        if (pst!=null){
            pst.close();
        }
        if (conn!=null){
            conn.close();
        }

    }
}

```

dao

```java
package day0530.dao;

import day0530.bean.Student;
import day0530.util.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentDao {
    //查看所有学生
    public ArrayList<Student> getAllStudent() throws SQLException {
        String sql="select * from student";
        ResultSet rs = DBUtil.select(sql);
        ArrayList<Student> list = new ArrayList<>();
        while (rs.next()){
            int id = rs.getInt("id");
            String name = rs.getString("name");
            String sex = rs.getString("sex");
            Student stu = new Student(id,name,sex);
            list.add(stu);
        }
        return list;
    }
    //根据id查看某个学生
    public Student getStudentById(int id) throws SQLException {
        String sql="select * from student where id=?";
        ResultSet rs = DBUtil.select(sql,id);
        Student stu =null;
        if (rs.next()){
            String name = rs.getString("name");
            String sex = rs.getString("sex");
            stu = new Student(id,name,sex);
        }
        return stu;
    }
    //根据id删除某个学生
    public int deleteStudentById(int id) throws SQLException {
        String sql="delete from student where id=?";
        int i = DBUtil.update(sql, id);
        return i;
    }
    //修改某个学生
    public int updateStudentById(Student student) throws SQLException {
        String sql="update student set sex=?,name=? where id=?";
        int i = DBUtil.update(sql, student.getSex(), student.getName(), student.getId());
        return i;
    }
    //添加学生
    public int addStudent(Student student) throws SQLException {
        String sql="insert into student values (?,?,?)";
        int i = DBUtil.update(sql, 0, student.getName(), student.getSex());
        return i;
    }
}

```

servlet

```java
package day0530.servlet;

import day0527.Util;
import day0530.bean.Student;
import day0530.dao.StudentDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;
@WebServlet("/student/*")
public class StudentController extends HttpServlet {
    StudentDao dao = new StudentDao();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        String path = req.getPathInfo();

        if (path == null || path.equals("/")) {
            //查看所有学生
            ArrayList<Student> list = null;
            try {
                list = dao.getAllStudent();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            req.setAttribute("list", list);
            req.getRequestDispatcher("//all_student.jsp").forward(req, resp);


        } else if (path.matches("/\\d+")) {     //matches匹配   d+ 表示1个以上的数字
            //根据id查看学生
            String id = path.substring(1);
            Student student = null;
            try {
                student = dao.getStudentById(Integer.parseInt(id));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            req.setAttribute("stu", student);
            req.getRequestDispatcher("//get_student.jsp").forward(req, resp);

        } else if (path.matches("/update/\\d+")) {
            //根据id修改学生
            String id = path.substring(8);
            Student student = null;
            try {
                student = dao.getStudentById(Integer.parseInt(id));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            req.setAttribute("stu",student);
            req.getRequestDispatcher("/update_student.jsp").forward(req, resp);


        } else if (path.matches("/delete/\\d+")) {
            //根据id删除学生
            String id = path.substring(8);
            try {
                dao.deleteStudentById(Integer.parseInt(id));
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            //重定向   删除后回到/student路径页面
            resp.sendRedirect("/student");

        } else if (path.matches("/add")) {
            req.getRequestDispatcher("/add_student.jsp").forward(req, resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        //获取请求中的参数
        int id= Integer.parseInt(req.getParameter("id"));
        String name=req.getParameter("name");
        String sex=req.getParameter("sex");
        Student stu=new Student(id,name,sex);
        String p = req.getPathInfo();

        if (p.matches("/add")){
            try {
                dao.addStudent(stu);
                resp.sendRedirect("/student");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }

        if (p.matches("/update/")){
            try {
                dao.updateStudentById(stu);
                resp.sendRedirect("/student");
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
```

学生类

```java
package bean;

public class Student {
    private int id;
    private String name;
    private String sex;

    public Student(int id, String name, String sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
    }

    public Student() {
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }
}

```

all_student.jsp

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生列表</title>
</head>
<body>
<h1>查看所有学生</h1>
<hr>
<table width="350px" border="1" >
    <tr>
        <th>编号</th>
        <th>姓名</th>
        <th>性别</th>
        <th>操作</th>
    </tr>

    <c:forEach items="${list}" var="stu">
        <tr>
            <td>${stu.id}</td>
            <td>${stu.name}</td>
            <td>${stu.sex}</td>
            <td>
                <a href="/student/${stu.id}">查看</a>
                <a href="/student/update/${stu.id}">修改</a>
                <a href="/student/delete/${stu.id}" onclick="return confirm('确认要永久删除吗？')">删除</a>
            </td>
        </tr>
    </c:forEach>

</table>
<hr>
<a href="/add_student.jsp" >添加学生</a>

</body>
</html>

```

add_student.jsp

```jsp

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加学生</title>
</head>
<body>

<h1>添加学生</h1>
<hr>
<form action="/student/add" method="post">
    <table>
        <tr>
            <td>编号</td>
            <td><input type="text" name="id"></td>
        </tr>
        <tr>
            <td>姓名</td>
            <td><input type="text" name="name"></td>
        </tr>
        <tr>
            <td>性别</td>
            <td><input type="text" name="sex"></td>
        </tr>
        <tr>
            <td><input type="submit" value="提交" onclick="return confirm('添加成功')"></td>
        </tr>
    </table>
</form>
<hr>
</body>
</html>

```

get_student.jsp

```jsp

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>学生</title>
</head>
<body>

<h1>查看id为${stu.id}学生</h1>
<hr>

  <table border="1" width="300px">
    <tr>
      <th>编号</th>
      <th>姓名</th>
      <th>性别</th>
    </tr>
    <tr>
      <td>${stu.id}</td>
      <td>${stu.name}</td>
      <td>${stu.sex}</td>
    </tr>
  </table>


</body>
</html>

```

update_student.jsp

```jsp
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改学生</title>
</head>
<body>

<h1>修改id为${stu.id}学生</h1>
<hr>

<form action="/student/update/" method="post">
    <table>
        <tr>
            <td>编号</td>
            <td><input type="text" name="id" value="${stu.id}"></td>
        </tr>
        <tr>
            <td>姓名</td>
            <td><input type="text" name="name" value="${stu.name}"></td>
        </tr>
        <tr>
            <td>性别</td>
            <td><input type="text" name="sex" value="${stu.sex}"></td>
        </tr>
        <tr>
            <td><input type="submit" value="修改"></td>
        </tr>
    </table>
</form>
<hr>
</body>
</html>

```

