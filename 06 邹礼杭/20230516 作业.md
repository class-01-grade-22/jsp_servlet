### JDBC作业：

1. MySQL中创建一个数据库student_db

2. 库中创建student表

3. 表中数据如下

4. | 编号 | 姓名 | 性别 |
   | ---- | ---- | ---- |
   | 1    | 张三 | 男   |
   | 2    | 李四 | 女   |
   | 3    | 王五 | 男   |

5. 编写java 4个类，分别实现以下功能

   1. 查询功能，查询student中所有数据
   2. 添加功能
   3. 修改功能
   4. 删除功能

6. 扩展题【预习题】

   1. 能否实现一个类中，用四个方法来实现上面4个类的功能
   2. 能否实现将查询的结果，封装成java对象

mysql

```mysql
create database student_db charset utf8;
use student_db;

create table if not exists student(
id int,
name VARCHAR(20),
sex VARCHAR(1)
);

desc student;
select * from student;
```

查询功能

```Java
import java.sql.*;
public class Select {
    public static void main(String[] args) {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            //1注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            //2获取连接对象
            String url=("jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8");
            String username="root";
            String password="root";
            conn = DriverManager.getConnection(url,username,password);
            //3编写语句
            String sql="select * from student; ";
            //4获取执行sql的对象
            st = conn.createStatement();
            //5执行sql
            rs = st.executeQuery(sql);
            //6查询结果
            while (rs.next()){
                System.out.println(rs.getInt("id") + "\t" + rs.getString("name")+"\t"+rs.getString("sex"));
            }
        } catch (ClassNotFoundException e) {
            System.out.println("驱动导入异常");
        } catch (SQLException e) {
            System.out.println("sql执行异常");
        } finally {
            try {
                //7释放资源
                if (rs != null){
                    rs.close();
                }
                if (st != null) {
                    st.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println("释放资源异常");
            }
        }
        
    }
}
```

添加功能

```java
import java.sql.*;

public class Insert {
    public static void main(String[] args) {
        Connection conn = null;
        Statement st = null;
        try {
            //1注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            //2获取连接对象
            String url=("jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8");
            String username="root";
            String password="root";
            conn = DriverManager.getConnection(url,username,password);
            //3编写语句
            String sql="insert into student VALUES (3,'王五','男');";
            //4获取执行sql的对象
            st = conn.createStatement();
            //5执行sql
            int i = st.executeUpdate(sql);
            //6查询结果
            System.out.println("成功添加了"+i+"行数据");
        } catch (ClassNotFoundException e) {
            System.out.println("驱动导入异常");
        } catch (SQLException e) {
            System.out.println("sql执行异常");
        } finally {
            try {
                //7释放资源
                if (st != null) {
                    st.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println("释放资源异常");
            }
        }
        
    }
}
```

修改功能

```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Update {
    public static void main(String[] args) {
        Connection conn = null;
        Statement st = null;
        try {
            //1注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            //2获取连接对象
            String url=("jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8");
            String username="root";
            String password="root";
            conn = DriverManager.getConnection(url,username,password);
            //3编写语句
            String sql="UPDATE student set id=1 where id=10;";
            //4获取执行sql的对象
            st = conn.createStatement();
            //5执行sql
            int i = st.executeUpdate(sql);
            //6查询结果
            System.out.println("成功修改了"+i+"行数据");
        } catch (ClassNotFoundException e) {
            System.out.println("驱动导入异常");
        } catch (SQLException e) {
            System.out.println("sql执行异常");
        } finally {
            try {
                //7释放资源
                if (st != null) {
                    st.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println("释放资源异常");
            }
        }


    }
}
```

删除功能

```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Delete {
    public static void main(String[] args) {
        Connection conn = null;
        Statement st = null;
        try {
            //1注册驱动
            Class.forName("com.mysql.jdbc.Driver");
            //2获取连接对象
            String url=("jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8");
            String username="root";
            String password="root";
            conn = DriverManager.getConnection(url,username,password);
            //3编写语句
            String sql="delete from student where name='玩vb'";
            //4获取执行sql的对象
            st = conn.createStatement();
            //5执行sql
            int i = st.executeUpdate(sql);
            //6查询结果
            System.out.println("成功删除了"+i+"行数据");
        } catch (ClassNotFoundException e) {
            System.out.println("驱动导入异常");
        } catch (SQLException e) {
            System.out.println("sql执行异常");
        } finally {
            try {
                //7释放资源
                if (st != null) {
                    st.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                System.out.println("释放资源异常");
            }
        }
        
    }
}
```

