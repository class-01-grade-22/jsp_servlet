```java
//学生类
package bean;

public class Student {
    private Object id;
    private String name;
    private Object age;
    private String sex;

    public Student() {

    }

    public Student(Object id, String name, Object age, String sex) {
        this.id = id;
        this.name = name;
        this.age = age;
        this.sex = sex;
    }

    public Object getId() {
        return id;
    }

    public void setId(Object id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public Object getAge() {
        return age;
    }

    public void setAge(Object age) {
        this.age = age;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", age=" + age +
                ", sex='" + sex + '\'' +
                '}';
    }
}


package dao;

import bean.Student;
import tool.DBTool;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class StudentDao {
    //查询所有学生
    public static ArrayList<Student> select(){
        String sql = "select * from massage";
        ResultSet rs = DBTool.select(sql);
        ArrayList<Student> list = new ArrayList<>();
        try {
            while (rs.next()){
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String sex = rs.getString("sex");
                Student stu = new Student(id,name,age,sex);
                list.add(stu);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBTool.close(null,null,rs);
        }
        return list;
    }

//dao包语句
    //根据ID查询学生
    public static Student selectByID(Object id){
        String sql = "select * from massage where id = ?";
        ResultSet rs = DBTool.select(sql,id);
        Student stu = null;
        try {
            if (rs.next()){
                int id1 = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String sex = rs.getString("sex");
                stu = new Student(id1,name,age,sex);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            DBTool.close(null,null,rs);
        }
        return stu;
    }

    //根据ID删除学生
    public static int deleteByID(Object id){
        String sql = "delete from massage where id = ?";
        int i = DBTool.update(sql,id);
        return i;
    }


    //新增学生
    public static int addByID(Student stu){
        String sql = "insert into massage values(?,?,?,?)";
        int i = DBTool.update(sql,stu.getId(),stu.getName(),stu.getAge(),stu.getSex());
        return i;
    }


    //修改学生
    public static int updateByID(Student stu, String id){
        String sql = "update massage set id = ?,name = ?,age = ?,sex = ? where id = ?";
        int i = DBTool.update(sql,stu.getId(),stu.getName(),stu.getAge(),stu.getSex(),stu.getId());
        return i;
    }
}


//servlet
package servlet;

import bean.Student;
import dao.StudentDao;
import tool.DBTool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.util.ArrayList;

@WebServlet("/student/*")
public class StudentC extends HttpServlet {
    StudentDao dao = new StudentDao();
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //查看所有的学生
        String path = req.getPathInfo();
        System.out.println("path = " + path);
        
        
        if (path == null || path.equals("/")){
            //System.out.println("查看所有的学生");
            ArrayList<Student> list = dao.select();
            req.setAttribute("list",list);
            req.getRequestDispatcher("/allselect.jsp").forward(req,resp);


        } else if (path.matches("/\\d+")) {
            String id = path.substring(1);
            //System.out.println("正在查看编号为："+ id +"的学生");
            Student stu = dao.selectByID(id);
            req.setAttribute("stu",stu);
            req.getRequestDispatcher("/SelectByID.jsp").forward(req,resp);

        } else if (path.matches("/update/\\d+")) {
            String id = path.substring(8);
            Student stu = StudentDao.selectByID(id);
            req.setAttribute("stu",stu);
            req.getRequestDispatcher("/UpdateByID.jsp").forward(req,resp);

        }else if (path.matches("/delete/\\d+")) {
            String id = path.substring(8);
            //System.out.println("正在删除编号为：" + id + "的学生");
            int i = StudentDao.deleteByID(id);
            resp.sendRedirect("/student");

        }else if (path.matches("/add")) {
            //String id = path.substring(8);
            //System.out.println("正在添加");
            req.getRequestDispatcher("/add.jsp").forward(req,resp);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String age = req.getParameter("age");
        String sex = req.getParameter("sex");

        Student student = new Student(id,name,age,sex);
        String path = req.getPathInfo();
        System.out.println(id + name + age + sex);
        if (path.matches("/update/")){
            int i = StudentDao.updateByID(student,id);
            if (i > 0){
                resp.getWriter().write("修改成功");

            }else {
                resp.getWriter().write("修改失败");
            }
        }
        if (path.matches("/add")){
            int i = StudentDao.addByID(new Student(id, name, age, sex));
            if (i > 0){
                resp.getWriter().write("添加成功");

            }else {
                resp.getWriter().write("添加失败");
            }
        }
    }
}


//工具类
package tool;

import javax.servlet.http.HttpServlet;
import java.sql.*;

public class DBTool extends HttpServlet {
    //1.链接
    private static final String url = "jdbc:mysql://localhost:3306/student?useSSL=false&useUnicode=true&charsetEncoding=utf8";
    private static final String username = "root";
    private static final String password = "root";

    //2.注册
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("注册错误");
        }
    }

    //3.获取链接
    public static Connection getConn(){
        Connection conn = null;
        try {
             conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            System.out.println("连接错误");
        }
        return conn;
    }

    //4.关闭
    public static void close(Connection conn, PreparedStatement st, ResultSet rs){
        try {
            if (rs != null){
                rs.close();
            }
            if (st != null){
                st.close();
            }
            if (conn != null){
                conn.close();
            }
        } catch (SQLException e) {
            System.out.println("关闭失败");
        }
    }

    //5.通用的查询
    public static ResultSet select(String sql,Object ...more){
        Connection conn = getConn();
        ResultSet rs = null;
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            for (int i = 0; i < more.length; i++) {
                st.setObject((i + 1),more[i]);
            }
            rs = st.executeQuery();
        } catch (SQLException e) {
            System.out.println("查询语句异常");
        }
        return rs;
    }

    //6.通用的修改
    public static int update(String sql,Object ...more){
        Connection conn = getConn();
        int num = 0;
        try {
            PreparedStatement st = conn.prepareStatement(sql);
            for (int i = 0; i < more.length; i++) {
                st.setObject((i + 1),more[i]);
            }
             num = st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("修改语句异常");
        }
        return num;
    }
}

```

```jsp

//查看所有学生
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-05-31
  Time: 20:04
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>所有学生</title>
</head>
<body>
<table width="500px"border="1">
    <h1>查看所有学生</h1>
    <tr>
        <td>编号</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>性别</td>
        <td>操作</td>
    </tr>
    <c:forEach var="stu" items="${list}">
        <tr>
            <td>${stu.id}</td>
            <td>${stu.name}</td>
            <td>${stu.age}</td>
            <td>${stu.sex}</td>
            <td>
                <a href="/student/${stu.id}">查看</a>
                <a href="/student/update/${stu.id}">修改</a>
                <a href="/student/delete/${stu.id}" onclick="return confirm('确定要删除吗')">删除</a>
            </td>
        </tr>
    </c:forEach>
</table>
<table>
    <tr>
        <td><a href="student/add">添加学生</a></td>
    </tr>
</table>
</body>
</html>

//根据id查看学生
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-05-31
  Time: 20:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>ID查询</title>
</head>
<body>
<table width="500px"border="1">
    <h1>正在查看ID为${stu.id}的学生</h1>
    <tr>
        <td>编号</td>
        <td>姓名</td>
        <td>年龄</td>
        <td>性别</td>
        <td>操作</td>
    </tr>
    <c:forEach var="stu" items="${stu}">
    <tr>
        <td>${stu.id}</td>
        <td>${stu.name}</td>
        <td>${stu.age}</td>
        <td>${stu.sex}</td>
        <td>
            <a href="/student/${stu.id}">查看</a>
            <a href="/student/update/${stu.id}">修改</a>
            <a href="/student/delete/${stu.id}">删除</a>
        </td>
    </tr>
    </c:forEach>
</table>
</body>
</html>


//修改学生
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-01
  Time: 21:18
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改学生</title>
</head>
<body>
<form action="/student/update" method="post">
<table>
    <h1>正在修改编号为：${stu.id}的学生</h1>
    <tr>
        <td>编号</td>
        <td><input type="text" name = "id" value="${stu.id}"></td>
    </tr>
    <tr>
        <td>姓名</td>
        <td><input type="text" name = "name" value="${stu.name}"></td>
    </tr>
    <tr>
        <td>年龄</td>
        <td><input type="text" name = "age" value="${stu.age}"></td>
    </tr>
    <tr>
        <td>性别</td>
        <td><input type="text" name = "sex" value="${stu.sex}"></td>
    </tr>
</table>
    <table>
        <tr>
            <td><input type="submit" value="确定"></td>
        </tr>
    </table>
</form>
</body>
</html>


//添加学生
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-01
  Time: 23:05
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加学生</title>
</head>
<body>
<form action="/student/add" method="post">
  <table>
    <tr>
      <td>编号</td>
      <td><input type="text" name = "id" value="$stu.id"></td>
    </tr>
    <tr>
      <td>姓名</td>
      <td><input type="text" name = "name" value="$stu.name"></td>
    </tr>
    <tr>
      <td>年龄</td>
      <td><input type="text" name = "age" value="$stu.age"></td>
    </tr>
    <tr>
      <td>性别</td>
      <td><input type="text" name = "sex" value="$stu.sex"></td>
    </tr>
    <tr>
      <td><input type="submit" value="确定"></td>
    </tr>
  </table>
</form>
</body>
</html>

```

