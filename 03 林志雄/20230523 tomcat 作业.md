```markdown
# 作业，
 * 1 数据库相关的操作，使用封装的工具类
 * 2 编写四个servlet，使用注解指定相关的访问路径，分别对应查询，修改，删除，添加的操作
 * 3 从浏览器中，访问这中个路径，显示响应的信息，查询显示结果，其它的显示成功或是失败
 * 4 预习题：如何通过浏览器传送请求参数给servlet，servlet如何接收这些参数，并使用这些参数，去影响数据库的操作？

```

### mysql

```mysql
create database abc charset utf8;
use abc;
create table aaa(
id int PRIMARY key,
name varchar(10) 
);

insert into aaa VALUES
(1,"张三"),
(2,"王五"),
(3,"赵六");
```

### 封装

```java
package util;

import java.sql.*;

public class Fz {
    private static final String url = "jdbc:mysql:///abc?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String name = "root";
    private static final String pwd = "root";

    static{
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url, name, pwd);
        return conn;
    }

    public static ResultSet select(String sql, String ...key) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < key.length; i++) {
            pst.setString((i+1),key[i]);
        }
        ResultSet rs = pst.executeQuery();
        return rs;
    }

    public static int update(String sql,String ...key) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < key.length; i++) {
            pst.setString((i+1),key[i]);
        }
        int i=pst.executeUpdate();
        return i;
    }

    public static void close(Connection conn,PreparedStatement pst,ResultSet rs){
        try {
            if (rs!=null){
                rs.close();
            }
            if (pst!=null){
                pst.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
```

### 查询

```java
package servlet;

import util.Fz;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/cx")
public class ChaXunServlet extends HttpServlet {

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

        resp.setContentType("text/html;charset=utf-8");
        PrintWriter wri = resp.getWriter();

        wri.write("<style>\n" +
                "table{\n" +
                "width:400px;\n" +
                "border-collapse:collapse;\n" +
                "}\n" +
                "</style>" +
                "<table border=1>");
        wri.write("<tr><th>编号<th>姓名<tr>");
        String sql = "select * from aaa";
        try {
            ResultSet rs = Fz.select(sql);
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                wri.write("<tr><td>" + id + "<td>" + name + "<tr>");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
```

### 修改

```java
package servlet;

import util.Fz;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet("/xg")
public class XiuGaiServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter wri = resp.getWriter();

        String sql="update aaa set name='王七' where id=3";
        try {
            int i = Fz.update(sql);
            if (i>0){
                wri.write("成功");
            }else {
                wri.write("失败");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
```

### 添加

```java
package servlet;

import util.Fz;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet("/tj")
public class TianJiaServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter wri = resp.getWriter();

        String sql="insert into aaa values(4,'老六'),(5,'四七')";
        try {
            int i = Fz.update(sql);
            if (i>0){
                wri.write("成功");
            }else{
                wri.write("失败");
            }
        } catch (SQLException throwables) {
            throwables.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
```

### 删除

```java
package servlet;

import util.Fz;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet("/sc")
public class ShanChuServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter wri = resp.getWriter();

        String sql="delete from aaa where id = 4";
        try {
            int i = Fz.update(sql);
            if (i>0){
                wri.write("成功");
            }else {
                wri.write("失败");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
```