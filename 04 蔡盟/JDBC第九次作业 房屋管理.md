# mysql

```mysql
# 数据库名称：test
create database test charset utf8;
use test;
# 表：house_type (房屋类型表)
create table house_type
(
    id   int primary key auto_increment, # 编号  主键,自动增长列
    type varchar(50) not null            # 房屋类型    不允许为空
);
insert into house_type values
                           (null,'一室一厅'),
                           (null,'两室两厅'),
                           (null,'三室一厅');

# 表：house_info (房源信息表)
create table house_info
(
    id             int primary key auto_increment, # 编号    主键,自动增长列
    lease_mode     varchar(50),                    # 租赁方式  可以为空
    rent           double       not null,          # 租金       不允许为空
    contacts       varchar(20),                    # 联系人   可以为空
    deposit_method varchar(20),                    # 押金方式  可以为空
    house_type_id  int,                            # 房屋类型  外键
    address        varchar(200) not null,          # 详细地址  不允许为空
    foreign key (house_type_id) references house_type (id)
);
insert into house_info values
                           (null,'整租',1145,'张三','押一付一',1,'福山郊区'),
                           (null,'合租',1133,'张三','押二付一',2,'福山郊区'),
                           (null,'整租',2332,'张三','押一付一',1,'福山区'),
                           (null,'整租',11451,'张三','押一付三',3,'福山');
```

# bean

```java
package bean;

public class Info {
    private int id;
    private String mode;
    private double rent;
    private String contacts;
    private String method;
    private int house_type_id;
    private String address;
    private String type;

    @Override
    public String toString() {
        return "Info{" +
                "id=" + id +
                ", mode='" + mode + '\'' +
                ", rent=" + rent +
                ", contacts='" + contacts + '\'' +
                ", method='" + method + '\'' +
                ", house_type_id=" + house_type_id +
                ", address='" + address + '\'' +
                ", type='" + type + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getMode() {
        return mode;
    }

    public void setMode(String mode) {
        this.mode = mode;
    }

    public double getRent() {
        return rent;
    }

    public void setRent(double rent) {
        this.rent = rent;
    }

    public String getContacts() {
        return contacts;
    }

    public void setContacts(String contacts) {
        this.contacts = contacts;
    }

    public String getMethod() {
        return method;
    }

    public void setMethod(String method) {
        this.method = method;
    }

    public int getHouse_type_id() {
        return house_type_id;
    }

    public void setHouse_type_id(int house_type_id) {
        this.house_type_id = house_type_id;
    }

    public String getAddress() {
        return address;
    }

    public void setAddress(String address) {
        this.address = address;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public Info() {
    }

    public Info(int id, String mode, double rent, String contacts, String method, int house_type_id, String address, String type) {
        this.id = id;
        this.mode = mode;
        this.rent = rent;
        this.contacts = contacts;
        this.method = method;
        this.house_type_id = house_type_id;
        this.address = address;
        this.type = type;
    }
}
```

```java
package bean;

public class type {
    private int id;
    private String type;

    @Override
    public String toString() {
        return "type{" +
                "id=" + id +
                ", type='" + type + '\'' +
                '}';
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public type() {
    }

    public type(int id, String type) {
        this.id = id;
        this.type = type;
    }
}
```

# servtler

```java
package servtler;

import bean.Info;
import bean.type;
import utils.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.lang.reflect.Type;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/add")
public class addServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sql = "select * from house_type";

        ResultSet rs = DBUtil.qu(sql);

        ArrayList<type> list = new ArrayList<>();
        try {
            while (rs.next()) {
                int id = rs.getInt("id");
                String type = rs.getString("type");
                type type1 = new type(id, type);
                list.add(type1);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("list", list);
        request.getRequestDispatcher("/WEB-INF/add.jsp").forward(request, response);


    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String leaseMode = request.getParameter("leaseMode");
        String rent = request.getParameter("rent");
        String contacts = request.getParameter("contacts");
        String depositMiethod = request.getParameter("method");
        String typeId = request.getParameter("typeId");
        String address = request.getParameter("address");
        String sql = "insert into house_info values(?,?,?,?,?,?,?)";
        int i = DBUtil.up(sql, null, leaseMode, rent, contacts, depositMiethod, typeId, address);

        if (i > 0) {
            response.sendRedirect("/list");
        } else {
            request.setAttribute("msg", "添加失败");
            request.getRequestDispatcher("/WEB-INF/msg.jsp").forward(request, response);
        }
    }
}
```

```java
package servtler;

import bean.Info;
import utils.DBUtil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/list")
public class listServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        String sql = "select * from house_info i,house_type t where i.house_type_id=t.id";
        ResultSet rs = DBUtil.qu(sql);
        ArrayList<Info> list = new ArrayList<>();
        try {
            while (rs.next()){
                int id = rs.getInt("i.id");
                String mode = rs.getString("lease_mode");
                int rent = rs.getInt("rent");
                String contacts = rs.getString("contacts");
                String method = rs.getString("deposit_method");
                int tid = rs.getInt("t.id");
                String address = rs.getString("address");
                String type = rs.getString("type");
                Info info = new Info(id, mode, rent, contacts, method, tid, address, type);
                list.add(info);
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        request.setAttribute("list",list);
        request.getRequestDispatcher("/WEB-INF/list.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}
```

# utils

```java
package utils;

import java.sql.*;

public class DBUtil {
    static String url = "jdbc:mysql:///test?characterEncoding=utf8";
    static String uh = "root";
    static String pwa = "root";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url, uh, pwa);
        return conn;
    }

    public static ResultSet qu(String sql, Object... keys) {
        ResultSet rs = null;
        try {
            Connection conn = getConn();
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i + 1), keys[i]);
            }
            rs = pst.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return rs;
    }

    public static int up(String sql, Object... keys) {
        int rs = 0;
        try {
            Connection conn = getConn();
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i + 1), keys[i]);
            }
            rs = pst.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return rs;
    }
}
```

# jsp

```
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>房源信息</title>
</head>
<body>
<a href="/add"><button>添加</button></a>
<table border="1">
    <tr>
        <td>编号</td>
        <td>租赁方式</td>
        <td>租金</td>
        <td>联系人</td>
        <td>押金方式</td>
        <td>房屋类型</td>
        <td>详细地址</td>
    </tr>
    <c:forEach items="${list}" var="a">
        <tr>
            <td>${a.id}</td>
            <td>${a.mode}</td>
            <td>${a.rent}</td>
            <td>${a.contacts}</td>
            <td>${a.method}</td>
            <td>${a.type}</td>
            <td>${a.address}</td>
        </tr>
    </c:forEach>
</table>
</body>
</html>
```

```
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: 蔡庆辉
  Date: 2023/6/11
  Time: 18:55
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加信息</title>
</head>
<body>
<form action="/add" method="post">
    <table border="1">
        <tr>
            <td>租赁方式</td>
            <td><input type="text" name="leaseMode"></td>
        </tr>
        <tr>
            <td>租金</td>
            <td><input type="text" name="rent"></td>
        </tr>
        <tr>
            <td>联系人</td>
            <td><input type="text" name="contacts"></td>
        </tr>
        <tr>
            <td>押金方式</td>
            <td><input type="text" name="method"></td>
        </tr>
        <tr>
            <td>房屋类型</td>
            <td><select name="typeId" id="">
                <c:forEach items="${list}" var="t">
                    <option value="${t.id}">${t.type}</option>
                </c:forEach>
            </select></td>
        </tr>
        <tr>
            <td>详细地址</td>
            <td><input type="text" name="address"></td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit"></td>
        </tr>


    </table>
</form>
</body>
</html>
```

```
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
${msg}
<a href="/list">返回列表</a>
</body>
</html>
```