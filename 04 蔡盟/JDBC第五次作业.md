```mysql


CREATE DATABASE student_db CHARSET utf8; USE student_db;

CREATE TABLE student( id INT, name VARCHAR(20), sex VARCHAR(20) );

INSERT into student VALUES (1,"老大","女"), (2,"老二","男"), (3,"老三","男"), (4,"老四","女"), (5,"老五","男"), (6,"老六","女");


```

创建工具类：

```java
package util;

import java.sql.*;

public class uptil {

  private static final String url="jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";

  private static final String usename="root";

  private static final String password="root";

  static {

    try {

     Class.forName("com.mysql.jdbc.Driver");
    } catch (ClassNotFoundException e) {

      throw new RuntimeException(e);
    }
  }

  public static Connection getconn(){

    Connection conn = null;

    try {

      conn = DriverManager.getConnection(url, usename, password);

    } catch (SQLException e) {

      throw new RuntimeException(e);

    }

    return conn;

  }

  public static ResultSet select(String sql,Object ...keys){

    Connection conn = getconn();

    ResultSet rs = null;

    try {

      PreparedStatement pst = conn.prepareStatement(sql);

      for (int i = 0; i < keys.length; i++) {

        pst.setObject((i+1),keys[i]);

      }

      rs = pst.executeQuery();

    } catch (SQLException e) {

      throw new RuntimeException(e);

    }

    return rs;

  }

  public static int update(String sql,Object ...keys){

    Connection conn = getconn();

    int num = 0;

    try {

      PreparedStatement pst = conn.prepareStatement(sql);

      for (int i = 0; i < keys.length; i++) {

        pst.setObject((i+1),keys[i]);

      }

      num = pst.executeUpdate();

    } catch (SQLException e) {

      throw new RuntimeException(e);

    }

    return num;

  }

  public static void close(ResultSet rs,Connection conn,PreparedStatement pst){

    try {

      if (rs!=null){

        rs.close();
      }

      if (conn!=null){
        conn.close();
      }
      if (pst!=null){
        pst.close();
      }
    } catch (SQLException e) {
      throw new RuntimeException(e);
    }
  }
}

```

创建dao包：

```
package dao;

import bean.Student;

import util.uptil;

import java.sql.ResultSet;

import java.sql.SQLException;

import java.util.ArrayList;

public class Studentdao {

  public  ArrayList<Student> getallstudent(){

    String sql="select * from student";

    ArrayList<Student> list = new ArrayList<>();

    ResultSet rs = uptil.select(sql);

    try {

      while (rs.next()){

        int id = rs.getInt("id");

        String name = rs.getString("name");

        String sex = rs.getString("sex");

        Student stu = new Student(id,name,sex);

        list.add(stu);

      }

    } catch (SQLException e) {

      throw new RuntimeException(e);

    }

    return list;

  }


  public  Student selectstudent(int id){

    String sql="select * from student where id=?";

    Student stu=null;

    ResultSet rs = uptil.select(sql);

    try {

      while (rs.next()){

        String name = rs.getString("name");

        String sex = rs.getString("sex");

        stu = new Student(id,name,sex);

      }

    } catch (SQLException e) {

      throw new RuntimeException(e);

   }

    return stu;

  }


  public int deletestudent(int id){

    String sql="delete * from student where id=?";

    int i = uptil.update(sql, id);

    return i;

  }


  public int  updatestudent(Student student){

    String sql="update student set name=?,sex=? where id=?";

    int i = uptil.update(sql, student.getName(), student.getSex(), student.getId());

    return i;

  }


  public int addstudent(Student student){

    String sql="insert into student values (?,?,?)";

    int i = uptil.update(sql, student.getId(), student.getName(), student.getSex());

    return i;

  }

}
```

学生类：

```
package bean;

public class Student {

  private int id;

  private String name;

  private String sex;

  public Student() {

  }

  public Student(int id, String name, String sex) {

    this.id = id;

    this.name = name;

    this.sex = sex;

  }

  public int getId() {

    return id;

  }

  public void setId(int id) {

    this.id = id;

  }

  public String getName() {

    return name;

  }

  public void setName(String name) {

    this.name = name;

  }

  @Override

  public String toString() {

    return "Student{" +

        "id=" + id +

        ", name='" + name + '\'' +

        ", sex='" + sex + '\'' +

        '}';

  }

  public String getSex() {

    return sex;

  }

  public void setSex(String sex) {

    this.sex = sex;

  }

}
```

创建servlet：

```
package servlet;

import bean.Student;

import dao.Studentdao;

import javax.servlet.*;

import javax.servlet.http.*;

import javax.servlet.annotation.*;

import java.io.IOException;

import java.util.ArrayList;

@WebServlet("/student/*")

public class student extends HttpServlet {

  Studentdao dao = new Studentdao();

  @Override

  protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    String path = request.getPathInfo();

    System.out.println("path = " + path);

    if (path==null || path.equals("/")){


      ArrayList<Student> list = dao.getallstudent();

      request.setAttribute("list",list);

      request.getRequestDispatcher("/editById.jsp").forward(request,response);

    } else if (path.matches("/\\d+")) {

      String id = path.substring(1);

      System.out.println("/student/111 查看"+id+"编号的学生");

    } else if (path.matches("/delete/\\d+")) {

      String id = path.substring(8);

      System.out.println("/student/delete/111 删除"+id+"编号的学生");

    }else if (path.matches("/update/\\d+")){

      String id = path.substring(8);

      System.out.println("/student/update/111 修改"+id+"编号的学生");

    } else if (path.matches("/add")) {

      System.out.println("/student/add 正在添加学生");

    }

  }

  @Override

  protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    doGet(request,response);

  }

}
```

创建jsp：

```
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>

<%@ page contentType="text/html;charset=UTF-8" language="java" %>

<html>

<head>

  <title>学生列表</title>

</head>

<body>

<h1>学生列表</h1>

<hr>

<table border="1" width="500px">

  <tr>

    <th>编号</th>

    <th>姓名</th>

    <th>性别</th>

    <th>操作</th>

  </tr>

  <c:forEach var="stu" items="${list}">

    <tr>

      <td>${stu.id}</td>

      <td>${stu.name}</td>

      <td>${stu.sex}</td>

      <td>
                <a href="/student/${stu.id}">查看</a>

                <a href="/student/update/${stu.id}">修改</a>

                <a href="/student/delete/${stu.id}">删除</a>

      </td>

    </tr>

  </c:forEach>

</table>

<hr>

<a href="/student/add">添加学生</a>

</body>

</html>
```

