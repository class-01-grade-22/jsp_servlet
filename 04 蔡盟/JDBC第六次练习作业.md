# MySQL代码

```mysql
create database CarInfo charset utf8;
use CarInfo;
  
create table Brand (
    BrandID int primary key auto_increment, 
    BrandName   varchar(50) not null        
);
create table CarDetail (
    CID int     primary key auto_increment,
    Cname   Varchar(20),                    
    Content varchar(200)    not null,
    ltime   TIMESTAMP   not null default now(), 
    price   int ,                           
    BrandID int,                                         
    foreign key (BrandID) REFERENCES    Brand(BrandID)  
);
insert into Brand VALUES
(0,"宝马"),
(0,"奔驰"),
(0,"奥迪");

insert into CarDetail VALUES
(0,"宝马A9","坐在宝马中哭,也不愿在自行车上笑","2023-06-04 15:10:00",6521999,1),
(0,"奔驰","豪车","2023-06-04 15:10:00",5521999,2),
(0,"奥迪A4L","适合年轻人的一款车","2023-06-04 15:10:00",3521999,3);
SELECT * FROM brand;
select * from cardetail
```

# 工具类代码

```java
package utils;

import java.sql.*;

public class DBUtil {
    private static final String url="jdbc:mysql:///carinfo?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String username="root";
    private static final String password="root";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("驱动注册失败");
            throw new RuntimeException(e);
        }
    }
    public static Connection getConn()  {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return conn;
    }
    public static void close(Connection conn, PreparedStatement pst, ResultSet rs){
        try {
            if (rs!=null){
                rs.close();
            }
            if (pst!=null){
                pst.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
    public static ResultSet query(String sql,Object... keys){
        Connection conn = getConn();
        ResultSet rs=null;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            rs = pst.executeQuery();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return rs;
    }

    public static int update(String sql,Object... keys){
        Connection conn = getConn();
        int num=0;
        try {
            PreparedStatement pst = conn.prepareStatement(sql);
            for (int i = 0; i < keys.length; i++) {
                pst.setObject((i+1),keys[i]);
            }
            num = pst.executeUpdate();
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return num;
    }
}

```

# bean

```java
package bean;

public class Brand {
  private int brandId;
  private String brandName;

    public Brand() {
    }

    public Brand(int brandId, String brandName) {
        this.brandId = brandId;
        this.brandName = brandName;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    @Override
    public String toString() {
        return "Brand{" +
                "brandId=" + brandId +
                ", brandName='" + brandName + '\'' +
                '}';
    }
}
package bean;

import java.util.Date;

public class CarDetail {
    private  int cId;
    private  String cName;
    private  String content;
    private  Date lTime;
    private  int price;
    private  int BrandID;

    public CarDetail() {
    }

    public CarDetail(int cId, String cName, String content, Date lTime, int price, int brandID) {
        this.cId = cId;
        this.cName = cName;
        this.content = content;
        this.lTime = lTime;
        this.price = price;
        BrandID = brandID;
    }

    public int getCId() {
        return cId;
    }

    public void setCId(int cId) {
        this.cId = cId;
    }

    public String getCName() {
        return cName;
    }

    public void setCName(String cName) {
        this.cName = cName;
    }

    public String getContent() {
        return content;
    }

    public void setContent(String content) {
        this.content = content;
    }

    public Date getLTime() {
        return lTime;
    }

    public void setlTime(Date lTime) {
        this.lTime = lTime;
    }

    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    public int getBrandID() {
        return BrandID;
    }

    public void setBrandID(int brandID) {
        BrandID = brandID;
    }

    @Override
    public String toString() {
        return "CarDetail{" +
                "cId=" + cId +
                ", cName='" + cName + '\'' +
                ", content='" + content + '\'' +
                ", lTime=" + lTime +
                ", price=" + price +
                ", BrandID=" + BrandID +
                '}';
    }
}

```

# Dao

```java
package dao;

import bean.Brand;
import utils.DBUtil;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class BrandDao {
    public ArrayList<Brand> getAllBrand(){
        String sql ="select * from brand";
        ResultSet rs = DBUtil.query(sql);
        ArrayList<Brand> list = new ArrayList<>();
        try {
            while (rs.next()){
                int id = rs.getInt(1);
                String name = rs.getString(2);
                Brand brand = new Brand(id, name);
                list.add(brand);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

        return list;
    }
}




package dao;

import bean.CarDetail;
import utils.DBUtil;

import java.sql.Date;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class CarDao {
    public ArrayList<CarDetail> getAllCar() {
        ArrayList<CarDetail> list = new ArrayList<>();
        String sql = "select * from cardetail c , brand b where c.BrandID=b.BrandID";
        ResultSet rs = DBUtil.query(sql);
        try {
            while (rs.next()) {
                int cid = rs.getInt("cid");
                String cname = rs.getString("cname");
                String content = rs.getString("content");
                Date ltime = rs.getDate("ltime");
                int price = rs.getInt("price");
                int brandid = rs.getInt("brandid");
                String brandname = rs.getString("brandname");
                CarDetail car = new CarDetail(cid, cname, content, ltime, price, brandid,brandname);
                list.add(car);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        return list;
    }

    public int addCar(CarDetail car) {
        String sql = "insert into cardetail values (?,?,?,?,?,?)";
        int i = DBUtil.update(sql, car.getCId(), car.getCName(), car.getContent(), car.getLTime(), car.getPrice(), car.getBrandID());
        return i;
    }
}

```

# servlet

```java
package servlet;

import bean.Brand;
import bean.CarDetail;
import dao.BrandDao;
import dao.CarDao;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

@WebServlet("/car/*")
public class CarController extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String path = request.getPathInfo();
        if (path==null || path.equals("/")){
            ArrayList<CarDetail> list = new CarDao().getAllCar();
            request.setAttribute("list",list);
            request.getRequestDispatcher("/WEB-INF/carList.jsp").forward(request,response);
        }else if (path.equals("/add")){
            ArrayList<Brand> list = new BrandDao().getAllBrand();
            request.setAttribute("list",list);
            request.getRequestDispatcher("/WEB-INF/form.jsp").forward(request,response);
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        String path = request.getPathInfo();
        if (path.equals("/save")){
            String name = request.getParameter("name");
            int brandid = Integer.parseInt(request.getParameter("brandid"));
            String content = request.getParameter("content");
            int price = Integer.parseInt(request.getParameter("price"));
            String date = request.getParameter("date");  //date = 2023-06-09
            SimpleDateFormat simpleDateFormat = new SimpleDateFormat("yyyy-MM-dd");
            Date newDate = null;
            try {
                newDate = simpleDateFormat.parse(date);
            } catch (ParseException e) {
                throw new RuntimeException(e);
            }
            CarDetail car = new CarDetail(0, name, content, newDate, price, brandid, null);
            int i = new CarDao().addCar(car);
            if (i>0){
                request.setAttribute("msg","添加成功");
                request.getRequestDispatcher("/WEB-INF/msg.jsp").forward(request,response);
            }else{
                request.setAttribute("msg","添加失败");
                request.getRequestDispatcher("/WEB-INF/msg.jsp").forward(request,response);
            }
        }
    }
}

```

# jsp

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-02
  Time: 15:52
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<%--jsp 可以用el 表达式配合jstl标签库循环 显示--%>
<%--${list} 表示了所有集合内容--%>
<%--为了条例性，我们可以用表格来显示，可以看试卷中的示例--%>
<a href="/car/add"><button>添加</button></a>
<hr>

<table border="1">
<%--    第一行是表格头--%>
    <tr>
<%--  th 是加粗且居中的td      --%>
        <th>序号</th>
        <th>车辆名称</th>
        <th>所属品牌</th>
        <th>车辆简介</th>
        <th>价格</th>
        <th>录入时间</th>
    </tr>
<%--    第二行开始就是从list中循环读取 使用<c:forEach--%>
<c:forEach items="${list}"  var="car">
    <tr>
<%--        car 表示一个车辆对象，对象有属性，这时属性名，可以通用car.属性名的方式取值，属性名要从类中找 --%>
<%--       而且要注意大小写--%>
        <td>${car.CId}</td>
        <td>${car.CName}</td>
        <td>${car.brandName}</td>
<%--   brandID 改成中文品牌名称  --%>
        <td>${car.content}</td>
        <td>${car.price}</td>
        <td>${car.LTime}</td>
    </tr>
</c:forEach>


</table>


</body>
</html>

<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加</title>
</head>
<body>
<form action="/car/save" method="post">
    <%--    自动补全的快捷方式 tab --%>
    <table>
        <tr>
            <th>车辆名称</th>
            <td><input type="text" name="name" required></td>
        </tr>
        <tr>
            <th>所属品牌</th>
            <td>
                <select name="brandid" id="" >
                    <option value="">请选择</option>
                    <c:forEach items="${list}" var="brand">
                    <option value="${brand.brandId}">${brand.brandName}</option>
                    </c:forEach>

                </select>

            </td>
        </tr>
        <tr>
            <th>车辆简介</th>
            <td><input type="text" name="content" required></td>
        </tr>
        <tr>
            <th>价格</th>
            <td><input type="text" name="price" required></td>
        </tr>
        <tr>
            <th>录入时间</th>
            <td><input type="date" name="date" required></td>
        </tr>
        <tr>

            <td><input type="submit" value="确定"></td>
        </tr>
    </table>
</form>
</body>
</html>

<%--
  Created by IntelliJ IDEA.
  User: Administrator
  Date: 2023-06-02
  Time: 17:26
  To change this template use File | Settings | File Templates.
--%>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>提示信息</title>
</head>
<body>
<h1>${msg}</h1>

</body>
</html>

```
