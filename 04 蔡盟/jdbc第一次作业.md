### JDBC作业：

1、MySQL中创建一个数据库student_db

2、库中创建student表

3、表中数据如下

| 编号 | 姓名 | 性别 |
| ---- | ---- | ---- |
| 1    | 张三 | 男   |
| 2    | 李四 | 女   |
| 3    | 王五 | 男   |

```
CREATE database abl charset utf8;
use abl;
create table a(
id int,
name varchar(10),
sex enum('男','女')
);
insert into a values
(1,"张三","男"),
(2,"李四","女"),
(3,"王五","男");
```

4、编写java 4个类，分别实现以下功能

1、查询功能，查询student中所有数据

```
import java.sql.*;

public class ChaKan {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        ResultSet rs = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/student_db?characterEncoding=utf-8&useSSL=false", "root", "root");
            stmt = conn.createStatement();
            rs = stmt.executeQuery("select * from student");
            while (rs.next()) {
                System.out.println(rs.getInt("id") + "\t" + rs.getString("name") + "\t" + rs.getString("sex"));
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (rs != null) {
                    rs.close();
                }
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }


    }
}
```

2、添加功能

```
import java.sql.*;

public class TianJia {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/student_db?characterEncoding=utf-8&useSSL=false", "root", "root");
            String sql="insert into student values (4,'老六','男')";
            stmt = conn.createStatement();
            int i = stmt.executeUpdate(sql);
                System.out.println("添加成功" + i+"行");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt!=null){
                    stmt.close();
                }
                if (conn!=null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
```

3、修改功能

```
import java.sql.*;

public class XiuGai {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/student_db?characterEncoding=utf-8&useSSL=false", "root", "root");
            String sql="update student set name='王七' where id=3";
            stmt = conn.createStatement();
            int i = stmt.executeUpdate(sql);
            System.out.println("修改成功" + i+"行");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt!=null){
                    stmt.close();
                }
                if (conn!=null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
```

删除功能

```
import java.sql.*;

public class ShanChu {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;
        try {
            Class.forName("com.mysql.jdbc.Driver");
            conn = DriverManager.getConnection("jdbc:mysql://localhost:3306/student_db?characterEncoding=utf-8&useSSL=false", "root", "root");
            String sql = "delete from student where id=4";
            stmt = conn.createStatement();
            int i = stmt.executeUpdate(sql);
            if (i > 0) {
                System.out.println("删除成功" + i + "行");
            } else {
                System.out.println("删除了" + i + "行");
            }
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (stmt != null) {
                    stmt.close();
                }
                if (conn != null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
    }
}
```

扩展题【预习题】

1. 能否实现一个类中，用四个方法来实现上面4个类的功能
2. 能否实现将查询的结果，封装成java对象