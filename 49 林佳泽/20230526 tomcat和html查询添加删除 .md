DB工具集

```java

package tool;

import java.sql.*;

public class DBTool {
    private static final String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf-8";
    private static final String useName = "root";
    private static final String password = "root";

    //注册驱动
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    //获取连接对象
    public static Connection conn() throws SQLException {
        Connection conn = DriverManager.getConnection(url,useName,password);
        return conn;
    }

    //通用查询方法
    public static ResultSet select(String sql,String ...keys) throws SQLException {
        Connection conn = conn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+1),keys[i]);
        }
        ResultSet rs = pst.executeQuery();
        return rs;
    }

    //通用增删改
    public static int update(String sql,String ...keys) throws SQLException {
        Connection conn = conn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+1),keys[i]);
        }
        int i = pst.executeUpdate();
        return i;
    }

    //关闭资源
    public static void close(ResultSet rs,PreparedStatement pst,Connection conn) throws SQLException {
        if (rs != null){
            rs.close();}
        if (pst != null){
            pst.close();}
        if (conn != null){
            conn.close();
        }
    }
}

```

selectservlet

```java
package servlet;

import tool.DBTool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/selectservlet")
public class SelectServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //编码
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        //获取请求参数
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        //编写sql语句
        String sql = "select * from student where id = ? or name = ?";

        try {
            ResultSet rs = DBTool.select(sql, id, name);
            while (rs.next()){
                int id1 = rs.getInt("id");
                String name1 = rs.getString("name");
                String sex = rs.getString("sex");
                resp.getWriter().write(id1 + name1 + sex );

            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}
```

InsertServlet

```java

package servlet;

import tool.DBTool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/insertservlet")
public class InsertServlet extends HttpServlet {
    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //编码
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        String name = req.getParameter("name");
        String sex = req.getParameter("sex");

        String sql = "insert into student values (null,?,?)";
        try {
            int i = DBTool.update(sql, name, sex);
            if (i>0){
                resp.getWriter().write("你成功添加:"+name);
            }else {
                resp.getWriter().write("添加失败");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }
}

```

DeleteServlet

```java
package servlet;

import tool.DBTool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;

@WebServlet("/deleteservlet")
public class DeleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        String id = req.getParameter("id");
        String name = req.getParameter("name");
        String sql = "delete from student where id = ? or name = ? ";
        try {
            int i = DBTool.update(sql,id,name);
            if (i>0){
                resp.getWriter().write("删除成功");
            }else {
                resp.getWriter().write("删除失败");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

```

html

```html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>主页</title>
  <link rel="stylesheet" href="css.css">
</head>
<body>
<div class="dkj">
<div class="d1">
    <h1>学生管理系统</h1>
</div>
<div class="d2">
    <ul>
        <li><a href="select.html">查询</a> </li>
        <li><a href="insert.html">添加</a> </li>
        <li><a href="delete.html">删除</a> </li>
        <li><a href="">修改</a> </li>
    </ul>
</div>
</div>
</body>
</html>


查询
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>查询</title>
</head>
<body>
<form action="selectservlet" method="post">
  编号查询：<input type="text" name="id" ><br>
  或(二者选一即可)<br>
  姓名查询：<input type="text" name="name"><br>
  <input type="submit" value="提交查询">
</form>
</body>
</html>

添加
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>添加</title>
</head>
<body>
<form action="insertservlet" method="post">
  姓名：<input type="text" name="name" ><br>
  性别：<input type="radio" name="sex" value="男">男
  <input type="radio" name="sex" value="女">女<br>
    <input type="submit" value="提交">
</form>
</body>
</html>

删除
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>删除</title>
</head>
<body>
<form action="deleteservlet" method="post">
    编号删除：<input type="text" name="id"><br>
    或(二者选一)<br>
    姓名删除：<input type="text" name="name"><br>
    <input type="submit" value="删除">
</form>
</body>
</html>
```



css

```css
*{
    margin: 0;
    padding: 0;
}

li{
    list-style-type: none;
}

.dkj{
    width: 700px;
    margin: auto;
    margin-top: 300px;
}

.d1{
    width:700px ;
    height: 45px;
    margin: auto;
    margin-bottom: 30px;
    text-align: center;
}

.d2 {
    width: 700px;
}

.d2 ul li a{
    display: block;
    float: left;
    width:170px ;
    height: 33px;
    line-height: 33px;
    margin-left: 5px;
    font-weight: bold;
    background-color: aqua;
    text-align: center;
    text-decoration: none;
}

.d2 ul li a:link{
    color: darkorange;
}

.d2 ul li a:visited{
    color: darkorange;
}

.d2 ul li a:hover{
    background-color: blue;
}
```

