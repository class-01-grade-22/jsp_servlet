```java
import java.sql.*;

public class DBTool {
    private static final String url = "jdbc:mysql:///student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String name = "root";
    private static final String pwd = "root";
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url, name, pwd);
        return conn;
    }

    public static ResultSet select(String sql, String ...keys) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+1),keys[i]);
        }
        ResultSet rs = pst.executeQuery();
        return rs;
    }

    public static int update(String sql,String ...keys) throws SQLException {
        Connection conn = getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+1),keys[i]);
        }
        int i = pst.executeUpdate();
        return i;
    }

    public static void close(Connection conn,PreparedStatement pst,ResultSet rs){
        try {
            if (rs!=null){
                rs.close();
            }
            if (pst!=null){
                pst.close();
            }
            if (conn!=null){
                conn.close();
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}





import util.DBTool;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/test")
public class StudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        String sql="SELECT * FROM student";
        try {
            ResultSet rs = DBTool.select(sql);
            while (rs.next()){
                int id=rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                out.write(id+name+sex+"<br>");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}





import util.DBTool;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/test")
public class StudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();

        String sql="update student set name='sd' where id=3";
        try {
            int i = Fz.update(sql);
            if (i>0){
                out.write("修改成功");
            }else {
                out.write("修改失败");
            }
        } catch (SQLException e) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}





import util.DBTool;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/test")
public class StudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();

        String sql="insert into student values(5,'fde','男'),(6,'dsfse','女')";
        try {
            int i = Fz.update(sql);
            if (i>0){
                out.write("添加成功");
            }else {
               out.write("添加失败");
            }
        } catch (SQLException e) {
            throwables.printStackTrace();
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}





import util.DBTool;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;

@WebServlet("/test")
public class StudentServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();

        String sql="delete from student where id = 6";
        try {
            int i = Fz.update(sql);
            if (i>0){
                out.write("删除成功");
            }else {
               out.write("删除失败");
            }
        } catch (SQLException e) {
            throwables.printStackTrace();
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}
```

