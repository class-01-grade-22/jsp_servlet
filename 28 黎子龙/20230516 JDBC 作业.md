### JDBC作业：

1. MySQL中创建一个数据库student_db

2. 库中创建student表

3. 表中数据如下

4. | 编号 | 姓名 | 性别 |
   | ---- | ---- | ---- |
   | 1    | 张三 | 男   |
   | 2    | 李四 | 女   |
   | 3    | 王五 | 男   |



```mysql
create database student_db charset utf8;
use student_db;

create table student(
	id int,
	name varchar(10),
	sex varchar(10)
);

insert into student values 
	(1,'张三',男),
	(2,'李四',女),
	(3,'王五',男);
```




 5. 编写java 4个类，分别实现以下功能

   1. 查询功能，查询student中所有数据
   2. 添加功能
   3. 修改功能
   4. 删除功能

6. 扩展题【预习题】

   1. 能否实现一个类中，用四个方法来实现上面4个类的功能
   2. 能否实现将查询的结果，封装成java对象



```java
import java.sql.*;

// 0.创建工程，导入驱动jar包
public class Select {
    public static void main(String[] args) {
        Connection conn = null;
        Statement st = null;
        ResultSet rs = null;
        try {
            // 1.注册驱动
            Class.forName("com.mysql.jdbc.Driver");

            // 2.获取连接的对象
            String url = "jdbc:mysql://localhost:3306/student_db";
            String username = "root";
            String password = "root";
            conn = DriverManager.getConnection(url, username, password);

            // 3.定义SQL语句
            String sql = "select * from student";

            // 4.获取执行SQL的对象
            st = conn.createStatement();

            // 5.执行SQL
            rs = st.executeQuery(sql);

            // 6.处理返回结果
            while (rs.next()) {
                int id = rs.getInt("id");
                String name = rs.getString("name");
                String sex = rs.getString("sex");
                System.out.println(id + name + sex);
            }

        } catch (ClassNotFoundException e) {
            System.out.println("驱动导入异常");
            e.printStackTrace();
        } catch (SQLException e) {
            System.out.println("驱动导入异常");
            e.printStackTrace();
        } finally {
            try {
            // 7.释放资源
                if (rs != null)
                    rs.close();
                if (st != null)
                    st.close();
                if (conn != null)
                conn.close();
            } catch (SQLException e) {
                System.out.println("资源释放异常");
            }
        }
    }
}
```



```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Insert {
    public static void main(String[] args) {
        Connection conn = null;
        Statement st = null;
        try {
            // 1.注册驱动
            Class.forName("com.mysql.jdbc.Driver");

            // 2.获取连接的对象
            String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
            String username = "root";
            String password = "root";
            conn = DriverManager.getConnection(url, username, password);

            // 3.定义SQL语句
            String sql = "insert into asdf values (4,'赵七',女)";

            // 4.获取执行SQL的对象
            st = conn.createStatement();

            // 5.执行SQL
            int i = st.executeUpdate(sql);

            // 6.处理返回结果
            System.out.println("添加了" + i + "行数据");

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                // 7.释放资源
                if (conn != null)
                conn.close();
                if (st != null)
                st.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
```



```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Update {
    public static void main(String[] args) {
        Connection conn = null;
        Statement st = null;
        try {
            // 1.注册驱动
            Class.forName("com.mysql.jdbc.Driver");

            // 2.获取连接的对象
            String url = "jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
            String username = "root";
            String password = "root";
            conn = DriverManager.getConnection(url, username, password);

            // 3.定义SQL语句
            String sql = "update student set name = '王六' where id = 3";

            // 4.获取执行SQL的对象
            st = conn.createStatement();

            // 5.执行SQL
            int i = st.executeUpdate(sql);

            // 6.处理返回结果
            System.out.println("修改了" + i + "行数据");

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                // 7.释放资源
                if (conn != null)
                    conn.close();
                if (st != null)
                    st.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
```



```java
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;

public class Delete {
    public static void main(String[] args) {
        Connection conn = null;
        Statement st = null;
        try {
            // 1.注册驱动
            Class.forName("com.mysql.jdbc.Driver");

            // 2.获取连接的对象
            String url = "jdbc:mysql://localhost:3306/student";
            String username = "root";
            String password = "root";
            conn = DriverManager.getConnection(url, username, password);

            // 3.定义SQL语句
            String sql = "delete from student where id = 2";

            // 4.获取执行SQL的对象
            st = conn.createStatement();

            // 5.执行SQL
            int i = st.executeUpdate(sql);

            // 6.处理返回结果
            System.out.println("删除了" + i + "行数据");

        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        } catch (SQLException e) {
            throw new RuntimeException(e);
        } finally {
            try {
                // 7.释放资源
                if (conn != null)
                    conn.close();
                if (st != null)
                    st.close();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
        }
    }
}
```

