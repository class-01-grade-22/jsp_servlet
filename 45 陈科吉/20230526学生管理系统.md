## MySQL代码

```mysql
create database student_db character set utf8;
use student_db;
create table student(
id int unique key auto_increment,
naemd varchar(10),
sex varchar(10)
);
insert into student values 
(null,"张三","男"),
(null,"李四","女"),
(null,"王五","男");
```

## JAVA代码

### 1.工具类

```java
//数据库工具类
package Util;

import java.sql.*;

public class DButil {
    private static final String url="jdbc:mysql://localhost:3306/student_db?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String username="root";
    private static final String password="1011";

    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            e.printStackTrace();
        }
    }

    //查询
    public static ResultSet select(String sql,String...arr){
        ResultSet res=null;
        try {
            Connection conn = DriverManager.getConnection(url,username,password);
            PreparedStatement pre = conn.prepareStatement(sql);
            for (int i=0;i< arr.length;i++){
                pre.setString(i+1,arr[i]);
            }
            res = pre.executeQuery();
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return res;
    }


    //增删改
    public static int update(String sql,String...arr){
        PreparedStatement pre=null;
        Connection conn=null;
        int i = 0;
        try {
            conn = DriverManager.getConnection(url, username, password);
            pre = conn.prepareStatement(sql);
            for (i=0;i< arr.length;i++){
                pre.setString((i+1),arr[i]);
            }
            i = pre.executeUpdate();
        } catch (SQLException e) {
            e.printStackTrace();
        } finally {
            try {
                if (pre!=null) {
                    pre.close();
                }
                if (conn!=null) {
                    conn.close();
                }
            } catch (SQLException e) {
                e.printStackTrace();
            }
        }
        return i;
    }
}

```

```java
//学生工具类
package Util;

public class Student {
    private int id;
    private String name;
    private String sex;

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getSex() {
        return sex;
    }

    public void setSex(String sex) {
        this.sex = sex;
    }

    public Student() {
    }

    public Student(int id, String name, String sex) {
        this.id = id;
        this.name = name;
        this.sex = sex;
    }

    @Override
    public String toString() {
        return "Student{" +
                "id=" + id +
                ", name='" + name + '\'' +
                ", sex='" + sex + '\'' +
                '}';
    }
}

```

### 2.Servlet类

```java
//添加
package Servlet;

import Util.DButil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet("/insert")
public class Insert extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");

        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String sex = request.getParameter("sex");

        String sql="insert into student values (?,?,?)";
        int i = DButil.update(sql, id, name, sex);
        if (i>0){
            response.getWriter().write("添加成功");
        }else {
            response.getWriter().write("添加失败");
        }
    }
}

```

```java
//修改
package Servlet;

import Util.DButil;
import Util.Student;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Iterator;

@WebServlet("/update")
public class Update extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");

        String id = request.getParameter("id");
        ArrayList<Student> list = new ArrayList<>();
        String s="select * from student where id=?";
        ResultSet select = DButil.select(s, id);
        try {
            while (select.next()){
                int id1 = select.getInt("id");
                String named1 = select.getString("named");
                String sex1 = select.getString("sex");
                Student student = new Student(id1, named1, sex1);
                list.add(student);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        for (Student a:
             list) {
            System.out.println(a);
        }
        Student student=list.get(0);
        request.setAttribute("stu",student);
        request.getRequestDispatcher("/update.jsp").forward(request,response);
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");

        String id = request.getParameter("id");
        String name = request.getParameter("name");
        String sex = request.getParameter("sex");

        String sql="update student set named=?,sex=? where id=?";
        int i = DButil.update(sql, name, sex, id);
        if (i>0){
            response.getWriter().write("修改成功");
        }else {
            response.getWriter().write("修改失败");
        }
    }
}

```

```java
//删除
package Servlet;

import Util.DButil;

import javax.servlet.*;
import javax.servlet.http.*;
import javax.servlet.annotation.*;
import java.io.IOException;

@WebServlet("/delete")
public class Delete extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
        request.setCharacterEncoding("utf-8");
        response.setContentType("text/html;charset=utf-8");
        response.setCharacterEncoding("utf-8");

        String id=request.getParameter("id");
        System.out.println(id);

        String sql="delete from student where id=?";
        int update = DButil.update(sql, id);
        if (update>0){
            response.getWriter().write("删除成功");
        }else {
            response.getWriter().write("删除失败");
        }
    }

    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {

    }
}

```

## HTML

```html
       <!--查询学生所以信息并对其进行操作 -->

	    <%@ page import="Util.DButil" %>
        <%@ page import="java.sql.ResultSet" %>
        <%@ page import="java.sql.SQLException" %>
        <%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
  <head>
    <title>学生管理</title>
  </head>
  <body>
  <form action="/insert.jsp" method="post">
<h1>学生管理系统</h1>
  <table width="500" border="1">
    <tr>
      <th>编号</th>
      <th>姓名</th>
      <th>性别</th>
      <th>系统操作</th>
    </tr>
      <!--查询所以学生信息 -->
<%
  try {
    String sql = "select * from student";
    ResultSet select = DButil.select(sql);
    int id = 0;
    String name = null;
    String sex = null;
    while (select.next()) {
      id = select.getInt("id");
      name = select.getString("named");
      sex = select.getString("sex");
  %>
      <tr>
        <td><%=id%></td>
        <td><%=name%></td>
        <td><%=sex%></td>
        <td>
          <a href="/update?id=<%=id%>">修改</a><!--修改操作-->
          <a href="/delete?id=<%=id%>">删除</a><!--删除操作-->
        </td>
      </tr>
  <%
    }
  } catch (SQLException e) {
    throw new RuntimeException(e);
  }
%>
  </table>
    <br>
    <input type="submit" value="添加学生信息"><!--添加操作-->
  </form>
  </body>
</html>

```

```html
<!--添加学生信息-->
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>添加</title>
</head>
<body>
<form action="/insert" method="post">
    <h1>添加学生信息</h1>
    <p>编号：<input type="text" name="id"></p>
    <p>姓名：<input type="text" name="name"></p>
    <p>性别：<input type="radio" name="sex" value="男">男
            <input type="radio" name="sex" value="女">女
    </p>
    <input type="submit">
</form>
</body>
</html>

```

```html
<!--修改学生信息-->
<%@ page import="Util.Student" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>修改</title>
</head>
<body>
<%
    Student student= (Student) request.getAttribute("stu");
%>
<h1>更新学生信息</h1>
<form action="/update" method="post">
<p>姓名：<input type="text" name="name" value="<%=student.getName()%>"></p>
    <p>性别：<input type="radio" name="sex" value="男">男
            <input type="radio" name="sex" value="女">女
    </p>
    <input type="hidden" name="id" value="<%=student.getId()%>">
    <input type="submit">
</form>
</body>
</html>

```

