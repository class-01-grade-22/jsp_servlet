数据库

```mysql
create database attence charset utf8;
use attence;

create table Student
(
    sid   int primary key  auto_increment,        #     学号	主键,自动增长列
    sname varchar(20) unique not null #     学生姓名	唯一,非空
);

create table Attence
(
    aid  int primary key auto_increment,#                         考勤编号	主键,自动增长列
    time varchar(20) not null,#         出勤时间	非空
    type int comment '1:已到；2:迟到；3旷课',#         出勤状况	1:已到；2:迟到；3旷课
    sid  int ,#         学号	外键
    foreign key (sid) references Student(sid)
);

insert into Student values
                        (0,'张四'),
                        (0,'李三'),
                        (0,'赵六');

insert into Attence values
                        (0,'2022-04-23 18:30:00',1,2);
```

bean

```java
package bean;

public class Attence {
    private int aid;
    private String time;
    private int type;
    private int sid;
    private String sname;

    public int getAid() {
        return aid;
    }

    public void setAid(int aid) {
        this.aid = aid;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public int getType() {
        return type;
    }

    public void setType(int type) {
        this.type = type;
    }

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Attence() {
    }

    public Attence(int aid, String time, int type, int sid, String sname) {
        this.aid = aid;
        this.time = time;
        this.type = type;
        this.sid = sid;
        this.sname = sname;
    }

    @Override
    public String toString() {
        return "Attence{" +
                "aid=" + aid +
                ", time='" + time + '\'' +
                ", type=" + type +
                ", sid=" + sid +
                ", sname='" + sname + '\'' +
                '}';
    }
}

```

```java
package bean;

public class Student {
    private int sid;
    private String sname;

    public int getSid() {
        return sid;
    }

    public void setSid(int sid) {
        this.sid = sid;
    }

    public String getSname() {
        return sname;
    }

    public void setSname(String sname) {
        this.sname = sname;
    }

    public Student() {
    }

    public Student(int sid, String sname) {
        this.sid = sid;
        this.sname = sname;
    }

    @Override
    public String toString() {
        return "Student{" +
                "sid=" + sid +
                ", sname='" + sname + '\'' +
                '}';
    }
}

```

dao

```java
package dao;

import bean.Attence;
import bean.Student;
import util.DBUtil;

import java.sql.Array;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;

public class ServletDao {
    //查询所有考勤记录
    public ArrayList<Attence> allAttence() throws SQLException {
        String sql="select * from attence";
        ArrayList<Attence> list = new ArrayList<>();
        ResultSet re = DBUtil.getSelect(sql);
        while (re.next()) {
            int aid = re.getInt("aid");
            String time = re.getString("time");
            int type = re.getInt("type");
            int sid = re.getInt("sid");
            String sql2 = "select * from student where sid=?";
            ResultSet restu = DBUtil.getSelect(sql2, sid);
            String sname = null;
            while (restu.next()) {
                sname = restu.getString("sname");
            }
            Attence attence = new Attence(aid, time, type, sid, sname);
            list.add(attence);
        }
        return list;
    }

    //查看所有学生
    public  ArrayList<Student> getStu() throws SQLException {
        String sql="select * from student";
        ArrayList<Student> students = new ArrayList<>();
        ResultSet re = DBUtil.getSelect(sql);
        while (re.next()){
            int sid = re.getInt("sid");
            String sname = re.getString("sname");
            Student student = new Student(sid, sname);
            students.add(student);
        }
        return students;
    }

    //添加考勤
    public int addAttence(String sid,String time,String type) throws SQLException {
        String sql="insert into attence values (0,?,?,?)";
        int i = DBUtil.getUpdate(sql,  time, type,sid);
        return i;
    }
}

```

servlet

```java
package servlet;

import bean.Attence;
import bean.Student;
import dao.ServletDao;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.sql.SQLException;
import java.util.ArrayList;

@WebServlet("/attence/*")
public class TestServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");
        ServletDao dao = new ServletDao();
        String path = req.getPathInfo();
        if (path==null || path.equals("/")){
            ArrayList<Attence> list = null;
            try {
                list = dao.allAttence();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            req.setAttribute("list",list);
            req.getRequestDispatcher("/WEB-INF/allAtt.jsp").forward(req,resp);
        } else if (path.matches("/add")) {
            ArrayList<Student> stu = null;
            try {
                stu = dao.getStu();
            } catch (SQLException e) {
                throw new RuntimeException(e);
            }
            req.setAttribute("stu",stu);
            req.getRequestDispatcher("/WEB-INF/addAtt.jsp").forward(req,resp);
        }

    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        req.setCharacterEncoding("utf-8");
        resp.setContentType("text/html;charset=utf-8");

        String sid = req.getParameter("sid");
        String time = req.getParameter("time");
        String type = req.getParameter("type");
        try {
            int i = new ServletDao().addAttence(sid, time, type);
            if (i>0){
                resp.sendRedirect("/attence");
            }else {
                resp.getWriter().write("添加失败");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
    }
}

```

util

```java
package util;

import java.sql.*;

public class DBUtil {
    private static final String url ="jdbc:mysql:///attdb?useSSL=false&characterEncoding=utf-8";
    private static final String user="root";
    private static final String pwd="root";
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            throw new RuntimeException(e);
        }
    }

    public static Connection getConn() throws SQLException {
        Connection conn = DriverManager.getConnection(url, user, pwd);
        return conn;
    }
    public static ResultSet getSelect(String sql,Object... keys) throws SQLException {
        PreparedStatement pre = getConn().prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            pre.setObject((i+1),keys[i]);
        }
        ResultSet re = pre.executeQuery();
        return re;
    }
    public static int getUpdate(String sql,Object... keys) throws SQLException {
        PreparedStatement pre = getConn().prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            pre.setObject((i+1),keys[i]);
        }
        int j = pre.executeUpdate();
        return j;
    }
}

```

jsp

```jsp
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="/attence" method="post">
    <table border="1">
        <tr>
            <td>姓名：</td>
            <td><select name="sid" id="">
                <option value="">请选择学生</option>
                <c:forEach items="${stu}" var="stu">
                    <option value="${stu.getSid()}">${stu.getSname()}</option>
                </c:forEach>
            </select></td>
        </tr>
        <tr>
            <td>时间：</td>
            <td><input type="datetime-local" name="time"></td>
        </tr>
        <tr>
            <td>出勤情况：</td>
            <td>
                <input type="radio" name="type" value="1">
                已到
                <input type="radio" name="type" value="2">
                迟到
                <input type="radio" name="type" value="3">
                旷课
            </td>
        </tr>
        <tr>
            <td colspan="2"><input type="submit" value="添加"></td>
        </tr>
    </table>
</form>
</body>
</html>

```

```jsp
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ page contentType="text/html;charset=UTF-8" language="java" %>
<html>
<head>
    <title>Title</title>
</head>
<body>
<form action="/attence/add" method="get">
    <p><input type="submit" value="添加"></p>
</form>
    <table border="1">
        <tr>
            <th>考勤编号</th>
            <th>学生编号</th>
            <th>学生姓名</th>
            <th>出勤时间</th>
            <th>出勤状况</th>
        </tr>
        <c:forEach items="${list}" var="li">
            <tr>
                <td>${li.getAid()}</td>
                <td>${li.getSid()}</td>
                <td>${li.getSname()}</td>
                <td>${li.getTime()}</td>
                <td>
                    <c:if test="${li.getType()==1}">
                        已到
                    </c:if>
                    <c:if test="${li.getType()==2}">
                        迟到
                    </c:if>
                    <c:if test="${li.getType()==3}">
                        旷课
                    </c:if>
                </td>
            </tr>
        </c:forEach>
    </table>
</body>
</html>

```

