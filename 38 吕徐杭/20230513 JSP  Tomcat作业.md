# 作业

 * 1 数据库相关的操作，使用封装的工具类
 * 2 编写四个servlet，使用注解指定相关的访问路径，分别对应查询，修改，删除，添加的操作
 * 3 从浏览器中，访问这中个路径，显示响应的信息，查询显示结果，其它的显示成功或是失败
 * 4 预习题：如何通过浏览器传送请求参数给servlet，servlet如何接收这些参数，并使用这些参数，去影响数据库的操作？

```mysql
create database student_db character set utf8;
use student_db;

create table student (
	id int PRIMARY key auto_increment,
	name VARCHAR(20),
	sex VARCHAR(20);
);

insert into stu VALUES
(0,'张三','男'),
(0,'李四','男'),
(0,'王五','女');

select * from stu;
```

# 封装工具类util 

```java
import java.sql.*;

public class Util {
    private static final String url="jdbc:mysql:///test01?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String user="root";
    private static final String pwd="root";

    //注册驱动
    static {
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("驱动导入异常");
        }
    }


    //获取连接对象
    public static Connection getConn() throws SQLException {
        Connection conn= DriverManager.getConnection(url,user,pwd);
        return conn;
    }

    //通用的查询方法
    public static  ResultSet select(String sql,String ...keys) throws SQLException {
        Connection conn=getConn();
        PreparedStatement pst = conn.prepareStatement(sql); //prepare 预编译
        //给？赋值
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+i),keys[i]);
        }

        ResultSet rs = pst.executeQuery();
        return rs;
    }

    //通用的增 删 改 方法
    public static  int update(String sql,String ...keys) throws SQLException {
        Connection conn=getConn();
        PreparedStatement pst = conn.prepareStatement(sql);
        for (int i = 0; i < keys.length; i++) {
            pst.setString((i+1),keys[i]);
        }

        int i=pst.executeUpdate();
        return i;

    }
    //通用的关闭资源方法
    public static void close(Connection conn,PreparedStatement pst,ResultSet rs) throws SQLException {
        if (rs != null) {
            rs.close();
        }
        if (pst != null) {
            pst.close();
        }
        if (conn != null) {
            conn.close();
        }

    }
}

```

s

```
//增加
package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet("/insert")
public class InsertServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //http默认发起的是get请求
        System.out.println("收到响应");
        resp.setContentType("text/html;charset=utf-8");//设置网页的格式
        resp.setCharacterEncoding("utf-8");
        //通过响应对象把要显示的信息打印在浏览器
        PrintWriter out = resp.getWriter();//得到一个输出流对象
        out.write("添加成功<br>");
        int i=0;
        String sql="insert into student values (0,?,?)";
        try {
            i=Util.update(sql,"账上","女");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        out.write("成功添加"+i+"行数据");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}



//删除 

package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;

@WebServlet("/delete")
public class DeleteServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //http默认发起的是get请求
        System.out.println("收到响应");
        resp.setContentType("text/html;charset=utf-8");//设置网页的格式
        resp.setCharacterEncoding("utf-8");
        //通过响应对象把要显示的信息打印在浏览器
        PrintWriter out = resp.getWriter();//得到一个输出流对象
        out.write("删除成功<br>");
        int i=0;
        String sql="delete from student where name=?";
        try {
            i=Util.update(sql,"账上");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
            out.write("成功删除"+i+"行数据");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}


//修改

package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.SQLException;
@WebServlet("/update")

public class Updateservlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //http默认发起的是get请求
        System.out.println("收到响应");
        resp.setContentType("text/html;charset=utf-8");//设置网页的格式
        resp.setCharacterEncoding("utf-8");
        //通过响应对象把要显示的信息打印在浏览器
        PrintWriter out = resp.getWriter();//得到一个输出流对象
        out.write("添加成功<br>");
        int i=0;
        String sql="update student set sex=? where name=?";
        try {
            i=Util.update(sql,"男","王五");
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        out.write("成功修改"+i+"行数据");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}


//查询

package servlet;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.ResultSet;
import java.sql.SQLException;
@WebServlet("/select")

public class SelectServlet extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        //http默认发起的是get请求
        System.out.println("收到响应");
        resp.setContentType("text/html;charset=utf-8");//设置网页的格式
        resp.setCharacterEncoding("utf-8");
        //通过响应对象把要显示的信息打印在浏览器
        PrintWriter out = resp.getWriter();//得到一个输出流对象
        out.write("查询成功");

        out.write("<style>\n" +
                "table{\n" +
                "width: 500px;\n" +
                "border-collapse: collapse;\n" +
                "}\n" +
                " </style>" +
                "<table border=1>");
        out.write(" <tr><th>编号</th><th>姓名</th><th>性别</th></tr>");
        //使用Util工具类将数据库数据读取
        try {
            ResultSet rs = Util.select("select * from student");
            while (rs.next()){
                int id=rs.getInt("id");
                String name=rs.getString("name");
                String sex=rs.getString("sex");
                out.write("<tr><th>"+id+"</th><th>"+name+"</th><th>"+sex+"</th></tr>");
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        out.write("</table>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

```

