```java
//工具类
package tool;

import java.sql.*;

public class DBtool {
    //1.注册
    static{
        try {
            Class.forName("com.mysql.jdbc.Driver");
        } catch (ClassNotFoundException e) {
            System.out.println("驱动注册异常");
        }
    }
    //2.获取链接
    private static final String url =
            "jdbc:mysql://localhost:3306/student?useSSL=false&useUnicode=true&characterEncoding=utf8";
    private static final String username = "root";
    private static final String password = "root";

    //3.连接
    public static Connection getConn() {
        Connection conn = null;
        try {
            conn = DriverManager.getConnection(url, username, password);
        } catch (SQLException e) {
            System.out.println("驱动连接异常");
        }
        return conn;
    }
    //通用查询方法
    public static ResultSet select(String sql) {
        ResultSet rs = null;
        try {
            Connection conn = getConn();
            PreparedStatement st = conn.prepareStatement(sql);
            rs = st.executeQuery();
        } catch (SQLException e) {
            System.out.println("sql语言异常");
        }
        return rs;
    }

    //通用的修改方法
    public static int update(String sql,String ...more) {
        int i = 0;
        try {
            Connection conn = getConn();
            PreparedStatement st = conn.prepareStatement(sql);
            for (int j = 0; j < more.length; j++) {
                st.setString((j + 1),more[j]);
            }
            i = st.executeUpdate();
        } catch (SQLException e) {
            System.out.println("sql语言异常");
            e.printStackTrace();
        }
        return i;
    }
    public static void close(Connection conn,PreparedStatement st,ResultSet rs){
        try {
            if (rs != null){
                rs.close();
            }
            if (st != null) {
                st.close();
            }
            if (conn != null){
                conn.close();
            }
        } catch (SQLException e) {
            System.out.println("资源关闭异常");
        }
    }
}


//查询
package Student;

import tool.DBtool;

import javax.rmi.CORBA.Stub;
import javax.servlet.ServletException;
import javax.servlet.ServletResponse;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Scanner;

@WebServlet("/select")
public class select extends HttpServlet {
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        ArrayList<Student> list = new ArrayList<>();
        out.write("<table border = 1 width = 300px cellpadding = 5 cellspacing = 1>");
        out.write("<tr><td>"+"编号"+"<td>"+"姓名"+"</td>"+"<td>"+"年龄"+"</td>"+"<td>"+"性别"+"</td>"+"</td></tr>");
        try {
            String sql = "select * from massage";
            ResultSet rs = DBtool.select(sql);
            while (rs.next()){
                Student stu=new Student();
                int id = rs.getInt("id");
                String name = rs.getString("name");
                int age = rs.getInt("age");
                String sex = rs.getString("sex");
                out.write("<tr><td>"+id+"<td>"+name+"</td>"+"<td>"+age+"</td>"+"<td>"+sex+"</td>"+"</td></tr>");
                list.add(stu);
            }
        } catch (SQLException e) {
            throw new RuntimeException(e);
        }
        out.write("</table>");
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

//删除
package Student;

import tool.DBtool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Scanner;

@WebServlet("/delete")
public class delete extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Scanner sc = new Scanner(System.in);
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        out.write("删除成功");
        String sql = "delete from student where id = ？";
        String id = sc.next();
        int i = DBtool.update(sql,id);
        if (i > 0){
            System.out.println("删除成功");
        }else {
            System.out.println("删除失败");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}


//添加
package Student;

import tool.DBtool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Scanner;

@WebServlet("/insert")
public class insert extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        Scanner sc = new Scanner(System.in);
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        out.write("添加成功");
        String sql = "insert into massage value(?,?,?,?)";
        String id = sc.next();
        String name = sc.next();
        String age = sc.next();
        String sex = sc.next();
        int i = DBtool.update(sql,id,name,age,sex);
        if (i > 0){
            System.out.println("修改成功");
        }else {
            System.out.println("修改失败");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

//修改
package Student;

import tool.DBtool;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import java.io.IOException;
import java.io.PrintWriter;
import java.sql.*;
import java.util.Scanner;

@WebServlet("/update")
public class update extends HttpServlet{
    @Override
    protected void doGet(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        resp.setContentType("text/html;charset=utf-8");
        PrintWriter out = resp.getWriter();
        out.write("修改成功");
        String sql = "update student set id=? name = ? age = ? sex = ? where name=?";
        int i = DBtool.update(sql);
        if (i > 0){
            System.out.println("修改成功");
        }else {
            System.out.println("修改失败");
        }
    }

    @Override
    protected void doPost(HttpServletRequest req, HttpServletResponse resp) throws ServletException, IOException {
        super.doPost(req, resp);
    }
}

```

